# Fiche de syntèse de débat

La préparation du débat et le débat lui même vous ont apporté un certain nombre d'éléments.

+++consignes
<b>Vous devez</b>
<ol start="0">
  <li>Choisir un des sujets abordé pendant le débat</li>
  <li>Rédiger une synthèse sur ce sujet</li>
  <li>Une fois cette fiche remplie, enregistrez-là en un fichier pdf en cliquant sur le bouton <a type="button" class="btn btn-dark leftcard-btn export-pdf nopath px-3">pdf</a></li>
  <li>Une fois le fichier pdf généré, le renommer en <b>monnom_monprenom_synthese_debat.pdf</b></li>
  <li>Une fois le fichier pdf renommé, le transmettre sur l'espace de travail en ligne</li>
</ol>
+++

<br>
<table class="table">
  <tbody>
    <tr>
      <th class="col-1 text-nowrap">Mon nom : </th>
      <td><input class="w-100" placeholder="Saisir votre nom ici" type="text"></td>
    </tr>
    <tr>
      <th class="col-1 text-nowrap">Mon prénom : </th>
      <td><input class="w-100" placeholder="Saisir votre prénom ici" type="text"></td>
    </tr>
  </tbody>
</table>

Le sujet que j'ai choisi est : 
<textarea class="col-12" placeholder="Saisir ici le titre du sujet qui a été abordé pendant le débat"></textarea>

Ma synthèse  : 
<textarea rows="10" class="col-12" placeholder="Rédiger ici votre synthèse. Les critères d'évaluation de votre synthèse sont décrits ci-dessous"></textarea>

## Grille d'évaluation de cette fiche 
<br>

<table class="table table-sm">
  <thead>
    <tr>
      <th></th>
      <th colspan="4" class="text-center">Appréciation (enseignant)</th>
    </tr>
    <tr class="text-center">
      <th class="border-0"></th>
      <th>TP</th>
      <th>P</th>
      <th>B</th>
      <th>TB</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><input type="checkbox"> Ma synthèse explique le point de vue que j'avais avant le débat sur le sujet que j'ai choisi</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><input type="checkbox"> Dans ma synthèse, j'explique si mon point de vue a évolué ou non suite au débat. J'argumente pourquoi.</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><input type="checkbox"> Ma synthèse contient au moins 5 phrases</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><input type="checkbox"> Dans mes phrases, j'ai fais attention à la grammaire et à l'orthographe (je peux m'aider d'un correcteur)</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><input type="checkbox"> J'ai enregistré le fichier sous format pdf. Puis je l'ai renommé sous le format <b>monnom_monprenom_preparation_debat.pdf</b> où :<ul>
        <li>'monnom' correspond à mon nom</li>
        <li>'monprenom' correspond à mon prenom</li>
      </ul></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><input type="checkbox"> J'ai transmis cette fiche avant la date limite de rendu annoncée par l'enseignant</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

<div class="d-print-none">

  <h3>Comment exporter cette fiche en format pdf ? </h3>
  
  <p>Il vous suffit de cliquer sur le bouton : <a type="button" class="px-1 btn btn-dark leftcard-btn export-pdf nopath px-5">pdf</a></p>
  
  <h3>Comment transmettre cette fiche sur l'espace de travail en ligne ? </h3>
  
  <p><a href="/apprenant/autre/aides/comment-_transmettre-_son-_travail-_sur-_l--__espace-_en-_ligne.md" target="_blank">Cette page</a> vous explique comment transmettre un travail sur l'espace de travail en ligne</p>

</div>