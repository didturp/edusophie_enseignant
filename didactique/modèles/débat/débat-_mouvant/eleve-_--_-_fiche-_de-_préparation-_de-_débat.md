# Fiche de préparation de débat

Pour animer un débat dans de bonnes conditions, il doit être préparé et c'est le but de cette fiche de préparation.

Le thème du débat sera  : 

<p class="d-block border border-2 border-dark p-2 rounded text-center h3">Si vous n'avez rien à vous reprocher, vous n'avez rien à cacher</p>
<br>

+++consignes
<b>Vous devez</b>
<ol start="0">
  <li>Indiquer votre nom et prénom</li>
  <li>Répondre aux questions de la fiche</li>
  <li>Une fois les questions répondues, enregistez cette fiche en un fichier pdf en cliquant sur le bouton <a type="button" class="btn btn-dark leftcard-btn export-pdf nopath px-3">pdf</a></li>
  <li>Une fois le fichier pdf généré, le renommer en <b>monnom_monprenom_preparation_debat.pdf</b></li>
  <li>Une fois le fichier pdf renommé, le transmettre sur l'espace de travail en ligne</li>
</ol>
+++

<br>
<table class="table">
  <tbody>
    <tr>
      <th class="col-1 text-nowrap">Mon nom : </th>
      <td><input class="w-100" placeholder="Saisir votre nom ici" type="text"></td>
    </tr>
    <tr>
      <th class="col-1 text-nowrap">Mon prénom : </th>
      <td><input class="w-100" placeholder="Saisir votre prénom ici" type="text"></td>
    </tr>
  </tbody>
</table>

## Préparer le débat

### Notions essentielles

+++question "Qu'est-ce qu'une données personnelle ? répondre en 2 ou 3 phrases maximum"

<textarea rows="4" class="w-100" placeholder="Saisir votre réponse ici (qui ne doit pas être un copié/collé de la source mais écrite avec vos propres mots). Aidez vous d'Internet pour y répondre ! vous pouvez consulter des articles, des vidéos, des podcasts, etc..."></textarea>
<input placeholder="Saisir ici le lien de la principale source qui vous a aidé à répondre (copiez/collez le lien)" class="w-100" type="text">

+++question "Qu'est-ce que le CNIL ? quel est le but de cet organisme ? répondre en 2 ou 3 phrases maximum"

<textarea rows="4" class="w-100" placeholder="Saisir votre réponse ici (qui ne doit pas être un copié/collé de la source mais écrite avec vos propres mots). Aidez vous d'Internet pour y répondre ! vous pouvez consulter des articles, des vidéos, des podcasts, etc..."></textarea>
<input placeholder="Saisir ici le lien de la principale source qui vous a aidé à répondre (copiez/collez le lien)" class="w-100" type="text">

### Préparer des arguments

[Dans ce lien](/s/FH5D9A) se trouve un corpus d'articles qui traitent des données personnelles et de l'anonymat sur Internet. (un grand merci à [Louis Paternault](https://ababsurdo.fr/apropos/) qui a produit ce corpus)

Ce corpus contient beaucoup d'articles, vous n'allez analyser qu'une seule page du corpus. Pour savoir quelle page vous devez analyser, complétez la ligne 2 dans le code python suivant et exécutez le : 

widget python
# la variable mon_nom doit être une variable de type "chaîne de caractères" et doit contenir votre nom
mon_nom = 

mon_nom = mon_nom.lower() # 'mon_nom.lower()' a pour effet de mettre en minuscule toutes les lettres contenues dans la variable mon_nom

premiere_lettre_du_nom = mon_nom[0]

page = None

if(premiere_lettre_du_nom < 'b' ): # si la première lettre de mon nom est en dessous de la lettre 'b'
  page = 1
elif(premiere_lettre_du_nom < 'c' ): # sinon, si la première lettre de mon nom est en dessous de la lettre 'c'
  page = 2
elif(premiere_lettre_du_nom < 'd' ): # ...
  page = 3
elif(premiere_lettre_du_nom < 'e' ):
  page = 4
elif(premiere_lettre_du_nom < 'g' ):
  page = 5
elif(premiere_lettre_du_nom < 'h' ):
  page = 6
elif(premiere_lettre_du_nom < 'j' ):
  page = 7
elif(premiere_lettre_du_nom < 'm' ):
  page = 8
elif(premiere_lettre_du_nom < 'o' ):
  page = 9
elif(premiere_lettre_du_nom < 'q' ):
  page = 10
elif(premiere_lettre_du_nom < 's' ):
  page = 11
else:
  page = 12

print("vous devez consulter le ou les articles du corpus qui se trouvent dans la page n°" + str(page))
widget

+++question "D'après le résultat qui est affiché par le programme python, quelle page devez vous analyser dans le corpus ?"
<input placeholder="Saisir ici le numéro de la page" type="text">

<hr>

Le ou les articles de la page du corpus que vous devez analyser maintenant défendent un point de vue.

+++question "Après lecture du ou des articles de la page du corpus que vous devez analyser, listez au moins deux arguments défendus par chaque article"
<textarea rows="4" class="w-100" placeholder="Saisir ici votre réponse (qui ne doit pas être un copié/collé de la source mais écrite avec vos propres mots)
Article 'nom de l'article'
argument 1 : ...
argument 2 : ..."></textarea>

## Grille d'évaluation de cette fiche 
<br>

<table class="table table-sm">
  <thead>
    <tr>
      <th></th>
      <th colspan="4" class="text-center">Appréciation (enseignant)</th>
    </tr>
    <tr class="text-center">
      <th class="border-0"></th>
      <th>TP</th>
      <th>P</th>
      <th>B</th>
      <th>TB</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><input type="checkbox"> J'ai répondu à toutes les questions avec mes propres mots</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><input type="checkbox"> J'ai indiqué, lorsque c'est demandé, les sources qui m'ont aidé(e) à répondre aux questions</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><input type="checkbox"> J'ai enregistré le fichier sous format pdf. Puis je l'ai renommé sous le format <b>monnom_monprenom_preparation_debat.pdf</b> où :<ul>
        <li>'monnom' correspond à mon nom</li>
        <li>'monprenom' correspond à mon prenom</li>
      </ul></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><input type="checkbox"> J'ai transmis le fichier pdf généré dans l'espace de travail 24 heures avant la séance "débat"</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

<div class="d-print-none">

  <h3>Comment exporter cette fiche en format pdf ? </h3>
  
  <p>Il vous suffit de cliquer sur le bouton : <a type="button" class="px-1 btn btn-dark leftcard-btn export-pdf nopath px-5">pdf</a></p>
  
  <h3>Comment transmettre cette fiche sur l'espace de travail en ligne ? </h3>
  
  <p><a href="/apprenant/autre/aides/comment-_transmettre-_son-_travail-_sur-_l--__espace-_en-_ligne.md" target="_blank">Cette page</a> vous explique comment transmettre un travail sur l'espace de travail en ligne</p>

</div>