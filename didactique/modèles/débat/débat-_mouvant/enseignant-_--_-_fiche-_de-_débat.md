# enseignant : fiche de débat

Cette fiche fournit une marche à suivre et outils accompagnant l'enseignant pour animer un débat de type "débat mouvant".

## Pourquoi un débat mouvant ? 

Une vidéo vaut mieux qu'un grand pavé de texte, youtube fourmille de vidéo explicatives sur ce type de déabt. En voici une ci-dessous très courte et qui présente rapidement ce qu'est un débat mouvant :

<div class="d-flex">
<iframe class="mx-auto" src="https://www.youtube-nocookie.com/embed/BShj1VsNQ74" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>
</div><br>

Maintenant quels sont les apports de ce type de débat ? En voici quelques un :
- L'organisation de ce type de débat est assez simple, les règles sont simples et peu nombreuses
- Tout le monde s'implique (positionnement spatial)
- On se dégourdit les jambes 

## Préparation du débat

Les élèves sont invités à remplir chez eux, et individuellement, une fiche de préparation de débat. À travers cette fiche, les élèves abordent les principales notions-clé en lien avec le ou les sujet du débat. Cette fiche doit être transmise par la suite à l'enseignant avant la séance dédiée au débat.

Un exemple de squelette de fiche de préparation [se trouve ici](fiche-_de-_preparation-_de-_debat.md)

## Sujets du débat

<textarea rows="5" class="col-12" placeholder="Voici quelques exemples de sujets. Dans mon cas, j'anime le débat autout de plusieurs sujets que j'annonce au fur et à mesure pendant la séance
_ sujet 0 : Si vous n'avez rien à vous reprocher, vous n'avez rien à cacher
_ puis sujet 1 :  L’anonymat sur Internet favorise les formes de violences
_ puis sujet 2 :  Donc il faut interdire l’anonymat sur Internet"></textarea>

## Étapes du débat

<style>
  b.textcode{
    color:green;
    padding: 0.25rem !important;
    margin: 0.25rem !important;
    border-radius: 0.25rem !important;
    border:1px solid blue;
  }
</style>

<br>
<table class="table table-sm">
  <thead>
    <tr>
      <th class="col-1 text-center text-nowrap">étape</th>
      <th class="col-10 text-center text-nowrap">contenu</th>
      <th class="col-1 text-center text-nowrap">terminée ?</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center">0</td>
      <td class="px-2">Expliquer aux élèves dans les grandes lignes le déroulement du débat : <ul>
        <li>Un à plusieurs sujets sont abordés</li>
        <li>Le ou les sujets exposés sont volontairement clivants / insolubles</li>
        <li>Le but n'est pas d'obtenir une réponse au(x) sujet(s) mais de construire sa réflexion</li>
        <li>Pour prendre la parole et annoncer un argument, vous devez lever la main</li>
        <li>À l'issue de ce débat, chaque élève devra rédiger à la maison une synthèse sur un des sujets traités pendant le débat</li>
      </ul></td>
      <td class="text-center"><input type="checkbox"></td>
    </tr>
    <tr>
      <td class="text-center">1</td>
      <td class="px-2"><p>Anoncer le sujet n°<b class="textcode">i</b> et pendant combien de temps il va être traité</p></td>
      <td class="text-center"><input type="checkbox"></td>
    </tr>
    <tr>
      <td class="text-center">2</td>
      <td class="px-2">Désigner un élève maître du temps : cet élève devra surveiller et annoncer la fin du temps pour le sujet en cours</td>
      <td class="text-center"><input type="checkbox"></td>
    </tr>
    <tr>
      <td class="text-center">3</td>
      <td class="px-2">Séparer dans la salle les élèves en deux groupes; un groupe d'élèves '(plutôt) pour' et un groupe d'élèves '(plutôt) contre'</td>
      <td class="text-center"><input type="checkbox"></td>
    </tr>
    <tr>
      <td class="text-center">4</td>
      <td class="px-2">Animer le débat :<ul>
        <li>Donner la parole</li>
        <li>Noter les arguments concaincants ( = les argument qui font bouger des élèves d'un groupe) : <ul>
          <li>Noter également le nombre d'élève qui change de groupe</li>
          <li><b>=> Un programme python est mis à disposition ci-dessous pour aider l'enseignant à la prise de notes pendant le débat</b></li>
        </ul></li>
        <li>Participer au débat : <ul>
          <li>Si il y a un fort déséquilibre entre les deux groupes</li>
          <li>Si il y a peu de prise de parole</li>
        </ul></li>
        <li>Aider les élèves à distinguer opinion de fait</li>
      </ul>
      </td>
      <td class="col-1 text-center"><input type="checkbox"></td>
    </tr>
    <tr>  
      <td class="col-1 text-center">5</td>
      <td class="px-2">Si il y a plusieurs sujets, incrémentez <b class="textcode">i</b> de 1 et revenir à l'étape 1 ;)<ul></ul></td>
      <td class="col-1 text-center"><input type="checkbox"></td>
    </tr>
    <tr>
      <td class="col-1 text-center">6</td>
      <td class="px-2">Clore le débat, demander aux élèves ce qu'ils ont appris pendant le débat</td>
      <td class="col-1 text-center"><input type="checkbox"></td>
    </tr>
  </tbody>
</table>

## Prise de note pendant le débat

Je propose ici à l'enseignant d'endosser le rôle de rapporteur et de noter, pendant le débat, les informations suivantes : 
- Un argument qui convainc ( = un argument qui fait venir des dans le groupe où se trouve l'élève qui a annoncé son argument)
    - Plus le nombre d'élève qui changent de groupe pour entrer dans celui où se trouve l'élève qui vient d'argumenter

Ci-dessous est mise à disposition un programme python qu'il faut compléter avec les informations mentionnées ci-dessus et qui génère quelques données à afficher en fin de débat et qui seront utilent aux élèves pour la redaction de leur syntèse.

### Programme python de prise de notes durant le débat (squelette)

widget python
class Sujet:
  def __init__(self, nomDuSujet:str):
    self.nomDuSujet = nomDuSujet
    self.arguments = {}
  def ajouterArgument(self, argument:str, nombreElevesConvaincus:int):
    self.arguments[argument] = str(nombreElevesConvaincus)
    return self
  def __str__(self):
    return self.nomDuSujet

# Sujet 0
sujet0 = Sujet("Nom du sujet 0")
sujet0
sujet0.ajouterArgument("argument 0 ...", 1)
sujet0.ajouterArgument("argument 1 ...", 3)
sujet0.ajouterArgument("argument 2 ...", 2)

sujets = [sujet0]

print("### Synthèse ###")

csv = "nom du sujet;argument;nombre de personnes convaincues\n"
for sujet in sujets:
  print("\n## Sujet : '",sujet,"'\n")
  print("Les 3 meilleurs arguments :")
  arguments = sorted(sujet.arguments.items(), key=lambda x: x[1], reverse=True)
  max = 0
  for argument in arguments:
    if(max < 3):
      print(argument[1] + " : " + argument[0])
    csv += sujet.nomDuSujet.replace(";", ":") + ";" + argument[0].replace(";", ":") + ";" + argument[1] + "\n"
    max += 1

print("\n\n### données des argument en format CSV ###\n")
print(csv)
widget

#### Programme python de prise de notes durant le débat (exemple)

widget python
class Sujet:
  def __init__(self, nomDuSujet:str):
    self.nomDuSujet = nomDuSujet
    self.arguments = {}
  def ajouterArgument(self, argument:str, nombreElevesConvaincus:int):
    self.arguments[argument] = str(nombreElevesConvaincus)
    return self
  def __str__(self):
    return self.nomDuSujet

# Sujet 0
sujet0 = Sujet("Si vous n'avez à vous reprocher, vous n'aviez rien à cacher")
sujet0.ajouterArgument("Oui, les personnes qui ont des choses à cacher sont forcemment malhonnêtes", 1)
sujet0.ajouterArgument("Non, les entreprises peuvent faire un usage malveillant de nos données, par exemple...", 3)

# Sujet 1
sujet1 = Sujet("L’anonymat sur Internet favorise les formes de violences")
sujet1.ajouterArgument("Non, l'anonymat est un faux problème, la solution se trouve dans l'éducation", 3)
sujet1.ajouterArgument("Oui, il y a eu ce cas X au moment T qui montre que l'anonymat rend violent", 4)

# Sujet 2
sujet2 = Sujet("Donc il faut interdire l’anonymat sur Internet")
sujet2.ajouterArgument("Non, les lanceurs d'alerte ne pourraient plus travailler", 5)
sujet2.ajouterArgument("Oui, ça permettra de banir toute forme de violence sur Internet", 2)

sujets = [sujet0, sujet1, sujet2]

print("### Synthèse ###")

csv = "nom du sujet;argument;nombre de personnes convaincues\n"
for sujet in sujets:
  print("\n## Sujet : '",sujet,"'\n")
  print("Les 3 meilleurs arguments :")
  arguments = sorted(sujet.arguments.items(), key=lambda x: x[1], reverse=True)
  max = 0
  for argument in arguments:
    if(max < 3):
      print(argument[1] + " : " + argument[0])
    csv += sujet.nomDuSujet.replace(";", ":") + ";" + argument[0].replace(";", ":") + ";" + argument[1] + "\n"
    max += 1

print("\n\n### données des argument en format CSV ###\n")
print(csv)

widget

## Synthèse du débat

Les élèves doivent rédiger une mini-rédaction sur un des sujets abordés dans le cadre du débat. Ils pourront s'appuyer sur leur feuille de préparation de débat ainsi que du fichier csv généré à l'issue du débat qui leur sera distribué.

Un exemple de squelette de fiche de synthèse de débat [se trouve ici](eleve-_--_-_fiche-_de-_syntese-_de-_debat.md)

## Si le débat termine avant l'heure ?
<br>
<textarea rows="5" class="col-12" placeholder="Saisir ici ce qui est prévu si le débat se termine avant la fin de la séance"></textarea>

