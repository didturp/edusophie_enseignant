# Projet "Galerie de photo"

+++sommaire 1

## Sujet du projet

Un client, nommé Lucas, vous demande de développer un site Internet affichant une galerie de photos. Ci-dessous, voici les principaux échanges qui ont eu lieu pendant la réunion : 

+++bulle lucas
  je ne peux pas vous fournir pour l'instant les photos !
+++

+++bulle matthieu droite
  pas de problème ! nous pouvons travailler dès maintenant en utilisant des photos factices en attendant ! 
+++

+++bulle lucas
  super !
+++

<br>

+++bulle lucas
  j'aimerai que les photos soient disposées dans une grille sous forme de card contenant quelques informations
+++

+++bulle lucas droite
  voici ce que ça donne si je griffonne ça sur un tableau
+++

<div class="d-flex justify-content-around">
  
  <div class="col-5">
    <img class="w-100" src="/images/md/9939CFC962A299HB3B8EB78CEG9AC7">
    <div class="text-center">vue générale</div>
  </div>
  
  <div class="col-5">
    <img class="w-100" src="/images/md/E435BD285BABCBH482A57HEHG55HHC">
    <div class="text-center">détaillé d'une 'card'</div>
  </div>

</div>

<br>

Durant le reste de la réunion, Lucas décrit les fonctionnalités qu'il aimerai avoir sur son site. Fonctionnalités qui sont négociées avec Matthieu, le chef de projet, en fonction du temps et de l'argent dont dispose Lucas. 

Le chef de projet rédige ensuite les fonctionnalités retenues dans le bloc "Spécifications fonctionnelles" qui se trouvent ci-dessous. 

## Spécifications fonctionnelles

Les spécifications fonctionnelles liste les fonctionnalités attendues de l'application ou du site Internet. Cette partie du document doit être compréhensible par n'importe quelle personne, qu'elle soit informaticienne ou non.

0. Lorsqu'un visiteur accède à la page Internet, il doit pouvoir voir l'ensemble des photos de Lucas
1. Sur chacune des photos, le visiteur peut lire un titre, une description ainsi qu'un mot-clé
2. Lorsque l'utilisateur clique sur une photo, la photo doit être zoomée (et ainsi permettre au visiteur de regarder en détail la photo)
3. (optionnel) si le temps le permet, l'aspect visuel de la page Internet peut être amélioré (ajout d'animations css et/ou javascript, prise en charge de l'affichage sur smartphone et sur ordinateur, ...)

## Spécifications techniques

Les spécifications techniques décrivent comment, techniquement, les développeurs doivent programmer l'application ou le site Internet afin de satisfaire les spécifications fonctionnelles. Cette partie est en général rédigée par un développeur expérimenté.

### Architecture générale 

<img src="/images/md/B7B9245795EE2EEG8CGDE9994BD7GE">

<br>
#### Répartition des rôles 


+++imageFloat /images/md/C5G49G6BHB43DA5C639H36EH9H34FF
Un <b>développeur backend</b> s'occupera du serveur qui doit fournir les données des photos
+++

+++imageFloat /images/md/FHE5DD822G3G39D7FA8E842E336F7A droite
Un <b>développeur frontend</b> s'occupera de la mise en forme de la page Internet index.html
+++


+++pagebreak

# fiche rôle "développeur backend"

+++imageFloat /images/md/D5G69B8CBGEH5FFH332C273HEG9BH9
Dans l'industrie informatique, le(la) développeur(se) backend est un(e) informaticien(e) spécialisé(e) dans le traitement et le transfert des données. Avec de l'expérience, cet(te) architecte des données est capable de concevoir et de développer des systèmes d'informations complexes. 
+++

<table class="table table-sm">
  <thead>
    <tr class="text-center"></tr>
  </thead>
  <tbody>
    <tr>
      <th class="align-middle text-nowrap">Responsable(s) dans le groupe : +++emoji-writing_hand </th>
      <td class="w-100"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm">
  <thead>
    <tr class="text-center"><th colspan="10">Objectif(s) principal(aux)</th></tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center align-middle">4 point(s)</td>
      <td class="col-10 align-middle">Le serveur doit transmettre, en plus de chaque photo, une description par photo</td>
      <td class="col-1"></td>
    </tr>
    <tr>
      <td class="text-center align-middle">4 point(s)</td>
      <td class="col-10 align-middle">Le serveur doit transmettre, en plus de chaque photo, un mot-clé par photo</td>
      <td class="col-1"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm">
  <thead>
    <tr class="text-center"><th colspan="10">Objectif(s) secondaire(s)</th></tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center align-middle">2 point(s)</td>
      <td class="col-10 align-middle">Les données envoyées par le serveur doivent être générées grâce à la librairie python Faker (<a href="https://faker.readthedocs.io/en/master/" target="_blank">https://faker.readthedocs.io/en/master</a>)</td>
      <td class="col-1"></td>
    </tr>
  </tbody>
</table>

<p class="text-center h2">Comment démarrer ?</p>

<p class="text-center h3">Préparer votre environnement de travail</p>

0. Depuis un terminal, créez vous un dossier du jours : '`mkdir /home/premiere/Eleves/nom_prenom/seances/18_10_21`'
    - **Attention !** pensez à modifier la commande ! (nom_prenom ainsi que la date du jour)
1. Déplacez vous dans le dossier du jours : '`cd /home/premiere/Eleves/nom_prenom/seances/18_10_21`'
2. Téléchargez dans le dossier, dans lequel votre terminal est, l'archive 'backend' : '`wget https://www.edusophie.com/fichiers/6HH975788HD9EC9GD4DB29A57HG75B`'
    - **Attention !** vérifiez bien d'abord que votre terminal se trouve dans votre dossier du jour
3. Décompressez l'archive : '`unzip 6HH975788HD9EC9GD4DB29A57HG75B`'
4. Ouvrez le fichier 'server.py' avec thonny : '`thonny backend/server.py`'

<p class="text-center h3">Démarrez le serveur de données</p>

5. Assurez vous qu'un cable éternet soit bien connecté sur votre ordinateur
6. Exécutez le script 'server.py' depuis thonny, voila ce que doit afficher le terminal de thonny : 

<img src="/images/md/4899H9G3955EBF83CC249B4AD72BHH">

7. Affichez votre adresse ip locale (qui est l'adresse utilisée par votre serveur de données) à l'aide de la commande : '`hostname –I`'

<div class="text-center mx-auto">
  <img src="/images/md/CHD55DGBGG7B5B5AACC6DBDCC299A2">
  <div>L'adresse IP peut varier</div>
</div>

8. Ouvrir le navigateur Internet Firefox, saisissez et validez l'url : xxx.xxx.xxx.xxx:9000 (où xxx.xxx.xxx.xxx est l'adresse IP de votre serveur de données trouvée dans l'étape précédente). Voilà ce qui doit afficher votre navigateur : 

<img src="/images/md/874DF2HDCCA3D78H84G469F2D74AA5">

Votre serveur de données fonctionne parfaitement ! 

9. Aidez votre développeur(se) frontend à terminer toutes ses étapes jusqu'à son étape n°7

<p class="text-center h3">Indications pour le travail côté backend</p>
- Lorsque vous souhaitez tester des modifications que vous avez apportées dans le fichier 'server.py', vous devez stopper le serveur (bouton rouge 'stop' tout en haut de thonny) et le redémarrer.
    - En stoppant le serveur, votre développeur frontend ne pourra plus charger correctement la page Internet de son côté, communiquez entre vous pour vous prévenir !
- Pour débugger votre code, gardez un oeil sur la console de thonny et utilisez/rafraichissez dans le navigateur Firefox l'url utilisée dans l'étape n°8
- Pour remplir vos objectifs principaux, vous devez analyser le code 'server.py' et trouver les lignes de codes qui contiennent les données liées aux photos (l'url de la photo et son titre)
    - une fois que vous avez trouvez ces lignes, essayez d'ajouter de nouvelles photos (url + titre)
    - ensuite, et toujours au niveau des lignes de code que vous avez trouvé, modifiez le code pour ajouter de nouveaux types de données tels que la description d'une photo et le mot-clé rattaché à cette photo. N'oubliez pas de rafraîchir la page sur votre navigateur pour voir si les données émises par le serveur de données correspondent à ce que vous souhaitez

+++pagebreak

# fiche rôle "développeur frontend"

+++imageFloat /images/md/E57H7EA647DH54G3GH8H5EA93BF84A
Dans l'industrie informatique, le(la) développeur(se) frontend est un(e) informaticien(e) spécialisé(e) dans la création d'interface graphiques soignées et ergonomiques . Avec de l'expérience, il ou elle est capable de concevoir et de développer des interfaces graphiques animées et qui s'adaptent à tous les appareils (ordinateur, tablette, ...). 
+++

<table class="table table-sm">
  <thead>
    <tr class="text-center"></tr>
  </thead>
  <tbody>
    <tr>
      <th class="align-middle text-nowrap">Responsable(s) dans le groupe : +++emoji-writing_hand </th>
      <td class="w-100"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm">
  <thead>
    <tr class="text-center"><th colspan="10">Objectif(s) principal(aux)</th></tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center align-middle">4 point(s)</td>
      <td class="col-10 align-middle">ajouter l'information 'titre' dans une 'card'
      <ul>
        <li>Aide : ligne(s) de code à ajouter entre les lignes 42 et 53 du fichier 'script.js'</li>
      </ul></td>
      <td class="col-1"></td>
    </tr>
    <tr>
      <td class="text-center align-middle">2 point(s)</td>
      <td class="col-10 align-middle">changer le titre de la page "Galerie d'image" par "Mes photos"</td>
      <td class="col-1"></td>
    </tr>
    <tr>
      <td class="text-center align-middle">1 point(s)</td>
      <td class="col-10 align-middle">colorer et centrer le titre principal de la page 
      <ul>
        <li>Aide 1 : rechercher sur Internet 'how color text css'</li>
        <li>Aide 2 : rechercher sur Internet 'how center title css'</li>
        <li>Aide 3 : ligne(s) de code à ajouter au niveau de la ligne 24 du fichier 'style.css'</li>
      </ul></td>
      <td class="col-1"></td>
    </tr>
    <tr>
      <td class="text-center align-middle">1 point(s)</td>
      <td class="col-10 align-middle">ajouter une bordure colorée autour de chaque 'card'<ul>
        <li>Aide : pour cela il faut modifier le fichier 'style.css'</li>
      </ul></td>
      <td class="col-1"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm">
  <thead>
    <tr class="text-center"><th colspan="10">Objectif(s) secondaire(s)</th></tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center align-middle">2 point(s)</td>
      <td class="col-10 align-middle">Utilisez la bibliothèque javascript (<a href="https://tholman.com/intense-images/" target="_blank">https://tholman.com/intense-images/</a>) qui permet au visiteur du site de zoomer sur une image en cliquant dessus</td>
      <td class="col-1"></td>
    </tr>
  </tbody>
</table>

<p class="text-center h2">Comment démarrer ?</p>

<p class="text-center h3">Préparer votre environnement de travail</p>

0. Depuis un terminal, créez vous un dossier du jours : '`mkdir /home/premiere/Eleves/nom_prenom/seances/18_10_21`'
    - **Attention !** pensez à modifier la commande ! (nom_prenom ainsi que la date du jour)
1. Déplacez vous dans le dossier du jours : '`cd /home/premiere/Eleves/nom_prenom/seances/18_10_21`'
2. Téléchargez dans le dossier, dans lequel votre terminal est, l'archive 'frontend' : '`wget https://www.edusophie.com/fichiers/4A774779D5D6ED279837G24B67F372`'
    - **Attention !** vérifiez bien d'abord que votre terminal se trouve dans votre dossier du jour
3. Décompressez l'archive : '`unzip 4A774779D5D6ED279837G24B67F372`'
4. Ouvrez dans gedit les principaux fichiers sur lesquels vous allez travailler : '`gedit frontend/index.html frontend/assets/style.css frontend/assets/script.js`'
    - **Attention !** n'oubliez pas d'utilisez la touche 'tabulation' pour compléter vos chemins pendant que vous tapez la commande 

<p class="text-center h3">Synchronisez vous avec votre développeur(se) backend</p>

5. Aidez votre développeur(se) backend à terminer toutes ses étapes jusqu'à son étape n°8 comprise
6. Dans le fichier 'script.js' ouvert dans gedit, modifiez la ligne :

'`let contenusEnFormatDeDonnees = getContentsFromServer('http://xxx.xxx.xxx.xxx:9000/')`'

=> Remplacez '`xxx.xxx.xxx.xxx `' par l'IP du serveur de données

7. Ouvrez le fichier 'index.html' (pour rappel : situé dans le dossier frontend) sur Firefox. Voilà ce qui doit afficher votre navigateur : 

<img src="/images/md/9245D4E2A9G3EA72CB598A6BA8C9B4">

<p class="text-center h3">Indications pour le travail côté frontend</p>

- Les lignes de code des fichiers 'style.css' et 'script.js' sont très largement commentés
- Gardez toujours ouvert la console de l'inspecteur de votre navigateur et n'hésitez pas à utiliser `console.log(...)` dans le fichier 'script.js' pour debugger votre code
- Explication sur les fichiers 'index.html', 'style.css' et 'script.js'
    - 'index.html' : contient le code HTML principal de la page Internet
    - 'style.css' (situé dans le dossier 'assets'): contient le code CSS qui décore la page Internet, il sera nécessaire d'y ajouter et/ou d'y modifier du code pour remplir vos objectifs 
    - 'script.js' (situé dans le dossier 'assets') : contient le code Javascript qui collecte les données du serveur de données et créé dynamiquement du code HTML en conséquence. Il sera nécessaire d'y ajouter et/ou d'y modifier du code pour remplir vos objectifs 
- Concernant l'objectif secondaire : la bibliothèque 'intense' est déjà installée dans le header de la page 'index.html', il vous faut trouver comment utiliser cette bibliothèque en consultant la documentation (rappel du lien : <a href="https://tholman.com/intense-images/" target="_blank">https://tholman.com/intense-images/</a>)


+++pagebreak

