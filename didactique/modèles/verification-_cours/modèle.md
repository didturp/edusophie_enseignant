# Evaluation de support de cours

<div style="font-size:0.7rem">

<table class="table table-sm text-center align-middle mb-2">
  <tbody>
    <tr>
        <td colspan="10" class="text-nowrap" style="width:0"><b>Méta-données</b></td>
    </tr>
    <tr>
        <td class="text-nowrap col-1">Date</td><td><input class="d-print-none w-100"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm mb-2">
  <thead>
    <tr>
      <th colspan="10" class="text-nowrap text-center" style="width:0"><b>Général 1/2</b></th>
    </tr><tr>
  </tr></thead>
  <tbody>
    <tr>
        <td class="col-8"></td>
        <td class="text-center col-1">non</td>
        <td class="text-center col-1">oui</td>
    </tr>
    <tr>
      <td class="col-10">Les différents modules sont rangés dans l'ordre</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm mb-2">
  <thead>
    <tr>
      <th colspan="10" class="text-nowrap text-center" style="width:0"><b>Module 00</b></th>
    </tr><tr>
  </tr></thead>
  <tbody>
    <tr>
        <td class="col-8" colspan="2"></td>
        <td class="text-center col-1">non</td>
        <td class="text-center col-1">oui</td>
    </tr>
    <tr>
      <td class="col-9" colspan="2">Les différents cours sont rangés dans l'ordre</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
    <td class="col-1 text-center align-middle" rowspan="3"><b>Cours 00</b></td>
      <td class="col-9">le cours est titré et daté</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">le cours contient le ou les blocs résumons ✍️ (photocopié ou stylo) (nbr: 3)</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">les réponses  ✍️  des activités (#3->3) sont corrigées au stylo</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-1 text-center align-middle" rowspan="3"><b>Cours 01</b></td>
      <td class="col-9">le cours est titré et daté</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">le cours contient le ou les blocs résumons ✍️ (photocopié ou stylo) (nbr: 3)</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">les réponses  ✍️  des activités (#0->6, #1->5, #2->7) sont corrigées au stylo</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-1 text-center align-middle" rowspan="3"><b>Cours 02</b></td>
      <td class="col-9">le cours est titré et daté</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">le cours contient le ou les blocs résumons ✍️ (photocopié ou stylo) (nbr: 1)</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">les réponses  ✍️  des activités (#0->7) sont corrigées au stylo</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-1 text-center align-middle" rowspan="3"><b>Cours 03</b></td>
      <td class="col-9">le cours est titré et daté</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">le cours contient le ou les blocs résumons ✍️ (photocopié ou stylo) (nbr: 2)</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">les réponses  ✍️  des activités (#0->4, #1->4) sont corrigées au stylo</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm mb-2">
  <thead>
    <tr>
      <th colspan="10" class="text-nowrap text-center" style="width:0"><b>Module 01</b></th>
    </tr><tr>
  </tr></thead>
  <tbody>
    <tr>
        <td class="col-8" colspan="2"></td>
        <td class="text-center col-1">non</td>
        <td class="text-center col-1">oui</td>
    </tr>
    <tr>
      <td class="col-9" colspan="2">Les différents cours sont rangés dans l'ordre</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
    <td class="col-1 text-center align-middle" rowspan="3"><b>Cours 00</b></td>
      <td class="col-9">le cours est titré et daté</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">le cours contient le ou les blocs résumons ✍️ (photocopié ou stylo) (nbr: 2)</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">les réponses ✍️ des activités (#0->3) sont corrigées au stylo</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-1 text-center align-middle" rowspan="3"><b>Cours 01</b></td>
      <td class="col-9">le cours est titré et daté</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">le cours contient le ou les blocs résumons ✍️ (photocopié ou stylo) (nbr: 3)</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">les réponses ✍️ des activités (#0->2, #1->3, #2->3) sont corrigées au stylo</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm mb-2">
  <thead>
    <tr>
      <th colspan="10" class="text-nowrap text-center" style="width:0"><b>Module 02</b></th>
    </tr><tr>
  </tr></thead>
  <tbody>
    <tr>
        <td class="col-8" colspan="2"></td>
        <td class="text-center col-1">non</td>
        <td class="text-center col-1">oui</td>
    </tr>
    <tr>
      <td class="col-9" colspan="2">Les différents cours sont rangés dans l'ordre</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
    <td class="col-1 text-center align-middle" rowspan="3"><b>Cours 00</b></td>
      <td class="col-9">le cours est titré et daté</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">le cours contient le ou les blocs résumons ✍️ (photocopié ou stylo) (nbr: 1)</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">les réponses ✍️ des activités (#0->6, #1->1) sont corrigées au stylo</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-1 text-center align-middle" rowspan="3"><b>Cours 01</b></td>
      <td class="col-9">le cours est titré et daté</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">le cours contient le ou les blocs résumons ✍️ (photocopié ou stylo) (nbr: 1)</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">les réponses ✍️ des activités (#0->2) sont corrigées au stylo</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm mb-2">
  <thead>
    <tr>
      <th colspan="10" class="text-nowrap text-center" style="width:0"><b>Module 03</b></th>
    </tr><tr>
  </tr></thead>
  <tbody>
    <tr>
        <td class="col-8" colspan="2"></td>
        <td class="text-center col-1">non</td>
        <td class="text-center col-1">oui</td>
    </tr>
    <tr>
      <td class="col-9" colspan="2">Les différents cours sont rangés dans l'ordre</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
    <td class="col-1 text-center align-middle" rowspan="3"><b>Cours 00</b></td>
      <td class="col-9">le cours est titré et daté</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">le cours contient le ou les blocs résumons ✍️ (photocopié ou stylo) (nbr: 2)</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">les réponses ✍️ des activités (#0->7, #1->4) sont corrigées au stylo</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-1 text-center align-middle" rowspan="3"><b>Cours 01</b></td>
      <td class="col-9">le cours est titré et daté</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">le cours contient le ou les blocs résumons ✍️ (photocopié ou stylo) (nbr: 1)</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
    <tr>
      <td class="col-9">les réponses ✍️ des activités (#0->2, #1->4) sont corrigées au stylo</td>
      <td class="col-1"></td><td class="col-1"></td>
    </tr>
  </tbody>
</table>

<table class="table text-center align-middle">
  <tbody>
    <tr>
        <td colspan="10" class="text-nowrap" style="width:0"><b>Bilan</b></td>
    </tr>
    <tr>
        <td colspan="2">
          <textarea class="w-100" rows="5"></textarea>
        </td>
    </tr>
    <tr>
        <td class="col-6">Le support de cours doit il être représenté ? <br><input type="checkbox"> non <input type="checkbox"> oui </td>
        <td class="col-6 text-start">Si oui, la date butoir pour représenter le cours est  :  </td>
    </tr>
  </tbody>
</table>

</div>