# Progression et programmation

## Liste des thèmes

<style>
  .t1{color:#67b021}
  .t2{color:#069668}
  .t3{color:#b25ea8}
  .t4{color:#081394}
  .t5{color:#932226}
  .t6{color:#444444}
  .t7{color:#e114dc}
  
  .modEven{background-color:#baffab !important;vertical-align: middle}
  .modOdd{background-color:#abfbff !important;vertical-align: middle}
  .vac{background-color:#ffabfb !important;vertical-align: middle}
  
  table td ul{
    margin-bottom:0px;
  }
  
  thead > tr {
    position: sticky;
    top: 0;
    background-color: #edecec;
    box-shadow: 0 4px 2px -1px rgba(0, 0, 0, 0.4);
  }
  table {
    border-collapse: separate; /* Don't collapse */
    border-spacing: 0;
  }
  
</style>

<ol>
  <li class="t1">Nom du theme du programme officiel 1</li>
  <li class="t2">Nom du theme du programme officiel 2</li>
  <li class="t3">...</li>
  <li class="t4">...</li>
  <li class="t5">...</li>
  <li class="t6">...</li>
  <li class="t7">...</li>
</ol>

**[? à effacer ?]** Le thème 'Histoire de l’informatique' étant transverse. Il n'est pas détaillé dans le tableau de la progression. 

## Progression

Chaque module contient un ensemble de capacités appartenant à différents thèmes. Voici la liste des modules :

- Module 00 : **Nom du module 00**
- Module 01 : **Nom du module 01**
- Module 02 : **...** (projet)
- Module 03 : **...**
- Module 04 : **...**
- Module 05 : **...** (projet)
- Module 06 : **...**
- Module 07 : **...**
- Module 08 : **...**
- Module 09 : **...**
- Module 10 : **...**
- Module 11 : **Module de fin d'année** (projetS)

## Programmation

<table class="table">
<thead>
<tr>
    <th scope="col" class="text-vertical px-0 text-center">semaine</th>
    <th scope="col" class="text-vertical px-0 text-center">séance</th>
    <th class="w-100 align-middle text-center" scope="col">contenu</th>
</tr>
</thead>
<tbody>
  <tr><td class="modEven" colspan="3"><div class="float-start"><b>[ Module 00 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>36</td><td>0</td>
    <td>
    <ul>
      <li class="t6">Capacité theme 6 du programme</li>
    </ul></td>
  </tr>
  <tr><td>36</td><td>1</td>
    <td><ul>
      <li class="t6">Capacité theme 6 du programme</li>
    </ul></td>
  </tr>
    <tr><td>37</td><td>2</td>
    <td><ul>
      <li class="t5">Capacité theme 5 du programme</li>
      <li class="t5">Capacité theme 5 du programme</li>
    </ul></td>
  </tr><tr><td>37</td><td>3</td>
    <td><ul>
      <li class="t6">Capacité theme 6 du programme</li>
    </ul></td>
  </tr>
  <tr><td>38</td><td>4</td>
    <td><ul>
      <li class="t5">Capacité theme 5 du programme</li>
      <li class="t6">Capacité theme 6 du programme</li>
      <li class="t6">Capacité theme 6 du programme</li>
    </ul></td>
  </tr><tr><td>38</td><td>5</td>
    <td><ul>
      <li class="t6">Capacité theme 6 du programme</li>
      <li class="t6">Capacité theme 6 du programme</li>
    </ul><div><i>+ exercices de programmation pendant une heure pour préparer le contrôle de la séance suivante</i></div>
    </td>
  </tr>
  <tr><td>39</td><td>6</td>
    <td>évaluation sommative : <b>[ Module 00 ]</b></td>
  </tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Module 01 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>39</td><td>0</td>
    <td><ul>
      <li class="t5">Capacité theme 5 du programme</li>
      <li class="t1">Capacité theme 1 du programme</li>
    </ul></td>
  </tr>
  <tr><td>40</td><td>1</td>
    <td><ul>
      <li class="t4">Capacité theme 4 du programme</li>
      <li class="t4">Capacité theme 4 du programme</li>
    </ul></td>
  </tr>
  <tr><td class="modEven" colspan="3"><div class="float-start"><b>[ Module 02 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>40</td><td>0</td>
    <td><ul>
      <li class="t2">Capacité theme 2 du programme</li>
    </ul></td>
  </tr>
  <tr><td>41</td><td>1</td>
    <td>évaluation sommative : <b>[ Modules ≤ 01 ]</b></td>
  </tr>
  <tr><td>41</td><td>2</td>
    <td><ul>
      <li class="t4">Capacité theme 4 du programme</li>
      <li class="t6">Capacité theme 6 du programme</li>
    </ul></td>
  </tr>
  <tr><td>42</td><td>3</td>
    <td class="align-middle" rowspan="2">Projet noté : <b>[ Modules ≤ 02 ]</b>
      <ul class="text-start mb-0"><li class="t6">Capacité theme 6 du programme</li></ul></td>
  </tr>
  <tr><td>42</td><td>4</td></tr>
  <tr><td colspan="3" class="vac"><b>Vacances de Toussaint</b></td></tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Module 03 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>45</td><td>0</td>
    <td><ul>
      <li class="t2">Capacité theme 2 du programme</li>
      <li class="t7">Capacité theme 7 du programme</li>
    </ul></td>
  </tr>
  <tr><td>45</td><td>1</td>
    <td><ul>
      <li class="t6">Capacité theme 6 du programme</li>
      <li class="t6">Capacité theme 6 du programme</li>
    </ul></td>
  </tr>
  <tr><td>46</td><td>1</td>
    <td><ul>
      <li>...</li>
      <li>...</li>
    </ul></td>
  </tr>
  <tr><td>...</td><td>...</td>
    <td>...</td>
</tr></tbody>
</table>

## État des lieux de la programmation

### Entre le début de l'année et Noël : 
- 13 semaines sans interruption (sans jour(s) férié(s), ..) se sont déroulées 
- XX capacités sont travaillées / évaluées  pour YY séances (ration : ~Z,ZZZZZ capa/séance)
- sur YY séances, AA séances sont prévues pour des projets (entre ~1/C et ~1/D du temps, au lieu de ~1/4 comme explicité dans le programme officiel) : 
    - Après noël, les projets devront être beaucoup plus présents !

### Restant de Noel à fin Mai : 
- environ FF séances restantes / GG semaines  (sans compter Juin)
- HH - XX = II capacités à travailler et évaluer (ratio : ~= J.JJJJ capa/séance)
    - Ce faible ratio laissera plus de place pour des projets 
- au rythme de Z.ZZZZ capa/séance (dans le meilleur des mondes),~KK séances suffiraient pour tout boucler au lieu de FF

## Mises à jours de la programmation

<table>
  <thead>
    <tr class="text-center">
      <th>Date</th>
      <th>Description</th>
      <th>Modification</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>14/09/2020</td>
      <td><ul>
        <li>...</li>
        <li>...</li>
      </ul></td>
      <td><ul>
        <li>...</li>
        <li>...</li>
      </ul></td>
    </tr>
    <tr>
      <td></td>
      <td><ul>
        <li></li>
      </ul></td>
      <td><ul>
        <li></li>
      </ul></td>
    </tr>
  </tbody>
</table>