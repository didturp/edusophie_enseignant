# Progression double flux

<table class="table text-center">
<thead>
<tr>
    <th scope="col" class="text-vertical px-0 text-center">semaine</th>
    <th scope="col" class="text-vertical px-0 text-center">séance(s)</th>
    <th class="d-print-none text-vertical px-0 text-center" scope="col">didactique</th>
    <th class="d-print-none text-vertical px-0 text-center" scope="col">support</th>
    <th class="w-100" scope="col"></th>
</tr>
</thead>
<tbody>
  <tr>
    <td rowspan="4">0</td>
    <td rowspan="2">0</td>
    <td rowspan="2" class="d-print-none"><a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a></td>
    <td rowspan="2" class="d-print-none">
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a><br>
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a>
    </td>
    <td class="text-left"><a href="" target="_blank"><i class="fas fa-folder isDirectory"></i> 0. titre séquence</a></td>
  </tr>
    <tr><td><ul class="text-start mb-0">
    <li>capacité X</li>
    <li>capacité X</li>
  </ul></td></tr>
  <tr>
    <td rowspan="2">0</td>
    <td rowspan="2" class="d-print-none"><a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a></td>
    <td rowspan="2" class="d-print-none">
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a><br>
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a>
    </td>
    <td class="text-left"><a href="" target="_blank"><i class="fas fa-folder isDirectory"></i> 1. titre séquence</a></td>
  </tr>
  <tr><td><ul class="text-start mb-0">
    <li>capacité X</li>
    <li>capacité X</li>
  </ul></td></tr>
  
  <tr>
    <td rowspan="4">1</td>
    <td rowspan="2">1</td>
    <td rowspan="2" class="d-print-none"><a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a></td>
    <td rowspan="2" class="d-print-none">
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a><br>
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a>
    </td>
    <td class="text-left"><a href="" target="_blank"><i class="fas fa-folder isDirectory"></i> 0. titre séquence</a></td>
  </tr>
    <tr><td><ul class="text-start mb-0">
    <li>capacité X</li>
    <li>capacité X</li>
  </ul></td></tr>
  <tr>
    <td rowspan="2">1</td>
    <td rowspan="2" class="d-print-none"><a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a></td>
    <td rowspan="2" class="d-print-none">
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a><br>
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a>
    </td>
    <td class="text-left"><a href="" target="_blank"><i class="fas fa-folder isDirectory"></i> 1. titre séquence</a></td>
  </tr>
  <tr><td><ul class="text-start mb-0">
    <li>capacité X</li>
    <li>capacité X</li>
  </ul></td></tr>
</tbody>
</table>