# Progression inter capacités

## Liste des thèmes

<style>
  .t1{color:#67b021}
  .t2{color:#069668}
  .t3{color:#b25ea8}
  .t4{color:#081394}
  .t5{color:#932226}
  .t6{color:#444444}
  .t7{color:#e114dc}
  
  .modEven{background-color:#baffab !important;vertical-align: middle}
  .modOdd{background-color:#abfbff !important;vertical-align: middle}
</style>

<ol>
  <li class="t1">A</li>
  <li class="t2">B</li>
  <li class="t3">C</li>
  <li class="t4">D</li>
  <li class="t5">E</li>
  <li class="t6">F</li>
  <li class="t7">G</li>
</ol>

Le thème 'X' étant transverse. Il n'est pas détaillé dans le tableau de la progression.

## Liste des modules

Chaque module contient un ensemble de capacités appartenant à différents thèmes. Voici la liste des modules :

- Module 0 : 'nom du module 0'
- Module 1 : 'nom du module 1'
- Module 2 : 'nom du module 2'
...

## Tableau

<table class="table text-center">
<thead>
<tr>
    <th scope="col" class="text-vertical px-0 text-center">semaine</th>
    <th scope="col" class="text-vertical px-0 text-center">séance(s)</th>
    <th class="d-print-none text-vertical px-0 text-center" scope="col">didactique</th>
    <th class="d-print-none text-vertical px-0 text-center" scope="col">support</th>
    <th class="w-100" scope="col"></th>
    <th scope="col" class="text-vertical px-0 text-center">module</th>
</tr>
</thead>
<tbody>
  <tr><td rowspan="2">0</td><td>0</td>
    <td class="d-print-none"><a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a></td>
    <td class="d-print-none">
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a><br>
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a>
    </td>
    <td><ul class="text-start mb-0">
      <li class="t">capacité X</li>
      <li class="t">capacité X</li>
    </ul></td>
    <td rowspan="2" class="modEven">0</td>
  </tr>
  <tr><td>1</td>
    <td class="d-print-none"><a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a></td>
    <td class="d-print-none">
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a><br>
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a>
    </td>
    <td><ul class="text-start mb-0">
      <li class="t">capacité X</li>
      <li class="t">capacité X</li>
    </ul></td>
  </tr>
  <tr><td rowspan="2">1</td><td>3</td>
    <td class="d-print-none"><a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a></td>
    <td class="d-print-none">
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a><br>
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a>
    </td>
    <td><ul class="text-start mb-0">
      <li class="t">capacité X</li>
      <li class="t">capacité X</li>
    </ul></td>
    <td rowspan="1" class="modOdd">1</td>
  </tr>
  <tr><td>4</td>
    <td class="d-print-none"><a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a></td>
    <td class="d-print-none">
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a><br>
      <a href="" class="isFile" target="_blank"><i class="fas fa-file-alt"></i></a>
    </td>
    <td colspan="2"><b>Évaluation notée</b> : capacités du module 0</td>
  </tr>
</tbody>
</table>