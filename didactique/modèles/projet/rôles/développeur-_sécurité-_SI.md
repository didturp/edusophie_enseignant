# Rôle : développeur(se) sécurité des Systèmes d'Information

+++image "/images/md/F32EB695EB4EF78B3C79FAD9FEHB3D" col-4 mx-auto

+++citation
  Dans l'industrie informatique, la sécurité est un domaine très vaste, au lycée nous nous focalisons sur la sécurité Web et la protection des données. Avec de l'expérience, un(e) spécialiste de la sécurité est un informaticien polyvalent et maître de la protection des systèmes Informatiques stockant des données personnelles et sensibles
+++

<div class="text-center h3 my-4">Spécialités du développeur frontend <b>au lycée</b></div>

<ul>
  <li>Mettre en place des protocoles sécurisés</li>
  <li>Chiffrer ou crypter des données </li>
  <li>Connaître des attaques informatiques courantes et savoir comment les déjouer</li>
  <li>Être garant de la sécurité de données personnelles utilisées et stockées par une application</li>
</ul>

<div class="text-center h3 my-4">Technologies utilisées <b>au lycée</b></div>

- **langages** : bash, python, sql
- **formats de fichiers utilisés** : sh, py, sql
- **applications diverses** : 
    - tshark (bash) : permet d'analyser les échanges entre un client et un serveur
    - les paquets selenium et webdriver_manager (paquets python) : permet de créer un robot virtuel ( +++flag-us bot ) pour naviguer sur un site Web

<div class="text-center h3 my-4">Arbre de progression <b>niveau lycée et au-delà</b></div>

+++bulle matthieu
  étant donné l'émergence quotidienne de nouvelles technologies, il n'est pas évident pour un débutant en informatique de se repérer durant son ascension
+++

Des informaticiens, qui se sont penchés sur cette problématique, élaborent des "roadmap" d'apprentissage sous forme de graphe. Ces "roadmap" permettent de situer visuellement son niveau et de voir quelles sont les technologies qu'il faut aborder par rapport à son niveau afin de progresser efficacement

+++imageFloat /images/md/892A88B4CG9C5839HCFF9E48667HBC droite
  <b>Arbre de progression 'Ethical Web-Hacking'</b> : 
  <ul>
    <li><u>début</u> : niveau bac</li>
    <li><u>fin</u> : niveau bac +5</li>
    <li><u>sens de lecture</u> : du haut vers le bas</li>
    <li><u>commentaire</u> : il s'agit d'un exemple de progression, parmis tant d'autres, spécialisé dans la sécurité Web. Vous remarquerez qu'il est nécessaire d'avoir des connaissances à la fois côté frontend et à la fois côté backend pour progresser dans cette roadmap</li>
    <li><u>lien</u> : +++lien "https://github.com/zourdycodes/vulnsrchattack" "github de zourdycodes"</li>
  </ul>
+++