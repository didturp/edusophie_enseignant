# Grille d'évaluation du projet

+++consignes
  <ul start="1">
    <li><b>Pendant le projet : </b><ul>
      <li>Seul les deux premiers tableaux doivent être rempli</li>
      <li><b>Tous</b> les rôles prévus au projet doivent être pourvus ,un rôle peut être partagé entre plusieurs membres de l'équipe</li>
      <li>En cours de projet, vous pouvez permutter de rôle avec un autre membre de l'équipe</li>
    </ul></li>
    <li><b>Lors de la dernière séance en projet</b>, vous devez répartir entre vous un total de 5 points en complétant la troisième colonne "Répartition des 5 points" du tableau "Membres de l'équipe"</li>
    <li><b>À la fin de la dernière séance en projet</b>, vous devez transmettre ce document à l'évaluateur. Ce document <b>ne doit donc pas être perdu en cours de projet</b></li>
    <li><b>Durant la présentation orale : </b><ul>
      <li>Un support de présentation sous forme de slides est attendu</li>
      <li>Une question orale technique sera posée à chaque membre de l'équipe</li>
    </ul></li>
  </ul>
+++

<table class="table table-sm">
  <tbody>
    <tr>
      <td><b>Nom du projet choisi</b></td>
      <td class="col-8"></td>
    </tr>
    <tr>
      <td><b>Nom de l'équipe</b></td>
      <td></td>
    </tr>
  </tbody>
</table>
  

<table class="table table-sm">
  <thead>
    <tr>
      <th class="text-center" colspan="4">Membres de l'équipe</th>
    </tr>
    <tr class="text-center align-middle">
      <th class="col-1">#</th>
      <th class="col-3">Nom Prenom</th>
      <th class="col-3">Rôle</th>
      <th class="col-3">Répartition des 5 points</th>
  </tr></thead>
  <tbody>
    <tr>
      <td class="p-3 text-center"><b>M1</b></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr>
      <td class="p-3 text-center"><b>M2</b></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr>
      <td class="p-3 text-center"><b>M3</b></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr>
      <td class="p-3 text-center"><b>M4</b></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
  </tbody>
</table>

+++pagebreak

## Évaluation

<div class="col-10 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr>
        <th colspan="4" class="text-center">Coefficients</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <tr>
        <td class="col-3"><b>T</b><sub>rès</sub><b>P</b><sub>assable</sub> | non</td>
        <td class="col-3"><b>P</b><sub>assable</sub></td>
        <td class="col-3"><b>B</b><sub>ien</sub></td>
        <td class="col-3"><b>T</b><sub>rès</sub><b>B</b><sub>ien</sub> | oui</td>
      </tr>
      <tr>
        <td>0</td>
        <td>0.3</td>
        <td>0.7</td>
        <td>1</td>
      </tr>
    </tbody>
  </table>
</div>

### Avant la présentation

<table class="table table-sm">
  <thead>
    <tr>
      <th class="text-center " colspan="6">Contenu du projet (10 points)</th>
    </tr>
  </thead>
  <tbody>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>2 point</b></td>
      <td rowspan="2">Le projet a été rendu <b>48 heures</b> avant la séance de présentation des projets</td>
      <td colspan="2" class="text-center col-1" style="height:23px !important">non</td>
      <td colspan="2" class="text-center col-1">oui</td>
    </tr>
    <tr>
      <td colspan="2" class="p-3"></td>
      <td colspan="2" class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>3 points</b></td>
      <td rowspan="2">Une documentation est fournie avec le projet; elle explique comment démarrer le projet et tester les fonctionnalités principales attendues</td>
      <td class="text-center" style="height:23px !important">TP</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>4 points</b></td>
      <td rowspan="2">Les fonctionnalités principales attendues sont implémentées et fonctionnent sans problème</td>
      <td class="text-center" style="height:23px !important">TP</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>1 point</b></td>
      <td rowspan="2">Le code est propre et facilement lisible par une personne extérieure au projet :<ul class="mb-0">
        <li>le code est commenté, surtout les parties qui ont pu poser des difficultés</li>
        <li>le nom des variables et des fonctions est clair</li>
      </ul></td>
      <td class="text-center" style="height:23px !important">TP</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
  </tbody>
</table>

+++pagebreak

### Pendant la présentation
  
<table class="table table-sm">
  <thead>
    <tr>
      <th colspan="6" class="text-center">Qualité de la présentation (10 points)</th>
    </tr>
  </thead>
  <tbody>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>3 points</b></td>
      <td rowspan="2">Le temps de présentation total (démo comprise) fait entre 9 et 11mn. La démo doit être unique et ne doit pas excéder les 2mn</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;height:23px !important">TP</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">P</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">B</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>4 points</b></td>
      <td rowspan="2">La parole est équitablement répartie entre chaque membre du groupe</td>
      <td class="text-center" style="height:23px !important">TP</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>3 points</b></td>
      <td rowspan="2">Le support de présentation (slides) est de bonne qualité : <ul class="mb-0">
        <li>Il n'y a pas de fautes d'orthographe ni de fautes de grammaire</li>
        <li>Les slides ne sont pas surchargés en texte, l'utilisation de mots-clés est privilégiée</li>
        <li>Du code est présenté dans les slides, le code n'exède pas 50% du contenu total de tous les slides</li>
        <li>Un slide pour la présentation du projet et pour une conclusion sont présents</li>
      </ul></td>
      <td class="text-center" style="height:23px !important">TP</td>
      <td class="text-center">P</td>
      <td class="text-center">B</td>
      <td class="text-center">TB</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm">
  <thead>
    <tr>
      <th class="text-center " colspan="6">Question technique individuelle (5 points)</th>
    </tr>
  </thead>
  <tbody>
    <tr class="align-middle">
      <td rowspan="2" class="text-center col-1"><b>5 points</b></td>
      <td rowspan="2">Chaque membre du groupe a su répondre à une question technique</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;height:23px !important">M1</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">M2</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">M3</td>
      <td class="text-center" style="flex: 0 0 auto;width: 4.166666665%;">M4</td>
    </tr>
    <tr>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
      <td class="p-3"></td>
    </tr>
  </tbody>
</table>

## Bilan

<table class="table table-sm">
  <thead>
    <tr class="text-center">
      <th style="border-left-style:hidden;border-top-style:hidden;"></th>
      <th>M1</th>
      <th>M2</th>
      <th>M3</th>
      <th>M4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="col-8">Contenu du projet (sur 10 points)</td>
      <td colspan="4"></td>
    </tr>
    <tr>
      <td>Qualité de la présentation (sur 10 points)</td>
      <td colspan="4"></td>
    </tr>
    <tr>
      <td>Répartition des points entre les membres de l'équipe (sur 5 points)</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>Question technique individuelle (sur 5 points)</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr class="border-2">
      <td>Note finale</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

