# Projet "nom du projet"

+++sommaire 1

## Sujet du projet

## Spécifications fonctionnelles

+++bulle matthieu
  les spécification fonctionnelles décrivent les fonctionnalités atendues du projet
+++

+++bulle matthieu droite
  un utilisateur non technique ( = non informaticien) doit pouvoir comprendre les spécification fonctionnelles
+++

## Spécifications techniques

+++bulle matthieu
  les spécification techniques décrivent les moyens techniques choisis par les informaticiens pour mettre en œuvre le projet
+++

+++bulle matthieu droite
  cette partie d'un cahier des charges est destinée à l'équipe qui développe le projet
+++

### Rôles 

Les fiches détaillées des rôles se trouvent +++lien "/enseignant/didactique/modeles/projet/roles" "dans ce lien"

### Les choix technologiques

### L’architecture générale

### L’accessibilité

// les utilisateurs utilisent des appareils particuliers ?

### La sécurité

### Le planning prévisionnel

### Le glossaire des termes techniques
