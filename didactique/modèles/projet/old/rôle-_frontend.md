# fiche rôle "développeur frontend"

+++imageFloat /images/md/E57H7EA647DH54G3GH8H5EA93BF84A
Dans l'industrie informatique, le(la) développeur(se) frontend est un(e) informaticien(e) spécialisé(e) dans la création d'interface graphiques soignées et ergonomiques . Avec de l'expérience, il ou elle est capable de concevoir et de développer des interfaces graphiques animées et qui s'adaptent à tous les appareils (ordinateur, tablette, ...). 
+++

<table class="table table-sm">
  <thead>
    <tr class="text-center"></tr>
  </thead>
  <tbody>
    <tr>
      <th class="align-middle text-nowrap">Responsable(s) dans le groupe : +++emoji-writing_hand </th>
      <td class="w-100"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm">
  <thead>
    <tr class="text-center"><th colspan="10">Objectif(s) principal(aux)</th></tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center align-middle">4 point(s)</td>
      <td class="col-10 align-middle">ajouter l'information 'titre' dans une 'card'
      <ul>
        <li>Aide : ligne(s) de code à ajouter entre les lignes 42 et 53 du fichier 'script.js'</li>
      </ul></td>
      <td class="col-1"></td>
    </tr>
    <tr>
      <td class="text-center align-middle">2 point(s)</td>
      <td class="col-10 align-middle">changer le titre de la page "Galerie d'image" par "Mes photos"</td>
      <td class="col-1"></td>
    </tr>
    <tr>
      <td class="text-center align-middle">1 point(s)</td>
      <td class="col-10 align-middle">colorer et centrer le titre principal de la page 
      <ul>
        <li>Aide 1 : rechercher sur Internet 'how color text css'</li>
        <li>Aide 2 : rechercher sur Internet 'how center title css'</li>
        <li>Aide 3 : ligne(s) de code à ajouter au niveau de la ligne 24 du fichier 'style.css'</li>
      </ul></td>
      <td class="col-1"></td>
    </tr>
    <tr>
      <td class="text-center align-middle">1 point(s)</td>
      <td class="col-10 align-middle">ajouter une bordure colorée autour de chaque 'card'<ul>
        <li>Aide : pour cela il faut modifier le fichier 'style.css'</li>
      </ul></td>
      <td class="col-1"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm">
  <thead>
    <tr class="text-center"><th colspan="10">Objectif(s) secondaire(s)</th></tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center align-middle">2 point(s)</td>
      <td class="col-10 align-middle">Utilisez la bibliothèque javascript (<a href="https://tholman.com/intense-images/" target="_blank">https://tholman.com/intense-images/</a>) qui permet au visiteur du site de zoomer sur une image en cliquant dessus</td>
      <td class="col-1"></td>
    </tr>
  </tbody>
</table>

## Comment démarrer ?

### Préparer votre environnement de travail

0. Depuis un terminal, créez vous un dossier du jours : '`mkdir /home/premiere/Eleves/nom_prenom/seances/18_10_21`'
    - **Attention !** pensez à modifier la commande ! (nom_prenom ainsi que la date du jour)
1. Déplacez vous dans le dossier du jours : '`cd /home/premiere/Eleves/nom_prenom/seances/18_10_21`'
2. Téléchargez dans le dossier, dans lequel votre terminal est, l'archive 'frontend' : '`wget https://www.edusophie.com/fichiers/4A774779D5D6ED279837G24B67F372`'
    - **Attention !** vérifiez bien d'abord que votre terminal se trouve dans votre dossier du jour
3. Décompressez l'archive : '`unzip 4A774779D5D6ED279837G24B67F372`'
4. Ouvrez dans gedit les principaux fichiers sur lesquels vous allez travailler : '`gedit frontend/index.html frontend/assets/style.css frontend/assets/script.js`'
    - **Attention !** n'oubliez pas d'utilisez la touche 'tabulation' pour compléter vos chemins pendant que vous tapez la commande 

### Synchronisez vous avec votre développeur(se) backend

5. Aidez votre développeur(se) backend à terminer toutes ses étapes jusqu'à son étape n°8 comprise
6. Dans le fichier 'script.js' ouvert dans gedit, modifiez la ligne :

'`let contenusEnFormatDeDonnees = getContentsFromServer('http://xxx.xxx.xxx.xxx:9000/')`'

=> Remplacez '`xxx.xxx.xxx.xxx `' par l'IP du serveur de données

7. Ouvrez le fichier 'index.html' (pour rappel : situé dans le dossier frontend) sur Firefox. Voilà ce qui doit afficher votre navigateur : 

<img src="/images/md/9245D4E2A9G3EA72CB598A6BA8C9B4">

### Indications pour le travail côté frontend

- Les lignes de code des fichiers 'style.css' et 'script.js' sont très largement commentés
- Gardez toujours ouvert la console de l'inspecteur de votre navigateur et n'hésitez pas à utiliser `console.log(...)` dans le fichier 'script.js' pour debugger votre code
- Explication sur les fichiers 'index.html', 'style.css' et 'script.js'
    - 'index.html' : contient le code HTML principal de la page Internet
    - 'style.css' (situé dans le dossier 'assets'): contient le code CSS qui décore la page Internet, il sera nécessaire d'y ajouter et/ou d'y modifier du code pour remplir vos objectifs 
    - 'script.js' (situé dans le dossier 'assets') : contient le code Javascript qui collecte les données du serveur de données et créé dynamiquement du code HTML en conséquence. Il sera nécessaire d'y ajouter et/ou d'y modifier du code pour remplir vos objectifs 
- Concernant l'objectif secondaire : la bibliothèque 'intense' est déjà installée dans le header de la page 'index.html', il vous faut trouver comment utiliser cette bibliothèque en consultant la documentation (rappel du lien : <a href="https://tholman.com/intense-images/" target="_blank">https://tholman.com/intense-images/</a>)