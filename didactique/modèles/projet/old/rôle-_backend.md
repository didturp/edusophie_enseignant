# fiche rôle "développeur backend"

+++imageFloat /images/md/D5G69B8CBGEH5FFH332C273HEG9BH9
Dans l'industrie informatique, le(la) développeur(se) backend est un(e) informaticien(e) spécialisé(e) dans le traitement et le transfert des données. Avec de l'expérience, cet(te) architecte des données est capable de concevoir et de développer des systèmes d'informations complexes. 
+++

<table class="table table-sm">
  <thead>
    <tr class="text-center"></tr>
  </thead>
  <tbody>
    <tr>
      <th class="align-middle text-nowrap">Responsable(s) dans le groupe : +++emoji-writing_hand </th>
      <td class="w-100"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm">
  <thead>
    <tr class="text-center"><th colspan="10">Objectif(s) principal(aux)</th></tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center align-middle">4 point(s)</td>
      <td class="col-10 align-middle">Le serveur doit transmettre, en plus de chaque photo, une description par photo</td>
      <td class="col-1"></td>
    </tr>
    <tr>
      <td class="text-center align-middle">4 point(s)</td>
      <td class="col-10 align-middle">Le serveur doit transmettre, en plus de chaque photo, un mot-clé par photo</td>
      <td class="col-1"></td>
    </tr>
  </tbody>
</table>

<table class="table table-sm">
  <thead>
    <tr class="text-center"><th colspan="10">Objectif(s) secondaire(s)</th></tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center align-middle">2 point(s)</td>
      <td class="col-10 align-middle">Les données envoyées par le serveur doivent être générées grâce à la librairie python Faker (<a href="https://faker.readthedocs.io/en/master/" target="_blank">https://faker.readthedocs.io/en/master</a>)</td>
      <td class="col-1"></td>
    </tr>
  </tbody>
</table>

## Comment démarrer ?

### Préparer votre environnement de travail

0. Depuis un terminal, créez vous un dossier du jours : '`mkdir /home/premiere/Eleves/nom_prenom/seances/18_10_21`'
    - **Attention !** pensez à modifier la commande ! (nom_prenom ainsi que la date du jour)
1. Déplacez vous dans le dossier du jours : '`cd /home/premiere/Eleves/nom_prenom/seances/18_10_21`'
2. Téléchargez dans le dossier, dans lequel votre terminal est, l'archive 'backend' : '`wget https://www.edusophie.com/fichiers/6HH975788HD9EC9GD4DB29A57HG75B`'
    - **Attention !** vérifiez bien d'abord que votre terminal se trouve dans votre dossier du jour
3. Décompressez l'archive : '`unzip 6HH975788HD9EC9GD4DB29A57HG75B`'
4. Ouvrez le fichier 'server.py' avec thonny : '`thonny backend/server.py`'

### Démarrez le serveur de données

5. Assurez vous qu'un cable éternet soit bien connecté sur votre ordinateur
6. Exécutez le script 'server.py' depuis thonny, voila ce que doit afficher le terminal de thonny : 

<img src="/images/md/4899H9G3955EBF83CC249B4AD72BHH">

7. Affichez votre adresse ip locale (qui est l'adresse utilisée par votre serveur de données) à l'aide de la commande : '`hostname –I`'

<div class="text-center mx-auto">
  <img src="/images/md/CHD55DGBGG7B5B5AACC6DBDCC299A2">
  <div>L'adresse IP peut varier</div>
</div>

8. Ouvrir le navigateur Internet Firefox, saisissez et validez l'url : xxx.xxx.xxx.xxx:9000 (où xxx.xxx.xxx.xxx est l'adresse IP de votre serveur de données trouvée dans l'étape précédente). Voilà ce qui doit afficher votre navigateur : 

<img src="/images/md/874DF2HDCCA3D78H84G469F2D74AA5">

Votre serveur de données fonctionne parfaitement ! 

9. Aidez votre développeur(se) frontend à terminer toutes ses étapes jusqu'à son étape n°7

### Indications pour le travail côté backend

- Lorsque vous souhaitez tester des modifications que vous avez apportées dans le fichier 'server.py', vous devez stopper le serveur (bouton rouge 'stop' tout en haut de thonny) et le redémarrer.
    - En stoppant le serveur, votre développeur frontend ne pourra plus charger correctement la page Internet de son côté, communiquez entre vous pour vous prévenir !
- Pour débugger votre code, gardez un oeil sur la console de thonny et utilisez/rafraichissez dans le navigateur Firefox l'url utilisée dans l'étape n°8
- Pour remplir vos objectifs principaux, vous devez analyser le code 'server.py' et trouver les lignes de codes qui contiennent les données liées aux photos (l'url de la photo et son titre)
    - une fois que vous avez trouvez ces lignes, essayez d'ajouter de nouvelles photos (url + titre)
    - ensuite, et toujours au niveau des lignes de code que vous avez trouvé, modifiez le code pour ajouter de nouveaux types de données tels que la description d'une photo et le mot-clé rattaché à cette photo. N'oubliez pas de rafraîchir la page sur votre navigateur pour voir si les données émises par le serveur de données correspondent à ce que vous souhaitez
