# fiche de Séance nº4

## Séance : 'Débat autour des données personnelles'

<table class="table table-sm text-nowrap mt-5">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Durée de la séance</b></td>
            <td>45 mn</td>
        </tr>
    </tbody>
</table>

<table class="table table-sm mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><ul class="mb-0">
              <li>Définir une donnée personnelle</li>
              <li>Utiliser un support de stockage dans le nuage</li>
              <li>Partager des fichiers</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm mt-4 mb-3">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
              <b>Compétences transversales :</b>
              <ul class="mb-0">
                  <li>partager des ressources</li>
                  <li>Faire un usage responsable et critique des sciences et technologies numériques</li>
              </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Objectif(s) de la séance</b></td></tr>
    </thead>    
    <tbody>
        <tr><td><ul class="mb-0">
            <li>Identifier une donnée personnelle</li>
            <li>Définir ce qu'est le cloud</li>
            <li>Justifier votre point de vue à l’aide d’arguments</li>
            <li>Apprendre à écouter les arguments opposés et à changer de position le cas échéant</li>
        </ul></td></tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>
            <td><ul class="mb-0">
              <li>Prévoir une installation au milieu de la salle pour l'enseignant avec un ordianteur portable</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Matériel</b></td>
            <td><ul class="mb-0">
                <li>Enseignant : ordinateur + video-projecteur</li>
                <li>Élèves : un ordinateur par chaque élève qui n'ont pas pu remplir la fiche de préparation de débat en amont</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Software</b></td>
            <td>RAS</td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Support(s) utilisé(s)</b></td>
            <td><ul class="mb-0">
                <li>Enseignant : <ul>
                  <li>+++lien "/apprenant/00-__-_SNT/01-__-_les-_donnees-_structurees-_et-_leur-_traitement/00-__-_cours/02-__-_les-_donnees-_personnelles-_et-_le-_cloud.md" "<b>[cours & activités]</b> Les données personnelles et le cloud"</li>
                  <li>+++lien "/apprenant/00-__-_SNT/01-__-_les-_donnees-_structurees-_et-_leur-_traitement/02-__-_divers/fiche-_de-_syntese-_de-_debat.md" "fiche de synthèse de débat"</li>
                </ul></li>
                <li>Élèves : <ul>
                  <li>Un exemplaire du cours imprimé</li>
                  <li>La fiche de préparation de débat imprimée (si elle a été remplie par l'élève en amont)</li>
                </ul></li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Préparation de la séance

Les élèves ont du remplir une fiche de préparation de débat en amont chez eux. J'ai imprimé ces fiches. Et avant le cours, je les distribue sur chacun des bureaux de la salle. 

### Déroulement prévu de la séance

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>
            <td colspan="3">Émission culture geek + mini debat</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h00 -> 00h10</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Une chronique vidéo est visionée par les élèves, pendant la vidéo je fais l'appel dans la classe</li>
              <li>À l'issue de la vidéo, les élèves sont sollicités pour donner leurs avis/impressions</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>La dernière chronique geek disponible dans le +++lien "https://www.bfmtv.com/economie/replay-emissions/culture-geek" "lien suivant"</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : vidéo projecteur</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Les élèves restent peu bavards après le visionnage de la vidéo : <ul class="mb-0">
                  <li>je provoque des sondages à main levée sur des questions en rapport avec la vidéo</li>
                  <li>sinon, je termine cette phase un peu plus tôt en enchaîne avec la suivante</li>
                </ul></li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#1</b></td>
            <td colspan="3">Début du cours</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h10 -> 00h15</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Je présente le contenu de la séance dans les grandes lignes</li>
              <li>Je corrige avec les élèves les deux premières questions du support du cours, les elèves ont préparés les réponses à ces questions chez eux. Pour la question sur le cloud, j'en profite pour tisser un parallèle avec EcoleDirecte</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : le cours (projeté)</li>
                <li>Élèves : la fiche de préparation de débat + le cours (imprimé)</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : vidéo projecteur</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Certains élèves n'ont peut être pas pu remplir la fiche de préparation de débat chez eux pour x ou y raisons, je demanderai à ces élèves de venir me voir en fin de cours pour savoir quelles ont été les difficultés</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#2</b></td>
            <td colspan="3">Préparation pour initier un débat mouvant sur les données personnelles</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h15 -> 00h20</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>À l'aide du support du cours, je présente ce qu'est un débat mouvant</li>
              <li>J'affiche sur le tableau le premier sujet et les consignes à suivre pour le débat</li>
              <li>Je m'installe ensuite au centre de la classe sur un pupitre avec un ordinateur portable dans lequel je vais noter certains arguments pendant le débat. </li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : le cours (projeté)</li>
                <li>Élèves : le cours (imprimé)</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : vidéo projecteur + un ordinateur portable</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>RAS</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#3</b></td>
            <td colspan="3">Débat mouvant sur le sujet n°0 puis n°1 puis n°2 </td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h20 -> 00h27 -> 00h34 -> 00h41</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Les élèves qui ont une fiche de préparation de débat sont invites à se lever en prennant leur fiche. Les autres élèves restent assis et vont endosser le rôle de fact checker pendant le débat en s'aidant de recherches sur Internet.</li>
              <li>Pour chaque sujet de débat (il y en a 3), avec les élèves nous suivons les consignes étapes par étapes qui sont affichées sur le vidéo projecteur. Pendant les échanges, je note les arguments qui ont convaincus des élèves d'un groupe à venir dans un autre</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant & Élèves : le sujet du débat + les consignes qui sont projetés au tableau</li>
                <li>Élèves : fiche de préparation de débat</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : vidéo projecteur + un ordinateur portable</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>La majorité des élèves n'ont pas rempli en amont la feuille de préparation de débat : ( il y a plus de fact checker que d'élèves debouts par exemple)<ul>
                  <li>Le débat mouvant est annulé. J'invite les élèves à lire des articles en rapport avec les sujets prévus des débats. Nous échangeons ensuite au fil de la séance sur le contenu des articles</li>
                  <li>Je sollicite les élèves sur les documents qu'ils ont du lire pour préparer le débat</li>
                </ul></li>
                <li>Lors du débat, les élèves participent peu ou les groupes sont trop déséquilibrés : <ul>
                  <li>Je participe ponctuellement au débat pour l'entretenir</li>
                  <li>Je sollicite les élèves sur les documents qu'ils ont du lire pour préparer le débat</li>
                  <li>Je sollicite les élèves fact checker pour aller rechercher une information sur Internet qui peut animer le débat</li>
                  <li>Je peux aussi stopper le débat sur le sujet en cours et passer au sujet suivant</li>
                </ul></li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#4</b></td>
            <td colspan="3">Fin des débats => debrief</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h41 -> fin</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Je demande aux élèves de se rassoirs à leur place et je retourne au niveau de l'ordinateur qui gère le vidéo projecteur</li>
              <li>Nous relevons avec les élèves les arguments qui ont été les plus convaincants</li>
              <li>J'échange avec les élèves pour savoir si ils ont appréciés de faire ce débat mouvant</li>
              <li>J'explique aux élèves qu'ils vont devoir remplir pour la séance suivante une fiche de synthèse de débat (les informations se trouveront dans le cahier de texte numérique)</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : le cours (projeté) + fiche de syntèse de débat</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : vidéo projecteur</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>RAS</li>
            </ul></td>
        </tr>
    </tbody>
</table>


### Analyse réflexive
