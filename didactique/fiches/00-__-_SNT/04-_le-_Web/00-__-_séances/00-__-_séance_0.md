# fiche de Séance nºX

## Séance : 'Titre de la séance'

<table class="table table-sm text-nowrap mt-5">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Durée de la séance</b></td>
            <td>exemple '1h'</td>
        </tr>
    </tbody>
</table>

<table class="table table-sm w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><ul class="mb-0">
              <li>(<b>Pré-req</b> ?) Nom de la capacité (BO)</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
              <b>[NSI] Compétences de discipline :</b>
              <ul class="mb-0">
                  <li>analyser et modéliser un problème en termes de flux et de traitement d’informations</li>
                  <li>décomposer un problème en sous-problèmes, reconnaître des situations déjà analysées et réutiliser des solutions</li>
                  <li>concevoir des solutions algorithmiques</li>
                  <li>traduire un algorithme dans un langage de programmation, en spécifier les interfaces et les interactions, comprendre et réutiliser des codes sources existants, développer des processus de mise au point et de validation de programmes</li>
                  <li>mobiliser les concepts et les technologies utiles pour assurer les fonctions d’acquisition, de mémorisation, de traitement et de diffusion des informations</li>
                  <li>développer des capacités d’abstraction et de généralisation</li>
              </ul>
              <b>Compétences transversales :</b>
              <ul class="mb-0">
                  <li>Faire preuve d’autonomie, d’initiative et de créativité</li>
                  <li>Présenter un problème ou sa solution, développer une argumentation dans le cadre d’un débat</li>
                  <li>Coopérer au sein d’une équipe</li>
                  <li>Rechercher de l’information, apprendre à utiliser des sources de qualité, partager des ressources</li>
                  <li>Faire un usage responsable et critique des sciences et technologies numériques</li>
              </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Objectif(s) de la séance</b></td></tr>
    </thead>    
    <tbody>
        <tr><td><ul class="mb-0">
            <li>Faire découvrir tel logiciel...</li>
        </ul></td></tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>
            <td><ul class="mb-0">
              <li>Prévoir une installation au milieu de la salle pour l'enseignant avec un ordianteur portable</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Matériel</b></td>
            <td><ul class="mb-0">
                <li>Enseignant : ordinateur + video-projecteur</li>
                <li>Élèves : un ordinateur par élève</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Software</b></td>
            <td><ul class="mb-0">
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Support(s) utilisé(s)</b></td>
            <td><ul class="mb-0">
                <li>Enseignant : <ul>
                  <li>+++lien "" "<b>[cours & activité(s)]</b> Nom du cours" (fournis aux élèves en lien raccourci pendant la séance)</li>
                  <li>+++lien "" "<b>[cours]</b> Nom du cours" (fournis aux élèves en lien raccourci pendant la séance)</li>
                  <li>+++lien "" "<b>[activité]</b> Activité #0, nom de l'activité" (fournis aux élèves en lien raccourci pendant la séance)</li>
                </ul></li>
                <li>Élèves : <ul>
                  <li>Supports disponibles en ligne / Polycopié du cours</li>
                </ul></li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement prévu de la séance

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>
            <td colspan="3">titre de la phase : Introduction / évaluation (+ type) / nom d'une étape / ...</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h00 -> 00h10</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>détaillé de la phase</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li></li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#1</b></td>
            <td colspan="3"></td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00hXX -> 00h55</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li></li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li></li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Analyse réflexive

