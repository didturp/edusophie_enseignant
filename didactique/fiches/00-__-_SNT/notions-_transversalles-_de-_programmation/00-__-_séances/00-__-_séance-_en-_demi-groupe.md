# fiche de Séance nº0 en demi-groupe

La séance en demi-groupe a lieu avant la séance en groupe complet. Le but est de faire une présentation rapide de moi-même et de la discipline (qui seront denouveau présentés plus en détail en groupe complet). J'explique ensuite le but des séances en demi-groupe qui, dans un premier temps, seront dédiées à de la programmation python. Une séance en demi-groupe a lieu toute les 4 semaines de cette manière :

0. semaine 0 : demi-groupe 1 de seconde
1. semaine 1 : pas de cours en demi-groupe
2. semaine 2 : demi-groupe 2 de seconde
3. semaine 3 : pas de cours en demi-groupe

...et ainsi de suite. 

## Séance : 'Initiation à la programmation Python'

<table class="table text-nowrap mt-5">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Durée de la séance</b></td>
            <td>1h</td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
            <th class="text-nowrap" style="width:0" colspan="1">Pré-req</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Écrire  et  développer  des  programmes  pour  répondre  à  des 
problèmes et modéliser des phénomènes physiques, 
économiques et sociaux</td>
            <td></td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
                <b>Nom de la compétence transversale (BO)</b><br>
                <u>Savoir(s) associé(s)</u> : 
                <br><u>Savoir(s) faire(s)</u> :
                  <ul>
                    <li>faire preuve ’initiative et de créativité </li>
                  </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Objectif(s) de la séance</b></td></tr>
    </thead>    
    <tbody>
        <tr><td><ul>
            <li>Initiation à la programmation python : manipulation de variable, utilisation d'instruction conditionnelle, utilisation d'instructions itératives</li>
        </ul></td></tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>
            <td>RAS</td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Hardware</b></td>
            <td>Un ordinateur par élève</td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Software</b></td>
            <td>RAS</td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Support(s) utilisé(s)</b></td>
            <td><ul>
                <li>Enseignant : <ul>
                    <li>Projecteur (cours contenant du code exécutable)</li>
                    <li><a href="/apprenant/00-__-_SNT/notions-_transversalles-_de-_programmation/00-__-_cours/00-__-_introduction-_a-_python.md" target="_blank">Lien du cours</a> (fournis aux élèves en lien rapide et sur le cahier de texte numérique)</li>
                    <li><a href="/apprenant/00-__-_SNT/notions-_transversalles-_de-_programmation/00-__-_cours/00-__-_introduction-_a-_python.md" target="_blank">Lien de l'activité #0</a> (fournis aux élèves en lien rapide et sur le cahier de texte numérique)</li>
                </ul></li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement prévu de la séance

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>
            <td colspan="3">Présentation rapide de la discipline et but des séances en demi-groupe</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h00 -> 00h20</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"> Je présente et explique le but, dans un premier temps, des séances en demi groupe. J'incite les élèves à poser des questions et y réponds
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : cours d'initiation à la programmation python en ligne</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : video projecteur</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>savoir(s) associé(s) / compétence(s) travaillée(s)</b></td>
            <td colspan="10"><ul>
                <li>RAS</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li>RAS</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#1</b></td>
            <td colspan="3">Début du cours</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h20 -> 00h45</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">J'entame le cours sur les notions de variable. Des blocs de codes intégrés au cours sont modifiables. Des modification puis explications peuvent aussi être apportées devant toute la classe à l'aide du vidéo projecteur. J'incite les élèves à modifier par eux même les blocs de code et à intéragir entre eux. Pour terminer, les élèves sont invités à écrire un resumé des points importants liés aux variables dans leur classeur</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : cours d'initiation à la programmation python en ligne</li>
                <li>Élèves : cours disponible depuis l'ordinateur</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : video projecteur</li>
                <li>Élèves : ordinateur de la salle</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>savoir(s) associé(s) / compétence(s) travaillée(s)</b></td>
            <td colspan="10"><ul>
                <li>Déclarer et initialiser une variable</li>
                <li>Connaitre les différents types de variable en programmation</li>
                <li>Utiliser la fonction print</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li>Prise en charge individuelle auprès des élèves en difficulté. Si la difficulté semble partagée par un groupe, la difficulté est traité avec l'ensemble de la classe</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#2</b></td>
            <td colspan="3">Activité #0 du cours : 'Manipulation de variables'</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h45 -> 00h55</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">Les élèves sont invités, individuellement, à démarrer l'activité #0 du cours ( <a href="/apprenant/00-__-_SNT/notions-_transversalles-_de-_programmation/01-__-_activites/00-__-_applications-_directes/debranche-_--_-_manipulation-_de-_variables.md" target="_blank">lien</a> ) qui consiste à deviner le résultat qu'affiche un code présenté. Le programme affiche le résultat d'opérations simples entre deux variables et une des denières ligne contient une erreur : cette erreur permet de s'exercer sur la notion de type de variable. Je laisse avancer dans un premier temps les élèves en autonomie et viens ensuite en aide auprès de ceux qui ont du mal à démarrer l'activité</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Élèves : activité disponible depuis l'ordinateur</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Élèves : ordinateur de la salle</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>savoir(s) associé(s) / compétence(s) travaillée(s)</b></td>
            <td colspan="10"><ul>
                <li>Connaitre les différents types de variable en programmation</li>
                <li>Utiliser la fonction print</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li>aider les élèves qui ont du mal à démarrer : reformulation de l'exercice, ré-expliquer les parties du cours partiellement comprises, ...</li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement effectif

#### Groupe 1
Le déroulement #1 a pris plus de temps que prévue. Ainsi, l'activité du déroulement #2 n'a pu être démarrée. Cette activité a donc été donnée à faire à la maison. 

=> le groupe devra faire l'exercice 12 et 13 p182 du livre SNT

#### Groupe 2
Activité #1 terminée

=> le groupe devra faire l'exercice 12 et 13 p182 du livre SNT