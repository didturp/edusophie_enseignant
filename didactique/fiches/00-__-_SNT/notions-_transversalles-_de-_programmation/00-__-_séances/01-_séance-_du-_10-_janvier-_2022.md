# Séance du 10 Janvier 2022 en demi-groupe

## Programmation python : les fonctions (introduction)

<table class="table table-sm text-nowrap mt-5">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Durée de la séance</b></td>
            <td>50mn</td>
        </tr>
    </tbody>
</table>

<table class="table table-sm w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><ul class="mb-0">
              <li>Écrire et développer des programmes pour répondre à des problèmes</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
              <b>Compétences transversales :</b>
              <ul class="mb-0">
                  <li>Faire preuve d’autonomie, d’initiative et de créativité</li>
                  <li>Présenter un problème ou sa solution</li>
              </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Objectif(s) de la séance</b></td></tr>
    </thead>    
    <tbody>
        <tr><td><ul class="mb-0">
            <li>Initier les élèves à la programmation python, et plus précisemment sur l'utilisation des fonctions</li>
            <li>Faire quelques rappels sur le cours dédié aux variables en programmation</li>
        </ul></td></tr>
    </tbody>
</table>

<table class="table table-sm">
    <thead>
        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>
            <td><ul class="mb-0">
              <li>RAS</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Matériel</b></td>
            <td><ul class="mb-0">
                <li>Enseignant : ordinateur connecté sur Internet + video-projecteur</li>
                <li>Élèves : un ordinateur connecté sur Internet pour un ou deux élèves</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Software</b></td>
            <td><ul class="mb-0">
                <li>Enseignant : thonny (IDE python)</li>
                <li>Élèves : thonny (IDE python)</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Support(s) utilisé(s)</b></td>
            <td><ul class="mb-0">
                <li>Enseignant : <ul>
                  <li>+++lien "/apprenant/00-__-_SNT/notions-_transversalles-_de-_programmation/00-__-_cours/01-__-_programmation-_--_-_les-_fonctions.md" "<b>[cours & activité]</b> Programmation : les fonctions" (fournis aux élèves en lien raccourci en début de séance)</li>
                </ul></li>
                <li>Élèves : <ul>
                  <li>Supports disponibles en ligne</li>
                </ul></li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement prévu de la séance

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>
            <td colspan="3">Démarrer la séance</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h00 -> 00h10</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Demander aux élèves d'ouvrir leur session et d'ouvrir leur navigateur sur l'url indiquée sur le vidéo projecteur (en profiter pendant ce temps pour faire l'appel). Les élèves sont invités à noter le lien dans leur classeur</li>
              <li>Présenter rapidement le but de la séance et des séances à venir en s'appuyant sur la partie "Au programme" du cours</li>
              <li>Faire un rappel avec les élèves sur les fonctions déjà vues : print et str</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : le cours</li>
                <li>Élèves : le cours</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : vidéo-projecteur</li>
                <li>Élèves : vidéo-projecteur</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>RAS</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#1</b></td>
            <td colspan="3">Introduire le cours</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h10 -> 00h35</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Suivre avec les élèves le cours jusqu'à la partie "S'outiller pour programmer"</li>
              <li>Inviter les élèves à tester le code intégrer au cours et à échanger autour de questions liées au code</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : le cours</li>
                <li>Élèves : le cours</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : vidéo-projecteur</li>
                <li>Élèves : vidéo-projecteur + ordinateur</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>La classe peu bavarde lorsque je pose une question pendant le déroulement du cours : désigner une personne aléatoirement dans la classe et incitez les autres à l'aider si elle n'as pas la réponse à la question</li>
                <li>Un élève qui rencontre des difficultés : tourner dans la classe pour détecter les élèves qui bloquent et les aider individuellement ou faire appel à la classe si la problématique est pertinente. Faire reformuler les explications par des élèves qui ont compris.</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#2</b></td>
            <td colspan="3">Démarrer la partie du cours "S'outiller pour programmer"</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h35 -> 00h45</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Nous continuons le cours sur une partie qui se concentre sur l'IDE qui est utilisé tout au long de l'année. Le but est que les élèves se familiarisent davantage avec un environnement de développement tout en faisant des rappels sur les variables en programmation vues dans un cours précédent</li>
              <li>Les élèves doivent marquer dans leur classeur les réponses aux questions contenant un +++emoji-write </li>
              <li>Les élèves devront finir l'activité #0 chez eux pour la fois prochaine</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : le cours + activité #0</li>
                <li>Élèves : le cours + activité #0</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : vidéo-projecteur</li>
                <li>Élèves : vidéo-projecteur + ordinateur</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Idem que pour l'étape #1</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table table-sm il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#3</b></td>
            <td colspan="3">Fin de séance</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h45 -> 00h50</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol class="mb-0" start="0">
              <li>Cinq minutes avant la sonnerie, un temps est pris avec les élèves pour faire une synthèse orale de ce qui a été vu durant la séance</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : le cours</li>
                <li>Élèves : le cours</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>Enseignant : vidéo-projecteur</li>
                <li>Élèves : vidéo-projecteur</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul class="mb-0">
                <li>RAS</li>
            </ul></td>
        </tr>
    </tbody>
</table>