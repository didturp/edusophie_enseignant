# Fiche de séquence

<style>
table th{
    text-align:center;
}
</style>

<table class="table w-100 mt-5 mb-3">
    <tbody>
        <tr>
            <td><b>Place dans la progression : </b>exemple 'semaine X à semaine Y'</td>
        </tr>
        <tr>
            <td><b>Nombre de séance(s) : </b>exemple '6 séances : 4 * 2h + 2 * 1h'</td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>
          <ul>
            <li><b>Thème :</b> A<ul>
              <li>Nom de la capacité</li>
              <li>Nom de la capacité</li>
            </ul></li><br>
            <li><b>Thème :</b> B<ul>
              <li>Nom de la capacité</li>
              <li>Nom de la capacité</li>
          <ul> 
        </ul></ul></li></ul></td></tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>
            <b>(NSI) Compétence(e)s de la discipline : </b><br><br>
              +++checkbox "Analyser et modéliser un problème en termes de flux et de traitement d’informations"---
              +++checkbox "Décomposer un problème en sous-problèmes, reconnaître des situations déjà analysées et réutiliser des solutions"---
              +++checkbox "Concevoir des solutions algorithmiques"---
              +++checkbox "Traduire un algorithme dans un langage de programmation, en spécifier les interfaces et les interactions, comprendre et réutiliser des codes sources existants, développer des processus de mise au point et de validation de programmes"---
              +++checkbox "Mobiliser les concepts et les technologies utiles pour assurer les fonctions d’acquisition, de mémorisation, de traitement et de diffusion des informations"---
              +++checkbox "Développer des capacités d’abstraction et de généralisation"---
              
            <br><b>Compétence(e)s transversale(s) : </b><br><br>
              +++checkbox "Faire preuve d’autonomie, d’initiative et de créativité"---
              +++checkbox "Présenter un problème ou sa solution, développer une argumentation dans le cadre d’un débat"---
              +++checkbox "Coopérer au sein d’une équipe dans le cadre d’un projet"---
              +++checkbox "Rechercher de l’information, partager des ressources"---
              +++checkbox "Faire un usage responsable et critique de l’informatique"---
        </td></tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Évaluation(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr><td><ul>
            <li>Diagnostique > ...</li>
            <li>Formative > controlle surprise au début d'une séance sous la forme de QCM</li>
            <li>Sommative > controlle de fin de séquence</li>
        </ul></td></tr>
    </tbody>
</table>