# fiche de renseignement

<table class="table">
    
        <tbody>
        <tr>
            <td class="text-nowrap" style="width:0">Classe</td><td></td>
            <td class="text-nowrap" style="width:0">Date</td><td></td>
        </tr>
        <tr>
            <td class="text-nowrap" style="width:0">Nom</td><td></td>
            <td class="text-nowrap" style="width:0">Prénom</td><td></td>
        </tr>
    
</tbody></table>

<table class="table mt-4">
  <tbody>
      <tr>
          <td class="text-nowrap" style="width:0">Possédez vous un ordinateur personnel ?</td>
          <td>
              <div class="d-flex justify-content-around">
                  <div><input type="radio" name="ordi"> Oui</div>
                  <div><input type="radio" name="ordi"> Non</div>
              </div>
          </td>
      </tr>
      <tr>
          <td class="text-nowrap" style="width:0">Est-ce que cet ordinateur a une connexion internet ?</td>
          <td>
              <div class="d-flex justify-content-around">
                  <div><input type="radio" name="ordico"> Oui</div>
                  <div><input type="radio" name="ordico"> Non</div>
              </div>
          </td>
      </tr>
      <tr>
          <td class="text-nowrap" style="width:0">Est-ce que cet ordinateur est personnel ou familial ?</td>
          <td>
              <div class="d-flex justify-content-around">
                  <div><input type="radio" name="ordiperso"> Oui</div>
                  <div><input type="radio" name="ordiperso"> Non</div>
              </div>
          </td>
      </tr>
      <tr>
          <td class="text-nowrap" style="width:0">Quel est le système d'exploitation de cet l'ordinateur ?</td>
          <td>
              <div class="text-center">
                  <div><input type="checkbox"> windows</div>
                  <div><input type="checkbox"> mac OS</div>
                  <div><input type="checkbox"> une distribution linux</div>
                  <div><input type="checkbox"> je ne sais pas</div>
              </div>
          </td>
      </tr>
      <tr>
          <td class="" style="width:0">Savez vous déjà si en première vous choisirez la spécialité NSI ? (Numérique et Siences Informatiques)</td>
          <td class="text-center">
            <div><input type="checkbox"> Oui !</div>
            <div><input type="checkbox"> Non !</div>
            <div><input type="checkbox"> je ne sais pas pour l'instant</div>
          </td>
      </tr>
</tbody></table>

+++pagebreak

<h3 class="text-center">Questions de programmation</h3>

**1) Qu'est-ce qu'une variable en programmation ?**
<ul type="a">
  <li>Cela permet de modifier les registres de l'ordinateur</li>
  <li>Cela permet de stocker temporairement une donnée pendant l'exécution du programme</li>
  <li>Cela sert à accélérer la variation du temps d'exécution du programme</li>
  <li>Ce concept n'existe pas en programmation</li>
</ul>

**2) Que fait une instruction de type 'boucle' en programmation ?** 
<ul type="a">
  <li>Cela permet de répéter plusieurs fois des instructions</li>
  <li>Cela sert à créer un maître anneau digital, un anneau pour les gouverner tous !</li>
  <li>Cela permet de capturer les erreurs relevées par les instructions try et catch</li>
  <li>Ce concept n'existe pas en programmation</li>
</ul>

**3) Avez vous déjà programmé en Scratch ? Quelle est la valeur de `a` à la fin de ce programme Scratch ?** 

![scratch](https://i.ibb.co/CzKHsmq/Screenshot-from-2021-08-31-11-09-30.png)

**4) Avez vous déjà programmé en Python ? Qu'affiche ce programme Python une fois exécuté ?**

```python
condition = false
calcul = 2 + 4 * 3
if(condition):
    print('Hello !')
print(calcul)
```
