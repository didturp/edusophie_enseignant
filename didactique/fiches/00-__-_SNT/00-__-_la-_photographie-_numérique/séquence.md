# Fiche de séquence

<style>
table th{
    text-align:center;
}
</style>

<table class="table w-100 mt-5 mb-3">
    <tbody>
        <tr>
            <td><b>Place dans la progression :</b> <span class="d-print-none">exemple 'semaine X à semaine Y'</span></td>
        </tr>
        <tr>
            <td><b>Nombre de séance(s) :</b> <span class="d-print-none">exemple '6 séances : 4 * 2h + 2 * 1h'</span></td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr><td><ul>
            <li>(Pré-requis ?) Nom de la capacité (BO)</li>
        </ul></td></tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr><td colspan="2">
            <b>Nom de la compétence transversale (BO)</b><br>
            <u>Savoir(s) associé(s)</u> :
            <p></p>
            <br><u>Savoir(s) faire(s)</u> :
            <p></p>
        </td></tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Évaluation(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr><td><ul>
            <li>Formative > controlle surprise au début d'une séance sous la forme de QCM</li>
            <li>Sommative > controlle de fin de séquence</li>
        </ul></td></tr>
    </tbody>
</table>