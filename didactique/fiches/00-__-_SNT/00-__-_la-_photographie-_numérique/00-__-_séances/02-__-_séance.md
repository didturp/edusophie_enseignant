# fiche de Séance nº2

## Séance : 'Corrections des activités 2 et 3, puis TP python'

<table class="table text-nowrap mt-5">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Durée de la séance</b></td>
            <td>45 mn</td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><ul>
              <li>Traiter par programme une image pour la transformer en agissant sur les trois composantes de ses pixels</li>
              <li>Expliciter des algorithmes associés à la prise de vue</li>
              <li>Identifier les étapes de la construction de l'image finale</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2"><ul>
                <li>Faire preuve d’autonomie</li>
                <li>Rechercher de l’information, apprendre à utiliser des sources de qualité, partager des ressources</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Objectif(s) de la séance</b></td></tr>
    </thead>    
    <tbody>
        <tr><td><ul>
            <li>Consolider les connaissances générales liées à la photographie numérique</li>
            <li>Apprendre à analyser et exécuter un script python fournis</li>
        </ul></td></tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>
            <td>RAS</td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Hardware</b></td>
            <td><ul>
                <li>Enseignant : ordinateur + video projecteur</li>
                <li>Élèves : un ordinateur par élève ou par binome</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Software</b></td>
            <td><ul>
                <li>Enseignant : RAS</li>
                <li>Élèves : IDE python thonny ou spyder</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Support(s) utilisé(s)</b></td>
            <td><ul>
                <li>Enseignant : <ul>
                  <li><a href="https://docs.google.com/document/d/1Y3Of624YHLsEwGbJGlb_xVf3lS-3O1I8cQZ7nZy_ZMs/edit?usp=sharing" target="_blank"><b>[cours / activités et tp]</b> La photographie numérique
</a></li>
                </ul></li>
                <li>Élèves : <ul>
                  <li>Polycopié du cours</li>
                </ul></li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement prévu de la séance

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>
            <td colspan="3">correction des activiées 2 et 3 du cours</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h00 -> 00h15</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">Je corrige les activités avec les reponses projetées au tableau. Les élèves auront accès aux corrections en ligne. Les élèves sont encouragés à partager leurs réponses et à poser des questions si besoin
            <ol start="0">
              <li>correction de l'activite 2</li>
              <li>correction de l'activite 3</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : activités avec correction</li>
                <li>Élèves : polycopié de cours</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : video projecteur</li>
                <li>Élèves : RAS</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li></li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>
            <td colspan="3">début du tp 'Traitement de l’image' qui débordera sans doute sur la séance suivante</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h15 -> 00h45</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">
            Les élèves travaillerons de manière autonomes sur un tp. La disposition de la salle m'oblige à mettre certains élèves en binomes.
            <ol start="0">
              <li>les élèves avancent en autonomie</li>
              <li>je détecte et aide les élèves en difficulté</li>
              <li>une première correction collective sera faite ~10mn avant la fin du cours. Ceci pour synchroniser les avancées des élèves</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li></li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement effectif

