# fiche de Séance nº1

## Séance : 'Titre de la séance'

<table class="table text-nowrap mt-5">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Durée de la séance</b></td>
            <td>45 mn</td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><ul>
              <li>Distinguer les photosites du capteur et les pixels de l’image en comparant les résolutions du capteur et de l’image selon les réglages de l’appareil</li>
              <li>Traiter par programme une image pour la transformer en agissant sur les trois composantes de ses pixels</li>
              <li>Expliciter des algorithmes associés à la prise de vue</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2"><ul>
                <li>Faire preuve d’autonomie</li>
                <li>Rechercher de l’information, apprendre à utiliser des sources de qualité, partager des ressources</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Objectif(s) de la séance</b></td></tr>
    </thead>    
    <tbody>
        <tr><td><ul>
            <li>Apprendre ce qu'est un photosite</li>
            <li>Apprendre la différence entre résolution et définition</li>
            <li>Exécuter un programme python qui manipule une photo</li>
        </ul></td></tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>
            <td><ul>
              <li>S'assurer que tous les élèves auront une chaise en entrant</li>
              <li>Placer les élèves par ordre alphabétique. Le placement devra être tenu par la suite</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Hardware</b></td>
            <td><ul>
                <li>Enseignant : ordinateur + videoprojectreur</li>
                <li>Élèves : un ordinateur par élève</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Software</b></td>
            <td><ul>
                <li>Enseignant : IDE thonny</li>
                <li>Élèves :  IDE thonny</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Support(s) utilisé(s)</b></td>
            <td><ul>
                <li>Enseignant : <ul>
                  <li><a href="https://docs.google.com/document/d/1XvaYgf5otYohqwUtX0lT9QoR_rKF-ScscCuXTdB2N10/edit?usp=sharing" target="_blank">polycopié du cours</a></li>
                  <li><a href="https://docs.google.com/document/d/1SJoD1oT5Ajamzflaw2bLTW-0XGWUJiE9/edit?usp=sharing&ouid=109380883561557730523&rtpof=true&sd=true" target="_blank">correction de l'activité #2</a></li>
                  <li><a href="https://docs.google.com/document/d/1eIlUSjTdMxZiimmlsk8jK9sh6_tZBsox/edit?usp=sharing&ouid=109380883561557730523&rtpof=true&sd=true" target="_blank">correction de l'activité #3</a></li>
                  <li><a href="https://docs.google.com/document/d/1D5EJT2Hf8agn0_1J-TExZffBNQhIA0Xd/edit?usp=sharing&ouid=109380883561557730523&rtpof=true&sd=true" target="_blank">correction de l'activité #5</a></li>
                </ul></li>
                <li>Élèves : <ul>
                  <li><a href="https://docs.google.com/document/d/1XvaYgf5otYohqwUtX0lT9QoR_rKF-ScscCuXTdB2N10/edit?usp=sharing" target="_blank">polycopié du cours</a> (fournis aux élèves en lien raccourci pendant la séance)</li>
                </ul></li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement prévu de la séance

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>
            <td colspan="3">Correction de l'activité 2 du polycopié donnée à faire à la maison</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h00 -> 00h15</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol start="0">
              <li>~à compléter en fonctiond e la disposition de la salle~</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : le polycopié et la correction de l'activité 2</li>
                <li>Élèves : le polycopié et leur travail effectué à la maison</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : correction affichée sur le vidéo projecteur</li>
                <li>Élèves : RAS</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li>Reformuler les définitions des concepts clé (photosites, pixel, ...)</li>
                <li>Faire reformuler par les élèves</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#1</b></td>
            <td colspan="3">Activité 3 du polycopié + correction</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h15 -> 00h35</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol start="0">
              <li>Les élèves démarrent l'activite 3</li>
              <li>J'aide les élèves qui ont des difficultés à démarrer</li>
              <li>Au bout de ~15mn nous entamons la correction</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : le polycopié et la correction de l'activité 2</li>
                <li>Élèves : le polycopié</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : correction affichée sur le vidéo projecteur</li>
                <li>Élèves : RAS</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li>Reformuler les définitions des concepts clé (photosites, pixel, ...)</li>
                <li>Faire reformuler par les élèves</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#1</b></td>
            <td colspan="3">Démarrer l'activité 5 du polycopié</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h35 -> fin du cours</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol start="0">
              <li>Les élèves démarrent l'activite 5. Ils sont invités à installer l'IDE thonny</li>
              <li>J'aide les élèves qui ont des difficultés à démarrer. Cette activité sera terminée la séance suivante</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : le polycopié et la correction de l'activité 2</li>
                <li>Élèves : le polycopié</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : correction affichée sur le vidéo projecteur</li>
                <li>Élèves : RAS</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li>Difficulté liée au matériel : ordinateurs, réseaux, session de connection... => faire un tour dans la salle avant la séance pour s'assurer que tout fonctionne</li>
                <li>Faire reformuler par les élèves</li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement effectif

