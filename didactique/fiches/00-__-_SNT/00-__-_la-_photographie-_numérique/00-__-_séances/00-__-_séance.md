# fiche de Séance nº0

## Séance : 'La photographie numérique (1/4)'

<table class="table text-nowrap mt-5">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Durée de la séance</b></td>
            <td>1h</td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
            <th class="text-nowrap" style="width:0" colspan="1">Pré-req</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Distinguer les photosites du capteurs et les pixels de l'image en comparant les résolutions du capteur et de l'image selon les réglages de l'appareil</td>
            <td></td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
                <b>Nom de la compétence transversale (BO)</b><br>
                <u>Savoir(s) associé(s)</u> :
                <ul>
                  <li>Définir ce qu'est un photosite</li>
                  <li>Définir ce qu'est un pixel</li>
                  <li>Établir le lien entre pixel et qualité de l'image</li>
                </ul>
                <u>Savoir(s) faire(s)</u> : 
                <ul>
                  <li>Faire preuve d’autonomie</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Objectif(s) de la séance</b></td></tr>
    </thead>    
    <tbody>
        <tr><td><ul>
            <li>Présentation générale : enseignant, discipline, organisation, ..</li>
            <li>Introduire les premiers élément liés à la photographie numérique : historique, photosite, ce qu'est la définition, ...</li>
        </ul></td></tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>
            <td>RAS</td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Hardware</b></td>
            <td><ul>
                <li>Élèves : un ordinateur par élève</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Software</b></td>
            <td>RAS</td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Support(s) utilisé(s)</b></td>
            <td><ul>
                <li>Enseignant : <ul>
                  <li>Projecteur (<a href="/apprenant/00-__-_SNT/README.md" target="_blank">fichier README</a> décrivant l'organisation générale de l'année et partagé par mail, le cours)</li>
                </ul></li>
                <li>Élèves : <ul>
                  <li>Polycopié du cours ( <a href="https://docs.google.com/document/d/1XvaYgf5otYohqwUtX0lT9QoR_rKF-ScscCuXTdB2N10/edit?usp=sharing" target="_blank">version numérique</a> )</li>
                </ul></li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement de la séance

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>
            <td colspan="3">Présentation de l'enseignant, de la discipline et de l'organisation générale de l'année</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h00 -> 00h20</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">
              Je me présente et présente la discipline. Je demande aux élèves quelle est la différence entre numérique et technologie (ref à SNT). Ensuite, j'expose l'organisation générale de l'année en parcourant avec les élèves le fichier README : matériel nécessaire, planning, évaluations, sanctions, ...
              <br>
              J'incite les élèves à poser des questions si ils en ont.
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10">RAS</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10">RAS</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>savoir(s) associé(s) / compétence(s) travaillée(s)</b></td>
            <td colspan="10">RAS</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10">RAS</td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#1</b></td>
            <td colspan="3">Démarrer le cours</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h20 -> 00h30</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">
              J'anime une discussion avec la classe sur l'utilisation d'appareils permettant de prendre des photos (les smartphone notament) : la fréquence d'utilisation, savez vous quelle taille en moyenne fait une photo ? , ... 
              <br>
              Ensuite, je démarre une très courte vidéo qui trace un historique de la photographie. Pour finir, nous discutons avec les élèves du contenu de la vidéo
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : la vidéo dont le lien est présent dans le cours</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : le vidéo projecteur</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>savoir(s) associé(s) / compétence(s) travaillée(s)</b></td>
            <td colspan="10">RAS</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10">RAS</td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#2</b></td>
            <td colspan="3">Première activité</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h30 -> 00h45</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">
              À travers cette premere activité, les elèves vont se familiariser avec les premiers concepts important de la photographie numérique : les principaux composants d'un appareil, les photosites, la définition d'un capteur photo.
              <br>
              Je laisse avancer les élèves en autonomie, puis nous corrigeons tous ensemble l'activité après 10 minutes
            </td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Élèves : polycopié de cours</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10">RAS</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>savoir(s) associé(s) / compétence(s) travaillée(s)</b></td>
            <td colspan="10"><ul>
                <li>Savoir définir ce qu'est un photosite</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li>je tourne pour aider les élèves en difficulté sur des questions précises et/ou ceux qui ont du mal à démarrer l'activité (problème de compréhension de l'énoncer, difficulté à comprendre le but de l'activité, ...)</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#3</b></td>
            <td colspan="3">Deuxième activité</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h45 -> 00h55</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">La deuxième activité est un peu plus courte que la première et met en lumière la notion de pixel et le lien avec la qualité de l'image. Les élèves sont invités à faire l'activité et je les laisse avancer denouveau en autonomie</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Élèves : polycopié de cours</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10">RAS</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>savoir(s) associé(s) / compétence(s) travaillée(s)</b></td>
            <td colspan="10"><ul>
                <li>Distinguer les photosites du capteurs et les pixels de l'image en comparant les résolutions du capteur et de l'image selon les réglages de l'appareil</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li>je tourne pour aider les élèves en difficulté sur des questions précises et/ou ceux qui ont du mal à démarrer l'activité (problème de compréhension de l'énoncer, difficulté à comprendre le but de l'activité, ...)</li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement effectif

Beaucoup de temps perdu suite à plusieurs évennements mal anticipés :
- le nombre important d'élèves (38) vis à vis de la configuration de la salle 
- le nombre de chaises manquantes en début de cours
    - la prochaine fois : arriver plus tôt pour récupérer des chaises dans les salles annexes
    - solution : demander à l'établissement de fournir plus de chaises pour cette salle
- la distribution laborieuse des polycopiés de cours
    - créer à l'avance des paquet distincts et facilement distribuables
- les divers problèmes de connexion sur les postes
    - solution appliquée sur le coup : créer des binômes entre élèves avec au moins un des deux possédant un compte de session fonctionnel. Demander aux élèves qui n'ont pas de compte de se rapprocher du service informatique du lycée

Avec ces difficulté, je n'ai seulement pu avancer qu'à la fin de la première activité. La deuxième activité a donc été donnée à faire à la maison.