# fiche de Séance nºX

## Séance : 'Titre de la séance'

<table class="table text-nowrap mt-5">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Durée de la séance</b></td>
            <td>exemple '2h'</td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
            <th class="text-nowrap" style="width:0" colspan="1">Pré-req</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Nom de la capacité (BO)</td>
            <td></td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
                <b>Nom de la compétence transversale (BO)</b><br>
                <u>Savoir(s) associé(s)</u> : 
                <br><u>Savoir(s) faire(s)</u> : 
            </td>
        </tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Objectif(s) de la séance</b></td></tr>
    </thead>    
    <tbody>
        <tr><td><ul>
            <li>Faire découvrir tel logiciel...</li>
        </ul></td></tr>
    </tbody>
</table>

<table class="table text-nowrap">
    <thead>
        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>
            <td><ul>
                <li></li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Hardware</b></td>
            <td><ul>
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Software</b></td>
            <td><ul>
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Support(s) utilisé(s)</b></td>
            <td><ul>
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement de la séance

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>
            <td colspan="3">titre de la phase : Introduction / évaluation (+ type) / nom d'une étape / ...</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h00 -> 00h10</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10">Détaillé de la phase</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0" rowspan="3"><b>activité</b></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>enseignant</b></td>
            <td colspan="10"><ul>
                <li>donner la consigne X / ... </li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>élève</b></td>
            <td colspan="10"><ul>
                <li>travail individuel / en groupe / ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>savoir(s) associé(s) / compétence(s) travaillée(s)</b></td>
            <td colspan="10"><ul>
                <li></li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li></li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#1</b></td>
            <td colspan="3"></td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0"></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0" rowspan="3"><b>activité</b></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>enseignant</b></td>
            <td colspan="10"><ul>
                <li></li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>élève</b></td>
            <td colspan="10"><ul>
                <li></li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : ...</li>
                <li>Élèves : ...</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>savoir(s) associé(s) / compétence(s) travaillée(s)</b></td>
            <td colspan="10"><ul>
                <li></li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li></li>
            </ul></td>
        </tr>
    </tbody>
</table>