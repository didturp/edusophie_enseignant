# fiche de Séance nº0

## Séance : 'Introduction en informatique fondamentale'

<table class="table text-nowrap mt-5">
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Durée de la séance</b></td>
            <td>1h40</td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Capacité(s) visée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><ul>
              <li>Distinguer les rôles et les caractéristiques des différents constituants d’une machine</li>
              <li>Dresser la table d’une expression booléenne</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table w-100 mt-4 mb-3">
    <thead>
        <tr>
            <th>Compétence(s) travaillée(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
              <b>[NSI] Compétences de discipline :</b>
              <ul>
                  <li>analyser et modéliser un problème en termes de flux et de traitement d’informations</li>
                  <li>reconnaître des situations déjà analysées et réutiliser des solutions</li>
                  <li>traduire un algorithme dans un langage de programmation</li>
                  <li>développer des capacités d’abstraction et de généralisation</li>
              </ul>
              <b>Compétences transversales :</b>
              <ul>
                  <li>Faire preuve d’autonomie</li>
                  <li>Coopérer au sein d’une équipe</li>
              </ul>
            </td>
        </tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" colspan="100"><b>Objectif(s) de la séance</b></td></tr>
    </thead>    
    <tbody>
        <tr><td><ul>
            <li>Découvrir l'architecture Von Neumann et comprendre son impact sur l'informatique d'aujourd'hui</li>
            <li>Introduire ce qu'est un processeur et faire le lien avec l'algèbre de Boole</li>
            <li>S'initier à l'algèbre de Boole</li>
        </ul></td></tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr><td class="align-middle text-center" style="width:0" colspan="100"><b>Environnement à préparer</b></td></tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" style="width:0"><b>Aménagement de l'espace</b></td>
            <td>RAS</td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Matériel</b></td>
            <td><ul>
                <li>Enseignant : ordinateur + video-projecteur</li>
                <li>Élèves : un ordinateur par élève</li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Software</b></td>
            <td><ul>
                <li>Enseignant : RAS</li>
                <li>Élèves : un outil en ligne permettant de simuler le comportement de portes logiques (<a href="https://logic.ly/demo/" blank="_blank">https://logic.ly/demo/</a>) </li>
            </ul></td>
        </tr>
        <tr>
            <td class="text-center" style="width:0"><b>Support(s) utilisé(s)</b></td>
            <td><ul>
                <li>Enseignant : 
                  <ul>
                    <li><a href="/apprenant/01-__-_NSI-_premiere/01-__-_Introduction-_en-_informatique-_fondamentale-_et-_introduction-_en-_programmation-_Web-_cote-_client/00-__-_cours/00-__-_Introduction-_en-_informatique-_fondamentale.md" target="_blank"><b>[cours & activité(s)]</b> Introduction en informatique fondamentale</a> (fournis aux élèves en lien raccourci pendant la séance)</li>
                    <li><a href="/apprenant/01-__-_NSI-_premiere/01-__-_Introduction-_en-_informatique-_fondamentale-_et-_introduction-_en-_programmation-_Web-_cote-_client/introduction.md" target="_blank"><b>[vidéo introductive]</b> Architectures matérielles et systèmes d’exploitation</a> (les élèves doivent la visionner avant la séance)</li>
                  </ul>
                </li>
                <li>Élèves : <ul>
                  <li>Supports disponibles en ligne</li>
                </ul></li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Déroulement prévu de la séance

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#0</b></td>
            <td colspan="3">Échanges avec les élèves sur la vidéo introductive</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h00 -> 00h10</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol start="0">
              <li>Nous échangeons avec les élèves sur la vidéo introductive à voir avant la séance</li>
              <li>Nous répondons ensemble aux questions reliées à la vidéo</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : le document introduisant le thème "Architectures matérielles et systèmes d’exploitation"</li>
                <li>Élèves : RAS</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li>Je prends le temps pendant ~10mn pour répondre aux diverses questions. l'idée est de rester général sur le thème avant de commencer le cours</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#1</b></td>
            <td colspan="3">Début du cours "Architectures matérielles et systèmes d’exploitation"</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h10 -> 00h35</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol start="0">
              <li>Je demande aux élèves de sortie une feuille pour démarrer le nouveau module</li>
              <li>Je lis la première partie (introduction et architecture Von Neumann) du support de cours</li>
              <li>Des questions prévues sont posées par moment aux élèves, ils doivent noter les réponses sur leur feuille</li>
              <li>Un ordinateur sera ouvert et les différents composants seront montrés aux élèves</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : le cours en ligne</li>
                <li>Élèves : RAS</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : video-projo + ordinateur</li>
                <li>Élèves : feuille de cours</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li>Partie du cours très théorique, je m'assure de ne pas perdre d'élève en chemin</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#2</b></td>
            <td colspan="3">Suite du cours "Architectures matérielles et systèmes d’exploitation"</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h35 -> 00h50</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol start="0">
              <li>Je lis la deuxième partie (Au cœur des processeurs) du support de cours consacrée à de l'histoire informatique</li>
              <li>Une courte vidéo est visualisée : zoom à l'intérieur d'un processeur</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : le cours en ligne</li>
                <li>Élèves : RAS</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : video-projo + ordinateur</li>
                <li>Élèves : feuille de cours</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li>Partie du cours très théorique, je m'assure de ne pas perdre d'élève en chemin</li>
            </ul></td>
        </tr>
    </tbody>
</table>

<table class="table il-table-collapse-2">
    <tbody>
        <tr>
            <td class="align-middle text-center text-nowrap w"><b>#3</b></td>
            <td colspan="3">Activité "Algrèbre de Boole et tables de vérité"</td>
            <td style="width:0" class="text-nowrap align-middle text-center"><b>temps ~~</b></td>
            <td class="align-middle text-center text-nowrap" style="width:0">00h50 -> 01h35</td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>contenu</b></td>
            <td colspan="10"><ol start="0">
              <li>Les élèves, mis en binômes, sont invités à démarrer l'activité #0 du cours</li>
              <li>Je les laisse avancer en autonomie et aide les groupes qui ont des difficultés à démarrer</li>
              <li>Une correction collective intermédiaire est faîte lorsque la plupart des groupes ont réussis la question 0</li>
              <li>Une deuxième correction, cette fois ci complète, est faîte après ~30mn d'activité ou plus tôt si tous les groupes ont répondus avant aux questions. Auquel cas, nous approfondirons les fonctionnalités du simulateur de portes logiques</li>
            </ol></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>support(s) utilisé(s)</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : l'activité, et sa correction, en ligne + le simulateur en ligne</li>
                <li>Élèves : le simulateur en ligne</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>matériel utilisé</b></td>
            <td colspan="10"><ul>
                <li>Enseignant : video-projo + ordinateur</li>
                <li>Élèves : feuille de cours</li>
            </ul></td>
        </tr>
        <tr>
            <td class="align-middle text-center" style="width:0"><b>difficultés<br>/<br>remédiations</b></td>
            <td colspan="10"><ul>
                <li>La création de binôme me libère plus de temps pour venir auprès des élèves en difficulté. La correction intermédiaire aidera aussi les élèves en difficulté à répondre plus facilement aux autres questions (qui reviennent au même, nous changeons juste de porte logique)</li>
                <li>La partie python de l'activité est très simple et est un ré-investissement de ce qui a été vu dans les séances précédantes => pour les élèves en difficulté sur ce point, je prendrai le temps aurpès d'eux pour ré-expliquer les concepts python nécessaires pour faire l'activité</li>
            </ul></td>
        </tr>
    </tbody>
</table>

### Analyse réflexive

