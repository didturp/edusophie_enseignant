# fiche de renseignement

<table class="table">
    
        <tbody>
        <tr>
            <td class="text-nowrap" style="width:0">Classe</td><td><input class="w-100"></td>
            <td class="text-nowrap" style="width:0">Date</td><td><input class="w-100"></td>
        </tr>
        <tr>
            <td class="text-nowrap" style="width:0">Nom</td><td><input class="w-100"></td>
            <td class="text-nowrap" style="width:0">Prénom</td><td><input class="w-100"></td>
        </tr>
    
</tbody></table>

<table class="table">
    
        <tbody>
        <tr>
            <td colspan="4" class="text-center" style="width:0"><b>Spécialités choisies</b></td>
        </tr>
        <tr>
            <td colspan="4">
                <div class="offset-1 col-11"><input type="checkbox" checked=""> Numérique et Sciences Informatiques (NSI)</div>
                <hr style="border: 3px solid #000000 !important;">
                <div class="offset-1 col-11"><input type="checkbox"> Histoire géographie, géopolitique et sciences politiques</div>
                <div class="offset-1 col-11"><input type="checkbox"> Humanités, littérature et philosophie</div>
                <div class="offset-1 col-11"><input type="checkbox"> Langues, littératures et cultures étrangères – Anglais</div>
                <div class="offset-1 col-11"><input type="checkbox"> Langues, littératures et cultures étrangères – Espagnol</div>
                <div class="offset-1 col-11"><input type="checkbox"> Mathématiques</div>
                <div class="offset-1 col-11"><input type="checkbox"> Physique Chimie</div>
                <div class="offset-1 col-11"><input type="checkbox"> Sciences de la vie et de la terre</div>
                <div class="offset-1 col-11"><input type="checkbox"> Sciences économiques et sociales</div>
            </td>
        </tr>
    
</tbody></table>

<table class="table mt-4">
  <tbody>
  
      <tr>
          <td class="text-nowrap" style="width:0">Possédez vous un ordinateur personnel ?</td>
          <td>
              <div class="d-flex justify-content-around">
                  <div><input type="radio" name="ordi"> Oui</div>
                  <div><input type="radio" name="ordi"> Non</div>
              </div>
          </td>
      </tr>
      <tr>
          <td class="text-nowrap" style="width:0">Est-ce que cet ordinateur a une connexion internet ?</td>
          <td>
              <div class="d-flex justify-content-around">
                  <div><input type="radio" name="ordico"> Oui</div>
                  <div><input type="radio" name="ordico"> Non</div>
              </div>
          </td>
      </tr>
      <tr>
          <td class="text-nowrap" style="width:0">Cet ordinateur est-il personnel ? ( = non familial)</td>
          <td>
              <div class="d-flex justify-content-around">
                  <div><input type="radio" name="ordiperso"> Oui</div>
                  <div><input type="radio" name="ordiperso"> Non</div>
              </div>
          </td>
      </tr>
      <tr>
          <td class="text-nowrap" style="width:0">Quel est le système d'exploitation de cet l'ordinateur ?</td>
          <td>
              <div class="text-center">
                  <div><input type="checkbox"> windows</div>
                  <div><input type="checkbox"> mac OS</div>
                  <div><input type="checkbox"> une distribution linux</div>
                  <div><input type="checkbox"> je ne sais pas</div>
              </div>
          </td>
      </tr>
      <tr>
        <td class="" style="width:0">Savez vous déjà si vous garderez la spécialité NSI l'an prochain ?</td>
          <td>
            <div class="text-center">
                <div><input type="radio" name="specialite"> Je compte la garder</div>
                <div><input type="radio" name="specialite"> Je ne compte pas la garder</div>
                <div><input type="radio" name="specialite"> Je ne sais pas encore</div>
            </div>
        </td>
      </tr>
</tbody></table>

<h3 class="text-center">Questions de programmation</h3>

<table class="table mt-4">
  <thead><tr><th class="text-center">Qu'est-ce qu'une variable en programmation ?</th></tr></thead>
  <tbody>
    <tr><td>
      <div><input type="checkbox"> Cela permet de modifier les registres de l'ordinateur</div>
      <div><input type="checkbox"> Cela permet de stocker temporairement une donnée pendant l'exécution du programme</div>
      <div><input type="checkbox"> Cela sert à accélérer la variation du temps d'exécution du programme</div>
      <div><input type="checkbox"> Ce concept n'existe pas en programmation</div>
    </td></tr>
  </tbody>
</table>


<table class="table mt-4">
  <thead><tr><th class="text-center">Que fait une instruction de type 'boucle' en programmation ?</th></tr></thead>
  <tbody>
    
        <tr><td>
            <div><input type="checkbox"> Cela permet de répéter plusieurs fois des instructions</div>
            <div><input type="checkbox"> Cela sert à créer un maître anneau digital, un anneau pour les gouverner tous !</div>
            <div><input type="checkbox"> Cela permet de capturer les erreurs relevées par les instructions <code>try</code> et <code>catch</code></div>
            <div><input type="checkbox"> Ce concept n'existe pas en programmation</div>
        </td>
    </tr>
  </tbody>
</table>

<table class="table mt-4">
  <thead><tr><th class="text-center">Qu'affiche le programme suivant après son exécution ?</th></tr></thead>
  <tbody>
    <tr><td>
    
```python
condition = false
calcul = 2 + 4 * 3
if(condition):
    print('Hello !', end = ' ')
print('voici le résultat du calcul : ', calcul)
```
          <div class="col-8 mx-auto">
            <textarea class="w-100" placeholder="Votre réponse"></textarea>
          </div>
        </td>
    </tr>
  </tbody>
</table>

