# Progression et programmation

## Liste des thèmes

<style>
  .t1{color:#67b021}
  .t2{color:#069668}
  .t3{color:#b25ea8}
  .t4{color:#081394}
  .t5{color:#932226}
  .t6{color:#444444}
  .t7{color:#e114dc}
  
  .modEven{background-color:#baffab !important;vertical-align: middle}
  .modOdd{background-color:#abfbff !important;vertical-align: middle}
  .vac{background-color:#ffabfb !important;vertical-align: middle}
  
  table td ul{
    margin-bottom:0px;
  }
  
  thead > tr {
    position: sticky;
    top: 0;
    background-color: #edecec;
    box-shadow: 0 4px 2px -1px rgba(0, 0, 0, 0.4);
  }
  table {
    border-collapse: separate; /* Don't collapse */
    border-spacing: 0;
  }
  
</style>

<ol>
  <li class="t1">Représentation des données: types et valeurs de base</li>
  <li class="t2">Représentation des données: types construits</li>
  <li class="t3">Traitement de données en tables</li>
  <li class="t4">Interactions entre l’homme et la machine sur le Web</li>
  <li class="t5">Architectures matérielles et systèmes d’exploitation</li>
  <li class="t6">Langages et programmation</li>
  <li class="t7">Algorithmique</li>
</ol>

Le thème 'Histoire de l’informatique' étant transverse. Il n'est pas détaillé dans le tableau de la progression. 

## Progression

Chaque module contient un ensemble de capacités appartenant à différents thèmes. Voici la liste des modules :

- Module 00 : **Rappels en programmation et système d'exploitation Linux**
- Module 01 : **Introduction en informatique fondamentale et introduction en programmation Web côté client**
- Module 02 : **Introduction aux tableaux et programmation Web côté client** (projet)
- Module 03 : **Parcours de tableaux, introduction en algorithmie et tests**
- Module 04 : **Intoduction au réseau**
- Module 05 : **Programmation Web et architecture client-serveur** (projet)
- Module 06 : **Recherche de données dans une table, systèmes binaire et hexadécimal**
- Module 07 : **Algorithmes de recherche, de tri et invariant de boucle**
- Module 08 : **Tri et fusion de données dans une table, représentation des entiers relatifs et décimaux en binaire**
- Module 09 : **Tableaux associatifs et systèmes d'encodage**
- Module 10 : **Tableaux à 2 dimensions, algorithmes d'apprentissage et algorithmes gloutons**
- Module 11 : **Module de fin d'année** (projetS)

+++todo réfléchir à la conception d'un/de projet(s) entre les modules ~6 et ~10

## Programmation

<table class="table">
<thead>
<tr>
    <th scope="col" class="text-vertical px-0 text-center">semaine</th>
    <th scope="col" class="text-vertical px-0 text-center">séance</th>
    <th class="w-100 align-middle text-center" scope="col">contenu</th>
</tr>
</thead>
<tbody>
  <tr><td class="modEven" colspan="3"><div class="float-start"><b>[ Module 00 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="/apprenant/01-__-_NSI-_premiere/00-__-_rappels-_en-_programmation-_et-_systeme-_d--__exploitation-_Linux">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="/enseignant/didactique/fiches/01-__-_NSI-_premiere/00-__-_rappels-_en-_programmation-_et-_systeme-_d--__exploitation-_Linux">enseignant</a></div>
  </div></td></tr>
  <tr><td>36</td><td>0</td>
    <td>
    <ul>
      <li class="t6">Mettre en évidence un corpus de constructions élémentaires (les variables)</li>
    </ul></td>
  </tr>
  <tr><td>36</td><td>1</td>
    <td><ul>
      <li class="t6">Mettre en évidence un corpus de constructions élémentaires (les instructions conditionnelles)</li>
    </ul></td>
  </tr>
    <tr><td>37</td><td>2</td>
    <td><ul>
      <li class="t5">Identifier les fonctions d’un système d’exploitation</li>
      <li class="t5">Utiliser les commandes de base en ligne de commande</li>
    </ul></td>
  </tr><tr><td>37</td><td>3</td>
    <td><ul>
      <li class="t6">Mettre en évidence un corpus de constructions élémentaires (les instructions itératives)</li>
    </ul></td>
  </tr>
  <tr><td>38</td><td>4</td>
    <td><ul>
      <li class="t5">Gérer les droits et permissions d’accès aux fichiers</li>
      <li class="t6">Prototyper une fonction</li>
      <li class="t6">Décrire les préconditions sur les arguments</li>
    </ul></td>
  </tr><tr><td>38</td><td>5</td>
    <td><ul>
      <li class="t6">Prototyper une fonction</li>
      <li class="t6">Décrire les préconditions sur les arguments</li>
    </ul><div><i>+ exercices de programmation pendant une heure pour préparer le contrôle de la séance suivante</i></div>
    </td>
  </tr>
  <tr><td>39</td><td>6</td>
    <td>évaluation sommative : <b>[ Module 00 ]</b></td>
  </tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Module 01 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="/apprenant/01-__-_NSI-_premiere/01-__-_Introduction-_en-_informatique-_fondamentale-_et-_introduction-_en-_programmation-_Web-_cote-_client">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="/enseignant/didactique/fiches/01-__-_NSI-_premiere/01-__-_Introduction-_en-_informatique-_fondamentale-_et-_introduction-_en-_programmation-_Web-_cote-_client">enseignant</a></div>
  </div></td></tr>
  <tr><td>39</td><td>0</td>
    <td><ul>
      <li class="t5">Distinguer les rôles et les caractéristiques des différents constituants d’une machine</li>
      <li class="t1">Dresser la table d’une expression booléenne</li>
    </ul></td>
  </tr>
  <tr><td>40</td><td>1</td>
    <td><ul>
      <li class="t4">Identifier les différents composants graphiques permettant d’interagir avec une application Web</li>
      <li class="t4">Identifier les événements que les fonctions associées aux différents composants graphiques sont capables de traiter</li>
    </ul></td>
  </tr>
  <tr><td class="modEven" colspan="3"><div class="float-start"><b>[ Module 02 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>40</td><td>0</td>
    <td><ul>
      <li class="t2">Lire et modifier les éléments d’un tableau grâce à leurs index</li>
    </ul></td>
  </tr>
  <tr><td>41</td><td>1</td>
    <td>évaluation sommative : <b>[ Modules ≤ 01 ]</b></td>
  </tr>
  <tr><td>41</td><td>2</td>
    <td><ul>
      <li class="t4">Analyser et modifier les méthodes exécutées lors d’un clic sur un bouton d’une page Web</li>
      <li class="t6">Repérer, dans un nouveau langage de programmation,les traits communs et les traits particuliers à ce langage</li>
    </ul></td>
  </tr>
  <tr><td>42</td><td>3</td>
    <td class="align-middle" rowspan="2">Projet évalué : <b>[ Modules ≤ 02 ]</b>
      <ul class="text-start mb-0"><li class="t6">Utiliser la documentation d’une bibliothèque</li></ul></td>
  </tr>
  <tr><td>42</td><td>4</td></tr>
  <tr><td colspan="3" class="vac"><b>Vacances de Toussaint</b></td></tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Module 03 ] Parcours de tableaux, introduction en algorithmie et tests</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr>
    <td colspan="3"><b>Contexte module</b> : en début de module, nous faisons un rappel sur les tableau en python et poussons plus loin pour apprendre à boucler dessus. Cela permet d'attaquer l'algorithmie avec déjà des bonnes billes en main</td>
  </tr>
  <tr><td>45</td><td>0</td>
    <td>
    <b>Contenu séance</b> : comment amener les eleves durant la séance / scénarisation
    <ul>
      <li class="t2">Itérer sur les éléments d’un tableau</li>
      <li class="t7">Écrire un algorithme de recherche d’une occurrence sur des valeurs de type quelconque</li>
    </ul></td>
  </tr>
  <tr><td>45</td><td>1</td>
    <td><ul>
      <li class="t6">Décrire des postconditions sur les résultats</li>
      <li class="t6">Utiliser des jeux de tests</li>
    </ul></td>
  </tr>
  <tr><td>46</td><td>2</td>
    <td>évaluation sommative : <b>[ Modules ≤ 03 ]</b></td>
  </tr>
  <tr><td class="modEven" colspan="3"><div class="float-start"><b>[ Module 04 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>46</td><td>0</td>
    <td><ul>
      <li class="t5">Mettre en évidence l’intérêt du découpage des données en paquets et de leur encapsulation</li>
      <li class="t5">Dérouler le fonctionnement d’un protocole simple de récupération de perte de paquets (bit alterné)</li>
      </ul>
    </td>
  </tr>
  <tr><td>47</td><td>1</td>
    <td><ul>
      <li class="t5">Simuler ou mettre en œuvre un réseau</li>
    </ul></td>
  </tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Module 05 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>47</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Analyser le fonctionnement d’un formulaire simple</li>
      <li class="t4">Distinguer les transmissions de paramètres par les requêtes POST ou GET</li>
    </ul></td>
  </tr>
  <tr><td>48</td><td>1</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Distinguer ce qui est exécuté sur le client ou sur le serveur et dans quel ordre</li>
      <li class="t4">Distinguer ce qui est mémorisé dans le client et retransmis au serveur</li>
    </ul></td>
  </tr>
  <tr><td>48</td><td>2</td>
    <td class="align-middle" rowspan="3">Projet évalué : <b>[ Modules ≤ 05 ]</b>
      <ul class="text-start mb-0"><li class="t4">Reconnaître quand et pourquoi la transmission est chiffrée</li></ul></td>
  </tr><tr><td>49</td><td>3</td>
  </tr><tr><td>49</td><td>4</td>
  </tr>
  <tr><td>50</td><td>X</td>
    <td class="align-middle" rowspan="2">~ séance(s) tampon(s) du trimestre ~</td>
  </tr>
  <tr><td>50</td><td>X</td>
  </tr>
  <tr><td colspan="6" class="vac"><b>Vacances de Noel</b></td></tr>
  <tr><td class="modEven" colspan="3"><div class="float-start"><b>[ Module 06 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>1</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Importer une table depuis un fichier texte tabulé ou un fichier CSV</li>
      <li class="t4">Rechercher les lignes d’une table vérifiant des critères exprimés en logique propositionnelle 1/2 : capacité étalée sur deux séances pour consacrer du temps à réinvestir sur les tableaux en python</li>
    </ul></td>
  </tr>
  <tr><td>1</td><td>1</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Rechercher les lignes d’une table vérifiant des critères exprimés en logique propositionnelle 2/2</li>
    </ul></td>
  </tr>
  <tr><td>2</td><td>2</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Passer de la représentation d’une base dans une autre</li>
      <li class="t4">Évaluer le nombre de bits nécessaires à l’écriture en base 2 d’un entier, de la somme ou du produit de deux nombres entier</li>
    </ul></td>
  </tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Module 07 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>2</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Écrire un algorithme de recherche d’une occurrence sur des valeurs de type quelconque</li>
    </ul></td>
  </tr>
  <tr><td>3</td><td>1</td>
    <td>évaluation sommative : <b>[ Modules ≤ 06 ]</b></td>
  </tr>
  <tr><td>3</td><td>2</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Écrire une fonction renvoyant un p-uplet de valeurs</li>
      <li class="t4">Écrire un algorithme de tri (insertion et sélection)</li>
    </ul></td>
  </tr>
  <tr><td>4</td><td>3</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Décrire un invariant de boucle qui prouve la correction des tris par insertion, par sélection</li>
    </ul></td>
  </tr>
  <tr><td class="modEven" colspan="3"><div class="float-start"><b>[ Module 08 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>4</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Trier une table suivant une colonne</li>
      <li class="t4">Construire une nouvelle table en combinant les données de deux tables</li>
    </ul></td>
  </tr>
  <tr><td>5</td><td>1</td>
    <td>évaluation sommative : <b>[ Modules ≤ 07 ]</b></td>
  </tr>
  <tr><td>5</td><td>2</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Utiliser le complément à 2</li>
      <li class="t4">Calculer sur quelques exemples la représentation de nombres réels : 0.1, 0.25 ou 1/3</li>
    </ul></td>
  </tr>
  <tr><td>6</td><td>3</td>
    <td><ul class="text-start mb-0">
      <li class="t4">?</li>
    </ul></td>
  </tr>
  <tr><td>6</td><td>4</td>
    <td>évaluation sommative : <b>[ Modules ≤ 08 ]</b></td>
  </tr>
  <tr><td>7</td><td>5</td>
    <td><ul class="text-start mb-0">
      <li class="t4">?</li>
    </ul></td>
  </tr>
  <tr><td>7</td><td>6</td>
    <td><ul class="text-start mb-0">
      <li class="t4">?</li>
    </ul></td>
  </tr>
  <tr><td colspan="6" class="vac"><b>Vacances de Février</b></td></tr>
  <tr><td>10</td><td>X</td>
    <td class="align-middle" rowspan="2">~ séance(s) tampon(s) du trimestre ~</td>
  </tr>
  <tr><td>10</td><td>X</td></tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Module 09 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>11</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Construire une entrée de dictionnaire</li>
      <li class="t4">Itérer sur les éléments d’un dictionnaire</li>
    </ul></td>
  </tr>
  <tr><td>11</td><td>1</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Identifier l’intérêt des différents systèmes d’encodage</li>
      <li class="t4">Convertir un fichier texte dans différents formats d’encodage</li>
    </ul></td>
  </tr>
  <tr><td class="modEven" colspan="3"><div class="float-start"><b>[ Module 10 ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>12</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Utiliser des tableaux de tableaux pour représenter des matrices : notation a [i] [j]</li>
    </ul></td>
  </tr>
  <tr><td>12</td><td>1</td>
    <td>évaluation sommative : <b>[ Modules ≤ 09 ]</b></td>
  </tr>
  <tr><td>13</td><td>2</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Écrire un algorithme qui prédit la classe d’un élément en fonction de la classe majoritaire de ses k plus proches voisin</li>
    </ul></td>
  </tr>
  <tr><td>13</td><td>3</td>
    <td><ul class="text-start mb-0">
      <li class="t4">Résoudre un problème grâce à un algorithme glouton</li>
    </ul></td>
  </tr>
  <tr><td>14</td><td>4</td>
    <td>évaluation sommative : <b>[ Modules ≤ 10 ]</b></td>
  </tr>
  <tr><td class="modOdd" colspan="3"><div class="float-start"><b>[ Module de fin d'année ]</b></div><div class="d-flex justify-content-end">
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">apprenant</a></div>
    <div class="px-2"> | </div>
    <div><i class="fa fa-folder isDirectory"></i> <a target="_blank" href="">enseignant</a></div>
  </div></td></tr>
  <tr><td>14</td><td>0</td>
    <td><ul class="text-start mb-0">
      <li class="t4">?</li>
      <li class="t4">?</li>
    </ul></td>
  </tr>
  <tr><td>15</td><td>1</td>
    <td><ul class="text-start mb-0">
      <li class="t4">?</li>
      <li class="t4">?</li>
    </ul></td>
  </tr>
  <tr><td>15</td><td>2</td>
    <td><ul class="text-start mb-0">
      <li class="t4">?</li>
      <li class="t4">?</li>
    </ul></td>
  </tr>
  <tr><td>16</td><td>3</td>
    <td><ul class="text-start mb-0">
      <li class="t4">?</li>
      <li class="t4">?</li>
    </ul></td>
  </tr>
  <tr><td>16</td><td>4</td>
    <td><ul class="text-start mb-0">
      <li class="t4">?</li>
      <li class="t4">?</li>
    </ul></td>
  </tr>
  <tr><td colspan="6" class="vac"><b>Vacances de Printemps</b></td></tr>
  <tr><td>19</td><td>5</td>
    <td><ul class="text-start mb-0">
      <li class="t4">?</li>
      <li class="t4">?</li>
    </ul></td>
  </tr>
  <tr><td>19</td><td>6</td>
    <td><ul class="text-start mb-0">
      <li class="t4">?</li>
      <li class="t4">?</li>
    </ul></td>
  </tr>
  <tr><td>20</td><td>7</td>
    <td><ul class="text-start mb-0">
      <li class="t4">?</li>
      <li class="t4">?</li>
    </ul></td>
  </tr>
  <tr><td>20</td><td>8</td>
    <td><ul class="text-start mb-0">
      <li class="t4">?</li>
      <li class="t4">?</li>
    </ul></td>
  </tr>
  <tr><td>21</td><td>X</td>
    <td class="align-middle" rowspan="2">~ séance(s) tampon(s) du trimestre ~</td>
  </tr>
  <tr><td>21</td><td>X</td></tr>
  
  
</tbody>
</table>

## État des lieux de la programmation

- nombre semaines totales : 30		
    - nombre semaines cours + évaluations : 19		
    - nombre semaines projets : 8	
    - nombre semaines tampons : 3	

=> ce qui nous donne un ration de 8 / 30 ~= 26% de temps consacré à des projets

=> réflexion toujours en cours : garder le plus gros des projets pour la fin de l'année scolaire ou davantage les distiller au fil de l'année ?

## Anciens calculs avant l'ajout de la progression de Noel à la fin de l'année

### Entre le début de l'année et Noël : 
- 13 semaines sans interruption (sans jour(s) férié(s), ..) se sont déroulées 
- 24 capacités sont travaillées / évaluées  pour 26 séances (ration : ~0.9231 capa/séance)
- sur 26 séances, 4 séances sont prévues pour des projets (entre ~1/6 et ~1/7 du temps, au lieu de ~1/4 comme explicité dans le programme officiel) : 
    - Après noël, les projets devront être beaucoup plus présents !

### Restant de Noel à fin Mai : 
- environ 34 séances restantes / 17 semaines  (sans compter Juin)
    - 9 semaines sont consacrées à des cours 
    - 6 semaines sont consacrées à des projets

- 50 - 24 = 26 capacités à travailler et évaluer (ratio : ~= 0.7647 capa/séance)
    - Ce faible ratio laissera plus de place pour des projets 
- au rythme de 0.9231 capa/séance (dans le meilleur des mondes),~28 séances suffiraient pour tout boucler au lieu de 34


## Mises à jours de la programmation

<table>
  <thead>
    <tr class="text-center">
      <th>Date</th>
      <th>Description</th>
      <th>Modification</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>14/09/2021</td>
      <td><ul>
        <li>les aléas de la rentrée et le niveau des élèves ont mal été anticipés. Le module 00 ne s'est pas déroulé en deux semaines comme prévu initialement</li>
        <li>Comme les sujets sont importants et seront récurent tout au long de l'année, je préfère donc prendre plus de temps dans ce module pour faire le tour des notions principales</li>
      </ul></td>
      <td><ul>
        <li>La durée du module 00 a été modifié de 2 semaines à 3 semaines (soit deux séances de 2 heures supplémentaires) </li>
        <li>En contrepartie, le temps alloué aux projets avant noël a été réduit</li>
      </ul></td>
    </tr>
    <tr>
      <td>11/11/2021</td>
      <td><ul>
        <li>Au lycée Beauséjour sont organisés des contrôles trimestriels. J'avais anticipé, entre autre, ce coup en ajoutant deux séances 'tampons' avant Noel au cas où. Les élèves de premières n'ont raté au final qu'une seule séance</li>
      </ul></td>
      <td><ul>
        <li>Pour garder cette progression flexible, j'ai donc réservé deux séances "tampons" dans le trimestre en les plaçant par défaut avant les vacances de Noel. Ces séances pourront servir à amortir les divers aléas du premier trimestre et je pense que je vais répéter ce process pour les deux autres trimestres</li>
      </ul></td>
    </tr>
    <tr>
      <td>11/11/2021</td>
      <td><ul>
        <li>Ajout de la progression de début janvier à fin Mai</li>
      </ul></td>
      <td><ul>
        <li>La progression janvier à fin Mai, conçue avant septembre, est maintenant posée en détail dans ce document</li>
        <li>Des statistiques sur l'ensemble de l'année ont été ajoutées dans "État des lieux de la programmation". J'ai laissé les anciennes projections (rédigées avant septembre) en dessous de cette partie</li>
      </ul></td>
    </tr>
    <tr>
      <td>25/11/2021</td>
      <td><ul>
        <li>Suppression de la séance 1 module 5 (évaluation module 4)</li>
      </ul></td>
      <td><ul>
        <li>Le but est de laissez davantages de temps pour le gros projet de décembre dans lequel sont  évaluées les capacités du module 4</li>
        <li>Le jour est ajouté dans le capital "séance tampon du trimestre"</li>
      </ul></td>
    </tr>
    <tr>
    <td>22/12/2021</td>
      <td><ul>
        <li>Ajout d'une séance supplémentaires pour le projet 'clicker game', on passe de 2 à 3 séances (sans compter la séance de présentations des projets) car plus volumineux que prévu</li>
      </ul></td>
      <td><ul>
        <li>Pas de modification pour l'instant dans le planning général, j'amortis le décalage avec une séance tampon du deuxième trimestre car les séances tampon du premier trimestre ont été prises</li>
        <li>MAJ (05/01/22) : les présentations s'étalent sur cette semaine et la suivante (pour les groupes qui n'ont pas rendu du projet encore), le temps pris pour les présentations cette semaine n'a finalement pas décalé mon planning</li>
      </ul></td>
    </tr>
    <tr>
    <td>09/01/2022</td>
      <td><ul>
        <li>Dans la progression actuelle, la capacité "Utiliser le complément à 2" est abordée avant la capacité "Évaluer le nombre de bits nécessaires à l’écriture en base 2 d’un entier, de la somme ou du produit de deux nombres entiers" (du thème : "Représentation des données: types et valeurs de base"). Or le complétment à 2 suppose que les élèves sachent déjà additionner des nombres binaires entre eux</li>
      </ul></td>
      <td><ul>
        <li>J'inverse les deux capacités dans la progression, ce qui fait plus sens</li>
      </ul></td>
    </tr>
  </tbody>
</table>