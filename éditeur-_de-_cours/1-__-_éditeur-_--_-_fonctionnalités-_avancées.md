# Éditeur : fonctionnalités avancées

+++ sommaire

L'éditeur utilise le langage Markdown avec certaines extensions développées sur mesure. Vous trouverez ci-dessous les fonctionnalités basiques de l'éditeur.

## Utiliser du code HTML/CSS, Bootstrap et de Fontawesome

Pour un theming maximal, il est possible d'utiliser des balises HTML et d'y ajouter du code css. 
De plus, Bootstrap v5 et [fontawesome v5](https://fontawesome.com/icons?d=gallery) s'invitent dans la partie ! 

Par exemple, si dans l'éditeur vous écrivez :

```html
<style>
.bgr{
    background-color:#ff8f6c;
}
.bgg{
    background-color:#76f07e;
}
.bgb{
    background-color:#94dbd4;
}
<\/style>

<div class="d-flex flex-wrap col-8 offset-2 border border-dark mb-3">
    <div class="p-2 m-2 border bgr"><i class="fas fa-7x fa-chess"></i></div>
    <div class="p-2 m-2 border bgg"><i class="fas fa-7x fa-cat"></i></div>
    <div class="p-2 m-2 border bgb"><i class="fas fa-7x fa-laptop-code"></i></div>
    <div class="p-2 m-2 border bgr"><i class="fas fa-7x fa-gamepad"></i></div>
    <div class="p-2 m-2 border bgg"><i class="fas fa-7x fa-music"></i></div>
    <div class="p-2 m-2 border bgb"><i class="fas fa-7x fa-plane"></i></div>
<\/div>
```

Cela produit le résultat suivant : 

<style>
.bgr{
    background-color:#ff8f6c;
}
.bgg{
    background-color:#76f07e;
}
.bgb{
    background-color:#94dbd4;
}
</style>
<div class="d-flex flex-wrap col-8 offset-2 border border-dark mb-3">
    <div class="p-2 m-2 border bgr"><i class="fas fa-7x fa-chess"></i></div>
    <div class="p-2 m-2 border bgg"><i class="fas fa-7x fa-cat"></i></div>
    <div class="p-2 m-2 border bgb"><i class="fas fa-7x fa-laptop-code"></i></div>
    <div class="p-2 m-2 border bgr"><i class="fas fa-7x fa-gamepad"></i></div>
    <div class="p-2 m-2 border bgg"><i class="fas fa-7x fa-music"></i></div>
    <div class="p-2 m-2 border bgb"><i class="fas fa-7x fa-plane"></i></div>
</div>

## Faire parler un avatar prédéfinis 

Vous pouvez invoquer des avatars prédéfinis et même les faire parler ! 

Par exemple, si dans l'éditeur vous écrivez :

```
+++bulle matthieu
Hello World !
+++
```

```
+++bulle emma
Je dirai même plus, hello World !!
+++
```


Cela produit le résultat suivant : 

+++bulle matthieu
Hello World !
+++
+++bulle emma
Je dirai même plus, hello World !!
+++

Il est possible d'ajouter des paramètres à notre bulle tels que : 
- en entête du bloc
    - la position de l'image de l'avatar (droite ou gauche, gauche par défaut)
    - la taille de l'image de l'avatar en poucentage
- au pied du bloc
    - les classes du bloc, par exemple : "col-6 col-lg-4"

Par exemple, si dans l'éditeur vous écrivez :


![code exemple](https://i.ibb.co/d7kr8JN/Screenshot-from-2021-01-17-20-07-48.png)

Cela produit le résultat suivant :

<div class="d-flex justify-content-center align-items-center">
<div>
+++bulle matthieu droite 250
À quelle vitesse vole une hirondelle ?
+++col-12
</div>
<div>
+++bulle kevin
<span class="font-weight-bold"> vous parlez de quelles hirondelles? Celles d’Afrique ou d’Europe?</span>
+++
+++bulle lucas
Banane flambée !
+++
+++bulle sarah
Obi Wan Kenobi <i class="fas fa-2x fa-jedi"></i>
+++
</div>
</div>

### Liste des avatars disponibles

Ci-dessous, la liste des avatars disponibles : 

<div class="d-flex flex-wrap col-12 mb-3 justify-content-center">
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/alexandre.png" style="width:100%">
        <p class="couleur-alexandre text-center mb-0 font-weight-bold">Alexandre</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/camille.png" style="width:100%">
        <p class="couleur-camille text-center mb-0 font-weight-bold">Camille</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/clara.png" style="width:100%">
        <p class="couleur-clara text-center mb-0 font-weight-bold">Clara</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/david.png" style="width:100%">
        <p class="couleur-david text-center mb-0 font-weight-bold">David</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/emma.png" style="width:100%">
        <p class="couleur-emma text-center mb-0 font-weight-bold">Emma</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/enzo.png" style="width:100%">
        <p class="couleur-enzo text-center mb-0 font-weight-bold">Enzo</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/julie.png" style="width:100%">
        <p class="couleur-julie text-center mb-0 font-weight-bold">Julie</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/kevin.png" style="width:100%">
        <p class="couleur-kevin text-center mb-0 font-weight-bold">Kevin</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/lucas.png" style="width:100%">
        <p class="couleur-lucas text-center mb-0 font-weight-bold">Lucas</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/lucie.png" style="width:100%">
        <p class="couleur-lucie text-center mb-0 font-weight-bold">Lucie</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/manon.png" style="width:100%">
        <p class="couleur-manon text-center mb-0 font-weight-bold">Manon</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/matthieu.png" style="width:100%">
        <p class="couleur-matthieu text-center mb-0 font-weight-bold">Matthieu</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/romain.png" style="width:100%">
        <p class="couleur-romain text-center mb-0 font-weight-bold">Romain</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/sarah.png" style="width:100%">
        <p class="couleur-sarah text-center mb-0 font-weight-bold">Sarah</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/yanis.png" style="width:100%">
        <p class="couleur-yanis text-center mb-0 font-weight-bold">yanis</p>
    </div>
    <div class="p-2 m-2 border border-dark bg-dark col-4 col-md-3 col-lg-2">
        <img src="/images/avatars/r3do.png" style="width:100%">
        <p class="couleur-yanis text-center mb-0 font-weight-bold">r3do</p>
    </div>
</div>


## Créer des diagrammes

L'éditeur utilise une librairie javascript permettant de générer des diagrammes tout choupi. La lib en question est [mermaid](https://mermaid-js.github.io/mermaid/#/)

Quelques exemples ci-dessous. Si dans l'éditeur vous écrivez :

![diagrammes](https://i.ibb.co/HqmZr1Y/Screenshot-from-2021-08-08-01-02-09.png)

Cela produit les résultats suivants : 

+++ diagramme
graph TD
    item1[initialisation] ==> item2[création d'un item coloté en bleu]
    style item2 color:blue
    item2 --> item3[ajout d'un item avec une icone fontawesome fab:fa-font-awesome-flag]
    item3 --> item4[je suis un lien cliquable !]
    item2 --> item4
    item3 --> item5[<img src="https://i.ibb.co/hYPttb4/planet.png" width="150">]
    click item4 "https://youtu.be/4TKcz5AqcnQ" _blank
+++

+++ diagramme
sequenceDiagram
    Client->>Serveur: hey ! je souhaite accéder à la page 'se connecter'
    Serveur-->>Client: Très bien ! je l'ai construite, je te l'envoie !
+++

+++ diagramme
 classDiagram
      Animal <|-- Canard
      Animal <|-- Poisson
      Animal <|-- Zèbre
      Animal : +int age
      Animal : +String genre
      Animal: +estUnMammifère()
      class Canard{
          +String couleur
          +nager()
      }
      class Poisson{
          -int taille
          -nager()
      }
      class Zèbre{
          +bool estSauvage
          +galoper()
      }
+++