# Créer un cours

Pour **créer un cours**, il suffit de cliquer sur le bouton "Créer un cours !" ci-dessous afin d'accéder à l'éditeur de cours "infoludique".

<div class="d-flex mb-2"><a href="/enseignant/editeur-_de-_cours/3-__-_creer-_un-_cours.md/edit" class="text-center m-auto btn btn-success" type="button">Créer un cours !</a></div>

**Attention :** pendant la rédaction de votre cours, vous devrez utliser le bouton <a class="p-1 actionButton d-inline"><i class="fa-1x fas fa-sync"></i></a> situé en bas à gauche pour transformer votre texte et sauvegarder dans le localstorage de votre navigateur. Ce bouton possède trois **états** :

<ul>
    <li class="mb-1"><a class="p-1 actionButton d-inline"><i class="fa-1x fas fa-sync"></i></a> <b>état 'manuel'</b> : un clic sur le bouton met à jour l'éditeur et la sauvegarde</li>
    <li class="mb-1"><a class="p-1 actionButton d-inline"><i class="fa-1x fas fa-sync slow-spin"></i></a> <b>état 'automatique lent'</b> : éditeur et la sauvegarde sont automatiquement mis à jour toutes les 10 secondes</li>
    <li class="mb-1"><a class="p-1 actionButton d-inline"><i class="fa-1x fas fa-sync fa-spin"></i></a> <b>état 'automatique temps réel'</b> : éditeur et la sauvegarde sont automatiquement mis à jour à chaque caractère écris (peut provoquer des lenteur en fonction de la taille du cours et de la puissance de l'ordinateur)</li>
</ul>

Pour changer le bouton <a class="p-1 actionButton d-inline"><i class="fa-1x fas fa-sync"></i></a> d'<b>état</b>, il suffit de double cliquer dessus :)

Une fois votre cours rédigé, vous pouvez le partager à vos apprenants comme suit :
1. Enregistrez votre cours dans un fichier texte
    - copiez le contenu du cours (le texte .md dans la partie gauche de votre écran) et collez le texte dans un fichier que vous avez créé sur votre ordinateur
2. Transmettez votre fichier à vos apprenants
3. Invitez vos apprenants à se rendre sur la [page de chargement de cours](/enseignant/editeur-_de-_cours/4-__-_charger-_un-_cours.md) et d'y charger votre fichier

## FAQ :

### Comment fonctionne cet éditeur ?
Cet éditeur utilise le langage [Markdown](https://en.wikipedia.org/wiki/Markdown), un langage à la syntaxe claire, légère et faisant partie des standards. D'autres librairies sont également utilisables. (bootstrap / fontawesome / mermaid / ...)

Vous pourrez voir les fonctionnalités disponibles de l'éditeur "infoludique" dans les pages :

- [Éditeur : fonctionnalités basiques][1]
- [Éditeur : fonctionnalités avancées][2]
- [Éditeur : blocs de codes][3]

### Help ! Mon ordinateur s'est éteint à cause d'une panne électrique ! j'étais en pleine rédaction de cours !
Don't panic ! Le cours est continuellement sauvegardé dans le [localstorage](https://fr.wikipedia.org/wiki/Stockage_web_local) de votre navigateur. Il vous suffit de revenir sur la page de l'éditeur de cours "infoludique" pour retrouver votre travail.

La sauvegarde dans le localstorage a également d'autres conséquences :
- Le serveur qui héberge cet éditeur **ne reçoit, ne stocke ni ne traite aucune donnée** relatives à votre cours.
- Votre cours disparaîtra si vous remettez à zéro votre navigateur. (pensez à sauvegarder votre écrit de temps en temps sur un fichier à part)

Dû à l'utilisation du localstorage de votre navigateur, il est <span style="color:red;font-weight:bold;">fortement déconseillé</span> d'utiliser l'éditeur de cours "infoludique" sur plusieurs onglets  la fois.

[1]: /enseignant/editeur-_de-_cours/1-__-_editeur-_--_-_fonctionnalites-_basiques.md
[2]: /enseignant/editeur-_de-_cours/1-__-_editeur-_--_-_fonctionnalites-_avancees.md
[3]: /enseignant/editeur-_de-_cours/2-__-_editeur-_--_-_blocs-_de-_codes.md