# cahier des charges du projet Air Quality Controller

+++sommaire

## Présentation du document

Différentes personnes de tous bords peuvent intervenir dans un projet informatique. Un cahier des charges permets de poser sur papier les attendus du ou des commanditaires du projet et les environnements et stratégies mis en places pour concrétiser le projet. Il s'agit en règle générale d'un document conséquent évoluant au fil du projet.

Ce cahier des charges, de taille modeste, ne changera pas.

Pour terminer, un court descriptif des différentes parties du projet. 
- **Présentation du projet** : contextualise et présente le projet dans ses grandes lignes
- **Spécifications fonctionnelles** : décrit le fonctionnement de l'application depuis le point de vue d'un utilisateur non technique. Cette partie est en général rédigée par le chef de projet informatique avec l'aide du commanditaire du projet
- **Spécifications techniques** : rassemble les informations techniques relatives à la mise en oeuvre le projet. Cette partie est rédigé par les informaticiens au sens large (architecte, lead/référent technique, ...)
- **Planification du projet** : cette partie expose les différentes phases de développement du projet

## Présentation du projet

<blockquote class="blockquote">
  <p>L’éducation à l’environnement et au développement durable est porteuse d’enjeux essentiels en termes d’évolutions des comportements, de connaissances nouvelles et de mise en capacité de chacun, au quotidien, d’être acteur de la transition et de la mise en œuvre des objectifs de développement durable.</p>
  <footer class="blockquote-footer"><cite title="Source Title">https://www.ecologie.gouv.fr/leducation-lenvironnement-et-au-developpement-durable</cite></footer>
</blockquote>

L'activité environnente et humaine influent sur la composition de l'air. Cette composition peut impacter de manière néfaste la santé des organismes vivants. 

Le but du projet "**A**ir **Q**uality **C**ontroller" est de développer une application permettant de consulter diverses données et statistiques liées à l'état de l'atmosphère et à la composition de l'air. Ces données sont récoltées à travers un ensemble de stations "air quality control".

Une station est mise à disposition à l'équipe de développeur, toute station contiendra un raspberry, un module 'sensor hat' et un module 'nova SDS011' (plus de détails dans les spécifications techniques).

## Spécifications fonctionnelles

Dans cette partie figure la liste des fonctionnalités à implémenter.

### Fonctionnalité obligatoire

L'utilisateur souhaite pouvoir observer les corrélations qu'il peut y avoir entre les données collectées par les deux modules et le lieu où se trouve la station.

On en déduit que l'utilisateur devra d'abord selectionner la station qui l'intéresse et pourra ensuite accéder aux données de cette dernière.

Cette fonctionnalité est compliquée à développer d'une seule traite. Pour faciliter les développements, elle peut être découpée en deux principales parties ; la partie "Base de données" et la partie "Application". La fiche de suivi de projet vous accompagne pour la partie "Base de données" **qui doit être terminée en priorité avant tout le reste**.

L'application doit in fine afficher sous la forme d'un tableau les données collectées par les deux modules d'une station données.

### Fonctionnalités optionnelles

Les fonctionnalités ci-dessous, **optionnelles**, demandent un peu plus de doigté pour être développées. Il vous est possible de proposer des idées qui devront être validées par votre enseignant.

**Fonctionnalité optionnelle 1 :**
- L'utilisateur souhaite pouvoir visualiser les dernières données collectées sous la forme d'un graphique. Affichez ces données sous la forme d'un graphique en courbe (📉)
- Aller plus loin : L'utilisateur souhaite que l'application soit visuellement agréable, ergonomique et accessible. Effectuez des recherches dans ces domaines et améliorez l'application en conséquence

**Fonctionnalité optionnelle 2 :**
- L'utilisateur a parfois besoin, pour diverses raisons, de déplacer des stations 'air quality control'. À travers l'application, donnez la possibilité à l'utilisateur de renseigner le nom du nouveau lieu pour une station donnée. Les données collectée par la stations doivent être alors supprimée de la base de données et le nom du lieu de la station doit être modifié.

**Fonctionnalité optionnelle 3 :**
- Aficher un 'air-quality-control-score' qui, se servant des dernières données récoltées par les modules, affiche une lettre notant la qualité de l'air. Cette note ira de A (qualité optimale) à E (qualité exécrable). Ce score s'inspire du [nutri-score](https://www.mangerbouger.fr/Manger-mieux/Comment-manger-mieux/Comment-comprendre-les-informations-nutritionnelles/Qu-est-ce-que-le-Nutri-Score) que l'on trouve sur certain produits consommables dans le commerce
- Aller plus loin : dans un soucis de transparance et de pédagogie vis à vis de l'utilisateur, affichez de manières claires et compréhensibles les raisons qui expliquent le score obtenu

## Spécifications techniques

### Environnement technique

Ci-dessous est décrit l'environnement technique sur lequel vous travaillerez pendant vos développements ainsi que son utilisation.

Le raspberry PI est un SBC (**S**ingle **B**oard **C**omputer) qui utilise dans sa 4ième version un système d'exploitation au nom très original "Raspberry PI OS". Ce système d'exploitation se repose sur un noyau Linux. Les lignes de commandes seront donc très similaires aux lignes de commandes utilisées dans d'autres systèmes d'exploitation Linux connus tels que Ubuntu ou encore Debian.

Les données liées à l'état de l'atmosphère et la composition de l'air sont enregistrées sur un raspberry PI. (version 4)

![](https://i.ibb.co/vkcBJtk/01.jpg)

... avec l'aide du module "sensort hat" 

![](https://i.ibb.co/gwLr4B0/02.jpg)

... et du module "nova SDS011". 

![](https://i.ibb.co/2n5B8F2/03.jpg)

Les modules permettent de collecter diverses données liées à la composition de l'air.

Pour accéder à ces données depuis le raspberry, **vous devrez vous y connecter via le protocol ssh** depuis un poste informatique connecté sur le même réseau. Le raspberry est déjà configure pour accepter des connexion entrantes à travers le protocol ssh. Si votre poste informatique est sur Windows, nous vous conseillons d'utiliser le logiciel freesource [putty](https://www.putty.org/).

Vous aurez besoin de votre **identifiant** et de votre **mot de passe** pour mener à bien votre connexion ssh, rapprochez vous de votre enseignant si besoin. Vous aurez également besoin de l'adresse IP du raspberry, elle est affichée sur l'étiquette attachées au raspberry. 

### Contrainte technique

Pour des questions de performances, les données ne doivent pas être toutes regroupées au sein de la même. Chaque module doit posséder sa propre table. Ainsi, toutes les données récoltées par les modules 'sensorhat' (resp. 'nova SDS011') de toutes les stations doivent être stockées dans une table nommée 'sensorhat' (resp. 'sds011').

Toujours pour des questions de performance, il faudra privilégier les requêtes de jointures lorsque c'est possible.

### Bonnes pratiques

**⬇️ partie en cours de rédaction ⬇️**

#### bien structurer son code

Concentrer tous le code dans un même endroit n'est jamais une bonne idée.

**⬆️ partie en cours de rédaction ⬆️**

## Planification du projet

Le développement du projet est découpé en 3 phases.

### Phase 1 : conception et création de la base de données "AQC"

Durée de la phase : environ 1h15

À la fin de cette phase, vous devrez : 
- avoir créé la base de données locale nommée "AQC" avec une table par module (une table "sensortHat" et une table "novaSDS011")
- avoir ajouté quelques données dans la base de données
- avoir complété la fiche de suivis de projet (partie "Phase 1")

### Phase 2 : peuplement et exploitation de la base de données "AQC"

Durée de la phase : 1 séance de 2 heures

À la fin de cette phase, vous devrez : 
- avoir créé un script python qui :
    - récupère les données collectées par les modules d'une station
    - mets à jour les données de votre base de données local "AQC"
- avoir complété la fiche de suivis de projet (partie "Phase 2")

### Phase 3 : développement de l'interface graphique

Durée de la phase : 3 séances (séance 1, séance 2 et séance 3) de 2 heures

Tout au long de ces séances vous devrez vous assurer de tenir à jour votre fiche de suivi de projet

À la fin de cette phase, vous devrez : 
- avoir développé la <a href="#fonctionnalitobligatoire">fonctionnalitée obligatoire</a> attendue 
- (optionnel) avoir développé une fonctionnalité supplémentaire : une fonctionnalité que vous proposez, ou, une fonctionnalité parmis celles exposées dans les <a href="#fonctionnalitsoptionnelles">fonctionnalités optionnelles</a>

