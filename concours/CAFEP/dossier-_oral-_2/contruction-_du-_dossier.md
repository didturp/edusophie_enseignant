# Construction du dossier pour l'oral 2

+++ sommaire

## Introduction

Ici est décrit un projet pédagogique s'inscrivant dans le thème "Bases de données" du programme de Terminale NSI. Je décris également la séquence durant laquelle se passe le projet, cela m'aide à le construire. 

**Pout l'instant**, je pense qu'à terme, le dossier qui sera fournis aux jury pourrait contenir à minima :
- L'idée principale du projet
- Son intégration dans une séquence 
- Ses liens avec le programme officiel
- Mes principaux choix pédagogiques faits pour la mise en place du projet
- Les différentes phases du projets
- Deux activités hors projet : la toute premiere activité introductive en Séance 1, une activité en cours de séquence dédiée aux clé étrangères et jointures entre tables.

~ à réflexionner/paufiner ~


**Pout l'instant toujours**, je pense que mes 30 minutes de presentations orale pourraient se focaliser sur : 
- Une présentation de l'activité introductive de séquence
- Une présentation du projet

~ à réflexionner/paufiner ~

## Fiche récapitulative du projet

<table class="table col-10 mx-auto">
    <tbody>
        <tr>
            <th scope="row">Résumé de l'activité</th>
            <td>
                <p>Les objectifs du projet :</p>
                <ul>
                    <li>Concevoir et créer une base de données adaptées aux données récoltées par le raspberry (à travers ses modules)</li>
                    <li>Réfléchir et développer une solution permettant à des utilisateurs non informaticiens de profiter des données de la base de données</li>
                </ul>
            
                <p>Le projet se scinde en 3 phases ;</p> 
                <ul>
                    <li>les deux premières, guidées (séances TP), se dérouleront avec des binômes composés par les élèves</li>
                    <li>la dernière (plus longue) se déroulera dans des équipes composées de deux binômes.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <th scope="row">Niveau concerné</th>
            <td>Terminale NSI</td>
        </tr>
        <tr>
            <th scope="row">Objectifs visés: connaissances, techniques, méthodes, compétences transversales, dimensions éthiques, juridiques, économiques ou environnementales</th>
            <td>
                <b>Connaissance(s)</b>
                <ul>
                    <li>language SQL : requêtes de recherche</li>
                    <li>principales lignes de commandes dans un terminal linux</li>
                    <li class="text-red">à compléter</li>
                </ul>
                <b>Technique(s)</b>
                <ul>
                    <li>Concevoir une base de données</li>
                    <li>Créer une base de données SQL</li>
                    <li>Peupler une base de données SQL</li>
                    <li>Extraire les informations d'une base de données SQL</li>
                </ul>
                <b>Compétence(s) du programme</b>
                <ul>
                    <li>analyser et modéliser un problème en termes de flux et de traitement d’informations</li>
                    <li>décomposer un problème en sous-problèmes, reconnaître des situations déjà analysées et réutiliser des solutions</li>
                    <li>mobiliser les concepts et les technologies utiles pour assurer les fonctions d’acquisition, de mémorisation, de traitement et de diffusion des informations</li>
                    <li>traduire un algorithme dans un langage de programmation, en spécifier les interfaces et les interactions, comprendre et réutiliser des codes sources existants, développer des processus de mise au point et de validation de programmes</li>
                    <li>? développer des capacités d’abstraction et de généralisation ?</li>
                </ul>
                <b>Compétence(s) transversale(s)</b>
                <ul>
                    <li>faire preuve d’autonomie, d’initiative et de créativité</li> 
                    <li>présenter un problème ou sa solution, développer une argumentation dans le cadre d’un débat</li>
                    <li>coopérer au sein d’une équipe dans le cadre d’un projet</li>
                    <li>rechercher de l’information, partager des ressources</li>
                </ul>
                <b>Dimension(s) environnementale(s)</b>
                <ul>
                    <li>Connaitre les principaux facteurs liés à la qualité de l'air en France</li>
                    <li>Établir des corrélations (si il y a) entre la température, la pression atmosphérique ou encore le taux d'humidité avec le taux de particules fines</li>
                </ul>
            </td>
        </tr>
        <tr>
            <th scope="row">Durée approximative (Nb séance - Temps)</th>
            <td>
                <p>Le projet :</p>
                <ul><li>4 séances de 2 heures</li></ul>
                <p>La séquence :</p>
                <ul><li>12 séances au total</li></ul>
            </td>
        </tr>
        <tr>
            <th scope="row">Environnement Informatique: matériel, langages</th>
            <td>
                <ul>
                    <li>Sur les postes sur lesquels travaillerons les élèves</li>
                    <ul>
                        <li>Le logiciel <a href="https://sqlitebrowser.org/dl" target="_blank">"SQLite Browser"</a></li>
                        <li>Le logiciel <a href="https://www.putty.org/" target="_blank">"Putty"</a></li>
                    </ul>
                    <br>
                    <li>Pour toute la salle</li>
                    <ul>
                        <li>Un <a href="https://www.raspberrypi.org/products/raspberry-pi-4-model-b" target="_blank">"raspberry PI 4 4Go"</a> configuré avec :</li>
                        <ul>
                            <li>un accès SSH sur un environnement virtuel aux droits restreints ( pas de sudo et nombre de commandes limités ;) )</li> 
                            <li>les packages nécessaires pour pouvoir travailler avec les SGBD MySQL et SQLite (à détailler)</li> 
                            <li>deux commandes disponibles ; une par module pour afficher les données collectées en temps réel</li>
                            <li>une IP fixe</li>
                            <li>un module <a href="https://www.raspberrypi.org/products/sense-hat" target="_blank">"Sense Hat"</a> ( récolte, entre autre, de données telles que la pression atmosphérique, la température ou encore le taux d'humidité )</li>
                            <li>un module <a href="http://www.inovafitness.com/en/a/chanpinzhongxin/95.html" target="_blank">"Nova pm2.5 SDS011"</a> ( récolte de données relatives aux particules fines )</li>
                            <li>à compléter ( carte SD, cables, boitié troué/transparent, ... )</li>
                        </ul>
                        <li>Un routeur pour créer un réseau local sur lequel sera connecté le raspberry (via éternet) et les ordinateurs des élèves</li>
                        <li>Sur le raspberry sera attachée une étiquette avec marquée dessus l'ip pour y accéder</li>
                    </ul>
<br>
                    <p><b>note :</b> le raspberry, le module "Sense Hat" et le routeur sont utiles dans d'autres parties du programme SNT et NSI</p> 
                </ul>
            </td>
        </tr>
        <tr>
            <th scope="row">Lien avec le programme scolaire</th>
            <td>
                <a href="#liensavecleprogrammescolaire">Détaillé plus bas</a>
            </td>
        </tr>
        <tr>
            <th scope="row">Pré-requis pour les élèves</th>
            <td>
                <b>Compétences issues du programme de NSI en première</b>
                <ul>
                    <li>Mobiliser des concepts et les technologies utiles pour assurer les fonctions d'acquisition, de mémorisation, de traitement et de diffusion des informations</li>
                    <li>Développer des capacités d'abstraction et de généralisation</li>
                </ul>
                <b>Compétences déduites des thèmes concernés</b>
                <ul>
                    <li>Thème "Interaction entre l'homme et la machine sur le Web"</li>
                    <ul>
                        <li>Distinguer ce qui est exécuté sur le client ou sur le serveur et dans quel ordre</li>
                        <li>Distinguer ce qui est mémorisé dans le client et retransmis au serveur</li>
                    </ul>
                    <li>Thème "Architectures matérielles et systèmes d'exploitations"</li>
                    <ul>
                        <li>Utiliser les commandes de base en ligne de commande</li>
                        <li>Gérer les droits et permissions d'accès aux fichiers</li>
                    </ul>
                </ul>
            </td>
        </tr>
        <tr>
            <th scope="row">Type d'évaluation prévue: TP, QCM, Projet, ...</th>
            <td>
                <ul>
                    <li>En fin de projet, les dossiers individuels de suivis de projet seront ramassés et notés (les critères sont détaillés dans les dossiers)</li> 
                    <li>En fin des séances 7-8-11 : ramasser les dossiers individuels de suivis de projet, à noter et commenter (coef faible)</li> 
                    <li>En fin de séquence : évaluation écrite sommative (coef important)</li> 
                    <li>En fin de séquence : présentation des projets (coef moyen)</li> 
                </ul>
            </td>
        </tr>
    </tbody>
</table>

## Organisation générale de la séquence "Bases de données"

Trois activités seront présentées dans le dossier transmis au jury. La troisième activité, s'étalant sur plusieurs séances, sera celle présentée lors de l'oral (et peut être la première aussi). 

<ul>
    <li>La première activité "<b><span style="background-color:rgb(255, 202, 202)">activité introductive</span></b>" du dossier est l'activité qui introduit la séquence</li>
    <li>La deuxième activité "<b><span style="background-color:rgb(202, 255, 202)">activité 'des tables et des jointures'</span></b>" a lieu pendant la séquence, plus précisemment pendant la séance dédiée aux clés étrangères et jointures de tables</li>
    <li>La troisième activié , qui est en fait un projet, "<b><span style="background-color:rgb(202, 202, 255)">projet 'air quality control'</span></b>" a lieu en fin de séquence. Ce projet démarrera par deux séances types TP (mais empruntant quelques mécanismes de pédagogie de projet). S'en suivra deux autres séances plus orientées projet où chaque groupe d'élève devra répondre à la problématique finale du projet "Comment valoriser des données stockées dans une base de données pour des utilisateurs non techniques ?"</li>
</ul>

<table class="table col-10 mx-auto">
  <tbody>
    <tr>
        <th rowspan="3" style="width: 0" class="text-nowrap">Semaine 1</th>
        <th class="text-nowrap">Séance 1</th>
        <td>
            <ul>
                <li>Introduction à travers l'<b><span style="background-color:rgb(255, 202, 202)">activité introductive</span></b> (détaillée <a target="_blank" href="dossier.md#activit1activitintroductivepourquoiatonbesoindesbasesdedonnes">ici</a>)</li>
                <li>Historique "bases de données"</li>
                <li>Cours magistral : </li>
                <ul>
                    <li>Définition des principaux mots clé : shéma, table, champ, cle primaire, ...</li>
                    <li>Rappels (première NSI) / introduction de la notion des domaines</li>
                    <li>à compléter</li>
                </ul>
                <li>Activité : prise en main d'un logiciel de gestion de base de données: <a href="https://sqlitebrowser.org/dl" target="_blank">SQLite Browser</a>
                <ul>
                    <li>Création d'une base de données</li>
                    <li>Création de champs</li>
                    <li>Ajout de données</li>
                    <li>à compléter</li>
                </ul>
            </li></ul>
            <p>=> Pour la séance suivante, faire remarquer dans le programme le terme "schéma relationnel": se renseigner sur ce qu'est un shéma relationel et quelle en est l'utilité. Rédiger un shéma relationnel de la base créer lors de cette séance</p>
        </td>
    </tr>
    <tr>
        <th class="text-nowrap">Séance 2</th>
        <td>
            <ul>
                <li>Restitution des recherches & proposition de schéma relationnel </li>
                <li>Cours magistral sur les bases de données et schéma relationnels</li>
                <li>Cours magistral et/ou activité dédié aux anomalies de bases de données, à éclaircir</li>
                <li>Activité : </li>
                <ul>
                    <li>Traduire un énoncé en un schéma relationnel</li>
                    <li>Créer une base de données depuis un schéma relationnel</li>
                    <li>Vérifier que la base de données ne présente pas d'anomalies, si il y en a, revoir le schéma relationnel</li>
                </ul>
                <li>Adapter le reste de la séance en fonction des avancées et/ou pédagogie différentiée nécessaire à mettre en oeuvre </li>
            </ul>
        </td>
    </tr>
    <tr>
        <th class="text-nowrap">Séance 3</th>
        <td>
            <p>Séance dédiée aux requêtes SQL de recherche, avec les clauses <code>SELECT</code>, <code>FROM</code> et <code>WHERE</code></p>
            <b>à détailler</b>
        </td>
    </tr>
    <tr>
        <th rowspan="3" style="width: 0" class="text-nowrap">Semaine 2</th>
        <th class="text-nowrap">Séance 4</th>
        <td>
            <p>Séance dédiée à l'utilisation de lignes de commandes pour créer bases de données (<code>CREATE DATABASE</code>) et tables (<code>CREATE TABLE</code>), et insérer/supprimer des données (<code>INSERT</code> / <code>DELETE</code>).</p><p> Le but de cette seance est de permettre aux élèves de comprendre le contenu d'un fichier d'export d'un gestionnaire de SGBD. Ces commandes ne font pas parties des attendues du programme, il est seulement utile d'en connaitre l'existance.</p>
            <b>à détailler</b>
        </td>
    </tr>
    <tr>
        <th class="text-nowrap">Séance 5</th>
        <td>
            <p>Séance dédiée à l'utilisation de clé étrangère et de jointure (<code>JOIN</code>) dans les requetes de recherche SQL</p>
            <p>Il s'agit ici d'une des parties les plus difficiles de cette séquence. Cette séance s'appuira sur l'<b><span style="background-color:rgb(202, 255, 202)">activité 'des tables et des jointures'</span></b> (détaillée <a target="_blank" href="dossier.md#activit2activitdestablesetdesjointurespourquoietcommentutiliserdesdonnesentrediffrentestables">ici</a>)</p>
            <p>Les élèves devront également apprendre comment rédiger un schéma relationnel contenant des clés étrangères</p><p>
            <b>à détailler</b>
        </p></td>
    </tr>
    <tr>
        <th class="text-nowrap">Séance 6</th>
        <td>
            <p>Séance de battement pour approfondir les points des précédentes séances.</p>
            <p>Il faudra également dédier une partie de la séance sur les SGBD (cours théorique / magistral ?)</p>
            <b>à détailler</b>
        </td>
    </tr>
    <tr>
        <th rowspan="3" style="width: 0" class="text-nowrap">Semaine 3</th>
        <th class="text-nowrap">Séance 7</th>
        <td>
            <p>Première séance dédiée au <span style="background-color:rgb(202, 202, 255)">projet 'air quality control'</span> : </p>
            <ul>
                <li>Projet décrit dans le dossier à remettre au jury, contenu du dossier accessible <a target="_blank" href="dossier.md#activit3projetairqualitycontrolcommentvaloriserlesdonnesdunebasededonnes">ici</a></li>
                <li>Séance détaillée <a target="_blank" href="">ici</a></li>
            </ul>
            <p>Les élèves seront regroupés en binômes. Il s'agit d'une séance sous la forme d'un TP où les groupes devront : </p>
            <ul>
                <li>Se connecter sur un raspberry et analyser des données receuillies par des modules connectés au raspberry</li>
                <li>Concevoir et créer une base de données adapté</li>
                <li>Insérer manuellement des valeurs dans la base de données</li>
            </ul>
            <p>En prévision de la séance 9, les groupes devront réfléchir à un moyen de valoriser, pour des utilisateurs non techniques, les données qu'ils ont stockées dans leur base de données.</p>
        </td>
    </tr>
    <tr>
        <th class="text-nowrap">Séance 8</th>
        <td>
            <p>Deuxième séance dédiée au <span style="background-color:rgb(202, 202, 255)">projet 'air quality control'</span>.</p>
            <p>Il s'agit d'une séance sous la forme d'un TP où les mêmes groupes de la séance précédente devront programmer un script python permettant de peupler automatiquement leur base de données. Ceci en utilisant la librairie python sqlite3.</p>
        </td>
    </tr>
    <tr>
        <th class="text-nowrap">Séance 9</th>
        <td>
            <p>Troisième séance dédiée au <span style="background-color:rgb(202, 202, 255)">projet 'air quality control'</span>.</p>
            <p>Le travail ici devient plus conséquent et s'étale sur les séances 10 et 11. Les binômes sont donc regroupés en groupe de 4 en fonction :</p>
            <ul>
                <li>des recherches efectuées après la séance 7</li>
                <li>des niveaux des élèves afin d'équilibrer au mieu les groupes</li>
            </ul>
            <p>Les nouveaux groupes devront choisir quelle base de données et programme python conserver et devront développer une solution permettant à des utilisateurs non techniques d'utiliser les données de leur base de données (site web, application mobile, interface graphique via python, ...)</p>
        </td>
    </tr>
    <tr>
        <th rowspan="3" style="width: 0" class="text-nowrap">Semaine 4</th>
        <th class="text-nowrap">Séance 10</th>
        <td>
            <p>Quatrième séance dédiée au <span style="background-color:rgb(202, 202, 255)">projet 'air quality control'</span> et dans la continuité de la scéance 9.</p>
        </td>
    </tr>
    <tr>
        <th class="text-nowrap">Séance 11</th>
        <td>
            <p>Cinquième séance dédiée au <span style="background-color:rgb(202, 202, 255)">projet 'air quality control'</span> et dans la continuité de la scéance 10.</p>
        </td>
    </tr>
    <tr>
        <th class="text-nowrap">Séance 12</th>
        <td>
            <p>Presentation des projets</p>
            <p>Évaluation sommative</p>
        </td>
    </tr>
  </tbody>
</table>

<table class="d-none table col-10 mx-auto">
  <tbody>
    <tr>
        <th scope="row">Semaine 1</th>
        <td>
            <p><b><a href="/enseignement/a-_trier/dossier-_oral-_2/seances/seance-_1.md" target="_blank">Séance 1</a>, objectif(s) : </b></p>
            <ul>
                <li>Prendre connaissance du projet</li>
                <li>Depuis un logiciel de gestion pour SGBD (<a href="https://sqlitebrowser.org/dl" target="_blank">SQLite Browser</a>)</li>
                <ul>
                    <li>Créer une base de données SQL</li>
                    <li>Créer une table</li>
                    <li>Créer des champs</li>
                    <li>Ajouter/supprimer des données dans une base de données</li>
                </ul>
            </ul>
        </td>
        <td>
            <p><b><a href="/enseignement/a-_trier/dossier-_oral-_2/seances/seance-_2.md" target="_blank">Séance 2</a>, objectif(s) : </b></p>
            <ul>
                <li>Rédiger un schéma relationnel sans clé étrangère</li>
                <li>Créer une base de données SQL à l'aide d'un schéma relationnel depuis un logiciel de gestion pour SGBD</li>
            </ul>
        </td>
        <td>
            <p><b>Séance 3, <a href="seances/seance-_03.md">projet phase 1</a>, objectif(s) :</b></p>
            <ul>
                <li>Analyser des données récoltées par un raspberry et ses modules</li>
                <li>Concevoir et créer une base de données adaptée</li>
                <li>Insérer manuellement quelques données dans la base de données</li>
            </ul>
        </td>
    </tr>
    <tr>
        <th scope="row">Semaine 2</th>
        <td>
            <p><b><a href="/enseignement/a-_trier/dossier-_oral-_2/seances/seance-_4.md" target="_blank">Séance 4</a>, objectif(s) : </b></p>
            <ul>
                <li>Sélectionner des données via des requêtes SQL avec les clauses <code>SELECT</code>, <code>FROM</code> et <code>WHERE</code></li>
            </ul>
        </td>
        <td>
            <p><b><a href="/enseignement/a-_trier/dossier-_oral-_2/seances/seance-_5.md" target="_blank">Séance 5</a>, objectif(s) : </b></p>
            <ul>
                <li>Créer une base de données SQL avec les clauses <code>CREATE DATABASE</code></li>
                <li>Créer des tables avec les clauses <code>CREATE TABLE</code></li>
                <li>Ajouter/supprimer des données via des requêtes SQL avec les clause <code>INSERT</code> et <code>DELETE</code></li>
            </ul>
            <p><b>note :</b> le but de cette seance est avant tout de permettre aux élèves de comprendre le contenu d'un fichier d'export d'un gestionnaire de SGBD. Ces commandes ne font pas parties des attendues du programme, il est seulement utile d'en connaitre l'existance.</p>
        </td>
        <td>
            <p><b>Séance 6, <a href="#phase2">projet phase 2</a>, objectif(s) :</b></p>
            <ul>
                <li>Se connecter sur un terminal distant via le protocol SSH</li>
                <li>Utiliser des commandes Linux usuelles</li>
                <li>Utiliser un éditeur de texte sans interface</li>
                <li>Créer un script python</li>
                <li>Utiliser la librairie 'sqlite3' de python</li>
            </ul>
        </td>
    </tr>
    <tr>
        <th scope="row">Semaine 3</th>
        <td>
            <p>
                <b><a href="/enseignement/a-_trier/dossier-_oral-_2/seances/seance-_7.md" target="_blank">Séance 7</a>, objectif(s) : </b>
                </p><ul>
                    <li>Rédiger un schéma relationnel contenant une/des clé(s) étrangère(s)</li>
                    <li>Sélectionner des données via des requêtes SQL à travers plusieurs tables à l'aide de la clause <code>JOIN</code></li>
                </ul>
            <p></p>
        </td>
        <td>
            <p><b><a href="/enseignement/a-_trier/dossier-_oral-_2/seances/seance-_8.md" target="_blank">Séance 8</a>, objectif(s) : </b></p>
            <ul>
                <li>Rédiger un schéma relationnel contenant une/des clé(s) étrangère(s)</li>
                <li>Sélectionner des données via des requêtes SQL à travers plusieurs tables à l'aide de la clause <code>JOIN</code></li>
                <li>Identifier les principales caractéristiques d'un SGBD</li>
            </ul>
        </td>
        <td>
            <p><b>Séance 9, <a href="#phase3">projet phase 3</a>, objectif(s) :</b></p>
            <ul>
                <li>Se connecter sur un terminal distant via le protocol SSH</li>
                <li>Utiliser des commandes Linux usuelles</li>
                <li>Utiliser un éditeur de texte sans interface</li>
                <li>Créer un script python</li>
                <li>Utiliser la librairie 'sqlite3' de python</li>
                <li>Rédiger un schéma relationnel contenant une/des clé(s) étrangère(s)</li>
                <li>Sélectionner des données via des requêtes SQL à travers plusieurs tables à l'aide de la clause <code>JOIN</code></li>
                <li>Déployer un serveur Web Python</li>
                <li>Créer des pages HTML</li>
            </ul>
        </td>
    </tr>
    <tr>
        <th scope="row">Semaine 4</th>
        <td>
            <p><b><a href="/enseignement/a-_trier/dossier-_oral-_2/seances/seance-_10.md" target="_blank">Séance 10</a>, objectif(s) : </b></p>
            <ul>
                <li>Évaluation sommative sous forme de QCM</li>
                <li>Continuer la <a href="#phase3">phase 3 du projet</a></li>
            </ul>
        </td>
        <td>
            <p><b>Séance 11, <a href="#phase3">projet phase 3</a></b></p>
        </td>
        <td>
            <p><b>Séance 12, <a href="#prsentationdesprojets">présentation des projets</a></b></p>
        </td>
    </tr>
  </tbody>
</table>

## Le projet

### Présentation

L'activité environnente et humaine influent sur la composition de l'air. Cette composition peut impacter de manière néfaste la santé des organismes vivants.

Le but du projet "Air Quality Controller" est de développer un site web permettant de consulter diverses données et statistiques liées à l'état de l'atmosphère et à la composition de l'air. 

Un cahier des charges a été créé et est accessible <a href="../supports/cahier-_des-_charges.md" target="_blank">ici</a>.

### Liens avec le programme scolaire

Le projet, principalement dans ses deux premières phases, permet de mettre directement en application le contenu de la séquence "Base de données" du programme de Terminale. 

Ci-dessous le contenu du programme officiel du thème.

**Légende :**
<ul>
    <li><span class="text-blue">élément en bleu</span> = élément abordé dans le projet</li>
    <li><span class="text-green">élément en vert</span> = élément abordé dans le projet <b>en fonction de son avancé</b></li>
    <li><span class="text-red">élément en rouge</span> = élément <b>non</b> abordé dans le projet</li>
    <li><span class="text-violet">élément en violet</span> = élément <b>partiellement</b> abordé dans le projet</li>
</ul>

<table class="table col-10 mx-auto">
  <thead>
    <tr>
      <th class="text-center" scope="col" colspan="2">Thème "Bases de données" (Terminale)</th>
    </tr>
    <tr>
      <th scope="col">Contenu</th>
      <th scope="col">Capacités attendues</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td><p>Modèle relationnel : <span class="text-blue">relation, attribut, domaine, clef primaire, <span class="text-green">clef étrangère</span>, schéma relationnel.</span></p></td>
        <td><p><span class="text-blue">Identifier les concepts définissant le modèle relationnel.</span></p></td>
    </tr>
    <tr>
        <td><p>Base de données relationnelle.</p></td>
        <td>
            <p><span class="text-blue">Savoir distinguer la structure d’une base de données de son contenu.</span></p>
            <p><span class="text-blue">Repérer des anomalies dans le schéma d’une base de données.</span></p>
        </td>
    </tr>
    <tr>
        <td><p>Système de gestion de bases de données relationnelles.</p></td>
        <td><p><span class="text-blue">Identifier les services rendus par un système de gestion de bases de données relationnelles : persistance des données, <span class="text-red">gestion des accès concurrents</span>, efficacité de traitement des requêtes, <span class="text-red">sécurisation des accès</span>.</span></p></td>
    </tr>
    <tr>
        <td><p>Langage SQL : <span class="text-blue">requêtes d’interrogation</span> et <span class="text-violet">de mise à jour</span> d’une base de données.</p></td>
        <td>
            <p>Identifier les composants d’une requête.</p>
            <p><span class="text-blue">Construire des requêtes d’interrogation à l’aide des clauses du langage SQL : SELECT, FROM, WHERE, <span class="text-green">JOIN</span>.</span></p>
            <p>Construire <span class="text-violet">des requêtes d’insertion et de mise à jour</span> à l’aide de : <span class="text-red">UPDATE</span>, <span class="text-blue">INSERT, DELETE</span>.</p>
        </td>
    </tr>
  </tbody>
</table>

Le projet fait également écho à des parties des programmes de seconde (SNT) et de première (NSI). Ces liens sont détaillés ci-dessous dans la partie "Pré-requis pour les élèves".
