# Annexes

<div class="d-print-none">
<h2>Accès rapide</h2>
+++sommaire
</div>

<style>
.shadow{
    box-shadow:0 .5rem 1rem rgba(9, 8, 8, 0.47) !important;
}

</style>

## 1. CV

<div class="shadow text-center">
<img style="width:75%" src="https://i.ibb.co/RhnjhBT/LOPEZ-MATTHIEU-CV-1.jpg">
</div>

## 2. Séquence : nombre de séances

<br>
<div class="p-2 py-3 shadow">

L'estimation du nombre de séances est basée sur l'année 2021-2022. Ci-dessous la liste des éléments pris en compte et des calculs : 
<table class="table bg-white">
    <tbody><tr>
        <th>Jour de rentrée scolaire des élèves</th>
        <td><u>Jeudi 2 septembre</u></td>
    </tr>
    <tr>
        <th>Zone académique</th>
        <td><u>C</u> (académie de Montpellier)</td>
    </tr>
    <tr>
        <th>Début des épreuves écrites en terminale</th>
        <td><u>Mars 2022</u></td>
    </tr>
    <tr>
        <th>Nombre de semaines de cours plaines (hors rentrée + semaines avec jour(s) fériés) avant Mars</th>
        <td><u>19 semaines de 6 heures chacune</u></td>
    </tr>
    <tr>
        <th>Nombre de semaines de cours "semaine de rentrée" + "semaines comprenant un/des jour(s) férié(s)" avant Mars</th>
        <td><u>2 semaines de 6 heures chacune</u></td>
    </tr>
    <tr>
        <th>Nombre de theme en Terminale</th>
        <td><u>5 thèmes</u> (sans compter le thème "Histoire de l'informatique" qui est transverse)</td>
    </tr>
    <tr>
        <th>Nombre d'heures par theme sur les 19 semaines pleines</th>
        <td>( 19 semaines * 6 heures ) / 5 thèmes = <u>22.8 heures par thème soit environ 11.4 séances de 2 heures par thème</u></td>
    </tr>
</tbody></table>

<b>note :</b> le temps manquant pour arriver aux 12 séances de deux heures pour le thème "Bases de données" se trouve dans les deux semaines non pleines et/ou éventuellement en consacrant moins de 22.8 heures sur un autre thème du programme.

</div>

## 3. Séquence : organisation des séances

<br>
<div class="p-2 py-3 shadow">
<table class="table col-10 mx-auto bg-white">
    <tbody>
        <tr><th class="text-center">Séance 1</th></tr>
        <tr>
            <td>
                <b>Contenu(s) abordé(s) du programme : </b>
                    <ul>
                        <li>Histoire de l'informatique : "Bases de données"</li>
                        <li>Modèle relationnel : relation, attribut, domaine, clef primaire</li>
                    </ul>
                <b>Capacité(s) abordée(s) du programme : </b>
                    <ul>
                        <li>Savoir distinguer la structure d'une base de données de son contenu</li>
                        <li>Identifier les concepts définissant le modèle relationnel</li>
                    </ul>
                <b>Activité(s) (liste non exhaustive) :</b>
                    <ul>
                        <li>Activité introductive</li>
                        <li>Prise en main d'un logiciel de gestion de base de données: <a href="https://sqlitebrowser.org/dl" target="_blank">SQLite Browser</a> : création de bdd, de tables, de champs, insertion manuelle de données, ...</li>
                    </ul>
            </td>
        </tr>
    </tbody>
</table>

<br>
<table class="table col-10 mx-auto bg-white">
    <tbody>
        <tr><th class="text-center">Séance 2</th></tr>
        <tr>
            <td>
                <b>Contenu(s) abordé(s) du programme : </b>
                    <ul>
                        <li>Schéma relationnel</li>
                    </ul>
                <b>Capacité(s) abordée(s) du programme : </b>
                    <ul>
                        <li>Savoir distinguer la structure d'une base de données de son contenu</li>
                        <li>Repérer des anomalies dans le schéma d'une base de données</li>
                    </ul>
                <b>Activité(s) (liste non exhaustive) :</b>
                <ul>
                    <li>Traduire un énoncé en un schéma relationnel</li>
                    <li>Créer une base de données depuis un schéma relationnel</li>
                    <li>Vérifier que la base de données ne présente pas d'anomalies</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<br>
<table class="table col-10 mx-auto bg-white">
    <tbody>
        <tr><th class="text-center">Séance 3</th></tr>
        <tr>
            <td>
                <b>Contenu(s) abordé(s) du programme : </b>
                    <ul>
                        <li>Langage SQL : requêtes d'interrogation d'une base de données</li>
                    </ul>
                <b>Capacité(s) abordée(s) du programme : </b>
                    <ul>
                        <li>Identifier les composants d'une requête</li>
                        <li>Construire des requêtes d'interrogation à l'aide des clauses du langage SQL : SELECT, FROM, WHERE</li>
                    </ul>
                <b>Activité(s) (liste non exhaustive) :</b>
                <ul>
                    <li><b>à compléter</b></li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<br>
<table class="table col-10 mx-auto bg-white">
    <tbody>
        <tr><th class="text-center">Séance 4</th></tr>
        <tr>
            <td>
                <p>Une évaluation formative aura lieu en début de séance, cela 
                dans le but d'identifier les élèves en difficulté. Ce qui permettra, 
                entre autres, une composition de binomes plus optimale.</p>
                <b>Contenu(s) abordé(s) du programme : </b>
                    <ul>
                        <li>Langage SQL : requêtes de mise à jour d'une base de données</li>
                    </ul>
                <b>Capacité(s) abordée(s) du programme : </b>
                    <ul>
                        <li>Identifier les composants d'une requête</li>
                        <li>Construire des requêtes d'insertion et de mise à jour à l'aide de : UPDATE, INSERT, DELETE</li>
                        <li>Construire des requêtes d'interrogation à l'aide des clauses du langage SQL : SELECT, FROM, WHERE</li>
                    </ul>
                <p><b>note : </b> la clause COUNT est également vue car présente dans le sujet zero</p>
                <b>Activité(s) (liste non exhaustive) :</b>
                <ul>
                    <li>Analyser un export de base de données produit par un gestionnaire de SGBD (<a href="https://sqlitebrowser.org/dl" target="_blank">SQLiteBrowser</a>)</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<br>
<table class="table col-10 mx-auto bg-white">
    <tbody>
        <tr><th class="text-center">Séance 5</th></tr>
        <tr>
            <td>
                <b>Contenu(s) abordé(s) du programme : </b>
                    <ul>
                        <li>Modèle relationnel : clef étrangère</li>
                        <li>Langage SQL : requêtes d'interrogation d'une base de données</li>
                    </ul>
                <b>Capacité(s) abordée(s) du programme : </b>
                    <ul>
                        <li>Identifier les composants d'une requête</li>
                        <li>Construire des requêtes d'interrogation à l'aide des clauses du langage SQL : JOIN</li>
                    </ul>
                <b>Activité(s) (liste non exhaustive) :</b>
                <ul>
                    <li>Activité "des tables et des jointures"</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<br>
<table class="table col-10 mx-auto bg-white">
    <tbody>
        <tr><th class="text-center">Séance 6</th></tr>
        <tr>
            <td>
                <b>Contenu(s) abordé(s) du programme : </b>
                    <ul>
                        <li>Système de gestion de bases de données relationnelles</li>
                    </ul>
                <b>Capacité(s) abordée(s) du programme : </b>
                    <ul>
                        <li>Identifier les services rendus par un système de gestion de bases de données relationnelles : persistance des données, gestion des accès concurrents, efficacité de traitement des requêtes, sécurisation des accès</li>
                    </ul>
                <b>Activité(s) (liste non exhaustive) :</b>
                <ul>
                    <li>Découverte du SGBD libresource MariaDB et de la librairie python "mariadb"</li>
                    <li><b>à compléter</b></li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>
</div>

<span class="pagebreak"></span>

## 4. Fiche support : activité introductive

<br>
<div class="p-3 mb-2 border border-danger"> ⚠️ Dans cette annexe, les <span class="text-blue">informations écrites en bleu</span> sont des notes personnelles destinées à l'enseignant. Elles ne figurent pas sur le document imprimé et fournis aux élèves.</div>


<div class="p-2 py-3 shadow">
+++bulle matthieu
    Aujourd'hui nous démarrons un tout nouveau chapitre dédié à un domaine majeur de l'inormatique, les <b>Bases de données</b> ! Avant de s'attaquer au vif du sujet, nous l'introduisons à travers une courte activité
+++

<h3>Étape 1 : exploiter une information</h3>

<p class="text-center">Mardi 9 février 2021, le directeur général du site leboncoin a posté le tweet suivant</p>

<p><img src="https://i.ibb.co/vJG49DV/Screenshot-from-2021-04-18-06-38-57.png" alt="https://twitter.com/ajouteau/status/1359080640122093572?ref_src=twsrc%5Etfw"></p>

<p>Supposons que le site soit consulté chaque jours en moyenne par 10 millions d'utilisateurs uniques ( ~ 20,4 millions / 2 ). Supposons également qu'une visite sur 50 aboutit à un achat sur le site.</p>

<p><strong>1) Combien d'utilisateurs totaux auront visité le site en 30 jours ?</strong></p>

<div class="border border-dark mb-3 p-1 pb-3 bg-white">
    <span class="text-blue">Nous supposons que 10 millions d'utilsateurs consultent le site quotidiennement, donc 300 000 000 ( 30 * 10 000 000 ) visiteurs auront visité le site leboncoin en 30 jours
    </span>
</div>


<p><strong>2) Combien y aura-t'il eu d'achats après 30 jours ?</strong></p>

<div class="border border-dark mb-3 p-1 pb-3 bg-white">
    <span class="text-blue">Nous supposons qu'une visite sur 50 aboutit à un achat sur le site, donc 6 000 000 ( 300 000 000 / 50 ) achats auront été effectués en l'espace 30 jours
    </span>
</div>

<p>Supposons maintenant que le site leboncoin stocke les informations de tous les achats de tous les utilisateurs dans un fichier '<b>lbc_achats.csv</b>'.</p>

<p><strong>3) Rappeler ce qu'est un fichier .csv et donner un exemple de contenu d'un tel fichier</strong></p>

<div class="border border-dark mb-3 p-1 pb-3 bg-white">
    <span class="text-blue">
        Un fichier .csv est un fichier contenant du texte, et ce texte suit une structure standardisée facilitant la conservation et le traitement ultérieur de données. CSV est l'acronyme de <b>C</b>omma-<b>S</b>eparated <b>V</b>alues, mais il est tout à fait possible d'utiliser un autre séparateur tel que le point virgule, un exemple : <br><br>
        jeu;difficulté;score maximal<br>
        pacman;medium;190000<br>
        arkanoid;hard;280000
    </span>
</div>

<span class="pagebreak"></span>

<h3>Étape 2 : rapeller et utiliser les connaissances du programme de première NSI (traitement des données en table)</h3>

<p>En général, lorsqu'un utilisateur X effectue un achat sur un site de vente en ligne, ce dernier consulte la page "mes transactions" pour vérifier que son dernier achat a bien été pris en compte. Le serveur gérant le site leboncoin doit alors ouvrir son fichier '<b>lbc_achats.csv</b>' pour en extraire les informations de tous les achats de l'utilisateur X. Nous allons simuler cette opération serveur pour l'utilisateur Moreau Romain.</p>

+++bulle matthieu 75
    La suite se fait sur ordinateur ! demandez à votre enseignent de vous transmettre 'la clé usb'
+++

+++bulle matthieu droite 75
    Copiez/collez le dossier 'introduction_bdd' sur votre ordinateur et passez votre clé à un autre groupe qui n'a pas encore copié le dossier. 
+++

<p>Une fois le dossier copié sur votre ordinateur, vous remarquez qu'il contient un ensemble de fichiers : </p>
<ul>
    <li>'<b>lbc_achats.csv</b>' : contient les informations des achats effectués en 30 jours (6 000 000 en tout)</li>
    <li>'<b>mini_lbc_achats.csv</b>' : contient les 10 premières entrées du fichier 'lbc_achats.csv'</li>
    <li>'<b>liste_des_achats.py</b>' : un script python ayant pour but de lister les informations des achats de l'utilisateur X</li>
</ul>

+++bulle matthieu 75
    Attention ! pour l'instant n'utilisez pas le fichier '<b>lbc_achats.csv</b>' qui est assez volumineux. 
+++

<p><strong>4) Quelle instruction dans le script lit le contenu du fichier .csv et de le stocke dans une variable ? préciser le type de structure python de la variable</strong></p>

<div class="border border-dark mb-3 p-1 pb-3 bg-white">
    <span class="text-blue">L'instruction est '<code>list(csv.reader(open(fichier_csv), delimiter=";"))</code>' et stocke dans la variable '<code>totalite_des_achats</code>' le contenu du fichier. Nous pouvons déduire de la fonction '<code>list</code>' que la variable qui stocke le contenu du fichier est une liste</span>
</div>

<p><strong>5) Complétez le script 'liste_des_achats.py' afin qu'il affiche le resultat de sa recherche directement dans le terminal. Puis exécutez le avec en paramètres le fichier .csv '<code>mini_lbc_achats.csv</code>' et <code>Moreau</code> et <code>Romain</code></strong></p>

<p>Une fois fonctionnel, votre script '<b>liste_des_achats.py</b>' avec les paramètres ci-dessus devrait vous afficher un résultat ressemblant à ceci : </p>

```md
Recherche des achats de Moreau Romain...
resultat(s) :
jour 01;Moreau;Romain;93922733604317328639;19
jour 01;Moreau;Romain;00076732160050035072;25

--- temps d'execution du script : X.X seconde(s) ---
```

<p>Au début de cette activité, nous avons estimé qu'au bout de 30 jours, 6 millions d'achats sont effectués. Le fichier 'lbc_achats.csv' contient 6 millions d'achat factices et maintenant que notre script de recherche est prêt, nous allons pouvoir tester une recherche sur un fichier volumineux.</p>

<p><strong>6) Utilisez le script 'liste_des_achats.py' avec en paramètres le fichier .csv '<code>lbc_achats.csv</code>' et <code>Moreau</code> et <code>Romain</code>.</strong></p>

+++bulle matthieu 75
    Attention ! le traitement peut être long étant donnée le nombre d'entrées dans le fichier 'lbc_achats.csv'.
+++
+++bulle matthieu droite 75
    Avant exécution du script, je vous conseil vivement de fermer toutes les applications gourmandes sur l'ordinateur (comme le navigateur web par exemple)
+++

<p><strong>7) Notez le temps d'exécution obtenu (en secondes) :</strong><span>_______________ <span class="text-blue">(9,44 secondes)</span></span></p>

<p><span class="text-blue">Bien entendu, le temps varie d'un ordinateur à l'autre, d'une exécution à l'autre... mais le but ici est avant tout de relever un "ordre de grandeur".</span></p>

<p>Nous avons maintenant une estimation du temps que mettrait le serveur pour rassembler les informations liées aux achats d'un utilisateur donné.</p>

<p><strong>8) Qu'en conclure sur l'expérience qu'aura notre utilisateur depuis son navigateur lorsqu'il accèdera à la page listant tous ses achats ?</strong></p>

<div class="border border-dark mb-3 p-1 pb-3 bg-white">
    <span class="text-blue">Notre utilisateur devra attendre au moins 9,44 secondes avant que son navigateur affiche les données de la page "mes achats". Cette longue attente impactera de manière très négative l'expérience utilisateur.</span>
</div>


+++bulle matthieu 75
    Pour les groupes arrivées ici, je vous invite à venir en aide aux autres !
+++

<span class="text-blue">Les questions suivantes ne seront pas rédigées sur le document imprimé à l'élève. Les groupes les plus avancés pourront réfléchir à comment aller au delà des calculs actuels et aider les groupes en retard. Ce temps permets aux groupes nécessaitant d'avantage de temps  d'avancer à leur rythme.</span>

<br><br>
<h3>Étape 3 : pulvériser les limites de notre approche</h3>

<b>9) <span class="text-blue">Étant donné que le site leboncoin existe depuis une quinzaine d'année. Combien de secondes faudrait il pour rechercher les achats d'un utilisateur dans un fichier .csv cumulant les achats depuis 10 ans ? Pour simplifier les calculs, nous supposerons que tous les mois sont composés de 30 jours.</span></b>

<div class="border border-dark mb-3 p-1 pb-3 bg-white">
    <span class="text-blue">Pour 30 jours de données, nous avons déterminé qu'il faudrait environ 9,44 secondes pour rechercher les informations des achats d'un utilisateur. Pour un .csv construit depuis 10 années, il faudra environ 1132,8 secondes ( 10 années * 12 mois * 9,44 sec = 1132,8 sec ) pour trouver les achats d'un utilisateur</span>
</div>

<b>10) <span class="text-blue">Le site a reçu 20.4 millions de visites le 09/02/2021. Nous avons supposé qu'un visiteur sur 50 effectue un achat et, donc, se rendra sur la page listant ses achats. Calculer en secondes puis en années (de 360 jours) le temps qu'un serveur monocore devra prendre pour chercher toutes les informations des achats de tous les utilisateurs qui les demandent.</span></b>

<div class="border border-dark mb-3 p-1 pb-3 bg-white">
    <span class="text-blue">Pour une demande, nous avons vu qu'il faut environ 1132,8 sec au serveur pour la traiter. En cette journée du 09/02/2021, et supposant qu'un utilisateur sur 50 consulte la page "mes achats", le serveur aura besoin d'environ 4,6218 * 10^8 secondes ( 1132,8 sec * 20 400 000 utilisateurs / 50 ) soit presque 15 années de 360 jours ( [4,6218 * 10^8] / [ 360 jours * 24 heures * 3600 secondes ] ) pour traiter les requêtes de la journée des utilisateurs.</span>
</div>

<span class="pagebreak"></span>
<b>11) <span class="text-blue">Quelle(s) conclusion(s) en tirer ?</span></b>

<div class="border border-dark mb-3 p-1 pb-3 bg-white">
    <span class="text-blue">La recherche d'information dans un fichier texte est inefficace lorsque ce dernier devient volumineux. Nous avons mis en lumière ce problème à travers un contexte web, mais ce problème s'étends bien entendu bien au delà :
        <ul>
            <li>Comment stocker, mettre à jours et récupérer efficacement toutes les informations liées aux livres mis à disposition par l'ensemble des bibliothèques de France ?</li>
            <li>Comment stocker, mettre à jours et récupérer efficacement toutes les informations qu'échangent en temps réel des milliers voir des millions de joueurs sur un jeu en ligne ?</li>
            <li>Comment stocker, mettre à jours et récupérer efficacement toutes les informations liées au génome humain ? et aux autres animaux ?</li>
            <li>etc ...</li>
        </ul>
    
    </span>
</div>

</div>

## 5. Script python à compléter

<div class="p-2 shadow">
```python
import csv  # permet de manipuler les fichiers .csv
import sys  # permet de récupérer les paramètres soumis lors de l'exécution du script
import time # permet de chronométrer le temps d'exécution de la recherche

#################
#-- FONCTIONS --#
#################

def check_parametres():
    
    # On test si le nombre d'éléments contenu dans le tableau sys.argv
    # est égal à 4 alors qu'on attends 3 paramètres
    # Cela est dû au fait que sys.argv contient aussi le nom du script en 
    # plus des paramètres
    if(len(sys.argv) != 4):
        print("Erreur ! le script doit être exécuté avec 3 paramètres :")
        print("le chemin et nom du fichier csv, nom de l'utilisateur, prenom de l'utilisateur")
        print("Exemple : python3.8 liste_des_achats.py mini_lbc_achats.csv Moreau Romain")
        print("Dans les options de l'IDE, vous pouvez renseigner des paramètres d'entrée")
        sys.exit() # on stoppe l'exécution du script

def achats_utilisateur_X(fichier_csv, nom, prenom):

    resultat = []
    totalite_des_achats = list(csv.reader(open(fichier_csv), delimiter=";"))

    print('Recherche des achats de ' + nom + ' ' + prenom)

    for entree in totalite_des_achats:
        if(entree[1] == nom and entree[2] == prenom):
            resultat.append(entree)

    return resultat

```
</div>
<span class="pagebreak"></span>
<div class="p-2 shadow">
```python
#############################
#-- DEROULEMENT DU SCRIPT --#
#############################

print("Le script est exécuté avec le(s) paramètre(s) suivant(s) : ", sys.argv)

# Le script doit être exécuté avec 3 paramètres, on test si ces paramètres sont 
# bien présents
check_parametres()

# On stocke les paramètres soumis avec l'exécution du script dans des variables
fichier_csv         = sys.argv[1]
nom_utilisateur     = sys.argv[2]
prenom_utilisateur  = sys.argv[3]

# Nous permet de calculer le temps d'exécution de la recherche
start_time = time.time()

# On recherche les ventes de notre utilisateur
print('Resultat(s) : ')
"""le code doit être complété entre ici..."""






"""...et ici !"""

# dernière ligne affichant le temps d'exécution de la recherche
print("\n# temps d'execution de la recherche : " 
      + str(round(time.time() - start_time, 2)) 
      + " seconde(s)")
```
</div>

## 6. Fiche support : qualité de l'air

<br>
<div class="p-2 py-3 shadow">
<p>La qualité de l'air est depuis de nombreuses années un enjeu majeur de santé publique. Cette qualité est grandement impactée par l'activité humaine. Le but de cette fiche est de comprendre ce qui définie la qualité de l'air en France, et par conséquent, de comprendre les données qui seront manipulées à travers le projet "air quality control".</p>

<p>Pour chaque questions ci-dessus, vous devez remplir à la maison le premier bloc "à remplir à la maison" et laisser le bloc "à compléter en classe" vide. Je vous conseille de lire toutes les questions avant de démarrer vos recherches.</p>

<p><b>Des élèves seront aléatoirement tirés au sort pour exposer oralement le résultat de leurs recherches.</b> Il est donc important de comprendre ce que vous décidez de noter à la maison afin de le partager le plus naturellement possible en classe.</p>

<span class="pagebreak"></span>
<h3>Les questions</h3>

<p><b>1)</b> Rechercher et donner une définition générale de ce qu'est la qualité de l'air en France.</p>

<p class="mb-1"><b>À remplir à la maison</b></p>
<div class="border border-dark mb-3 bg-white" style="height:200px"></div>

<p class="mb-1"><b>À compléter en classe</b></p>
<div class="border border-dark mb-3 bg-white" style="height:200px"></div>

<p><b>2)</b> Rechercher et expliquer à quoi correspondent les données PM2.5 et PM10. Précisez le lien entre ces données et les impacts sur la santé.</p>

<p class="mb-1"><b>À remplir à la maison</b></p>
<div class="border border-dark mb-3 bg-white" style="height:200px"></div>

<p class="mb-1"><b>À compléter en classe</b></p>
<div class="border border-dark mb-3 bg-white" style="height:200px"></div>

<span class="pagebreak"></span>
<p><b>3)</b> Recherchez une lois Française publiée en lien avec la qualité de l'air. Noter sa date de publication et expliquer en 2/3 lignes le but de la lois en question.</p>

<p class="mb-1"><b>À remplir à la maison</b></p>
<div class="border border-dark mb-3 bg-white" style="height:200px"></div>


<p>Restitution orale</p>

<p class="mb-1"><b>À remplir en classe</b></p>
<div class="border border-dark mb-3 bg-white" style="height:200px"></div>

</div>

## 7. Ressources utilisées pour le dossier et la présentation orale

<br>
<div class="p-2 py-3 shadow">

<h3>Bibliographie</h3>

<h3>Sitographie</h3>

<h4>Youtube</h4>
<ul>
    <li>vidéo courte en synthétique sur le sujet de la différenciation pédagogique : <a href="https://www.youtube.com/watch?v=MEkEMFGlvzY">https://www.youtube.com/watch?v=MEkEMFGlvzY</a></li>
</ul>

<h4>Site Internet "neoprofs"</h4>
<ul>
    <li>Fil de discussion sur le sujet de la progression spiralée : <a href="https://www.neoprofs.org/t92253-progression-spiralee">https://www.neoprofs.org/t92253-progression-spiralee</a></li>
    <li>Fil de discussion sur le sujet de la différenciation pédagogique : <a href="https://www.neoprofs.org/t71683-hg-faites-vous-de-la-differenciation-pedagogique-en-cours">https://www.neoprofs.org/t71683-hg-faites-vous-de-la-differenciation-pedagogique-en-cours</a></li>
    <li><a href=""></a></li>
    <li><a href=""></a></li>
</ul>


</div>