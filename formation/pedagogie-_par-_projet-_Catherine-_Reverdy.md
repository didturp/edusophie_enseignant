# La pedagogie par projet, dossier de Catherine Reverdy

+++ sommaire

## Introduction

Ce document est un résumé d'un dossier de veille rédigé par **[Catherine Reverdy](https://www.linkedin.com/in/reverdy/)** en **2013**. Celle-ci officiait pendant cette période en tant que chargée d'étude et de recherche dans l'**IFE** (**I**nstitut **F**rançais de l'**É**ducation).

Le dossier intitulé "**Des projets pour mieux apprendre ?**" est consultable dans divers endroit de la toile dont le site [veille-et-analyses.ens-lyon.fr](http://veille-et-analyses.ens-lyon.fr/DA-Veille/82-fevrier-2013.pdf). 

La pédagogie par projet m'intéresse particulièrement dans sa mise en place en cours de **NSI** (**N**umérique et **S**ciences **I**nformatiques), matière enseignée au lycée général. 

J'ai initialement découvert en 2017 cette aproche pédagogie lorsque je portais la casquette de formateur dans un organisme de formation privé pour le titre professionnel de **[Développeur Web et Web Mobile](https://www.banque.di.afpa.fr/espaceemployeurscandidatsacteurs/EGPResultat.aspx?ct=01280m03&type=t)**.

Les écrits de Catherine Reverdy me permettent de prendre de la hauteur sur cette pédagogie découverte directement sur le terrain. 

Je pioche donc dans le dossier les points faisant écho avec : 
- mon expérience en tant que formateur dans des formations professionalisantes
- l'application de cette pédagogie dans les cours de **NSI** au lycée.
- l'application de cette pédagogie dans un environement informatique technique

## Le résumé

### Introduction

+++citation
Pourquoi est elle si délicate à mettre en place ? Apporte-t-elle une plus-value pour les élèves dans leur développement et leurs apprentissages ?
+++

### Le projet sous (presque) toutes ses coutures

+++citation
L'objectif peut être de préparer concrètement les élèves à leur futur métier, de les motiver par une réalisation matérielle, ou d'inciter les enseignants de discipline différentes à travailler en équipes, ou encore de développer une approche par compétence
+++

La motivation par une réalisation matérielle, l'incitation au travail pluridisciplinaire et le développement de l'approche par compétence sont des pistes qui font particulièrement sens dans l'enseignement **NSI**.

#### Une définition très large du projet

##### Tentative de classification des projets en éducation

+++citation
<a href="https://fr.wikipedia.org/wiki/Jean-Pierre_Boutinet" target="_blank">Boutinet</a> (<a href="http://wikindx.ens-lyon.fr/biblio_vst/index.php?action=resource_RESOURCEVIEW_CORE&id=6942" target="_blank">2005</a>) distingue quatre niveaux de projet qui peuvent utiliser chacun la pédagogie par projet, mais qui ne doivent pas être confondus :
<ul>
    <li>le <b>projet éducatif</b>, qui se réfère au mode d’intégration des jeunes dans le monde des adultes et qui a pour rôle de les rendre autonomes. Ce type de projet dépasse le cadre strict de l’école ;</li>
    <li>le <b>projet pédagogique</b>, qui intervient dans la relation entre les enseignants et les élèves, dans le cadre scolaire : c’est celui auquel nous nous intéresserons ici ;</li>
    <li>le <b>projet d’établissement</b>, qui vise à accompagner l’autonomie grandissante des établissements scolaires par une mise en cohérence de ses activités ;</li>
    <li>le <b>projet de formation</b>, qui se situe davantage au niveau de la formation des adultes, vue depuis le stagiaire, l’organisme de formation ou le formateur ;</li>
</ul>
+++

Cette distinction entre projet **pédagogique** et **de formation** dessine clairement une frontière entre les projets que j'ai menés en tant que formateur et ceux déployés dans le cadre scolaire du lycée. 

Ce qui est assez logique, les enseignements au lycée général n'ont pas vocation à envoyer directement sur le marché du travail les élèves une fois le bac en poche. 

+++citation
[...] dans la présentation officielle du <b>P</b>rojet <b>P</b>luridisciplinaire à <b>C</b>aractère <b>P</b>rofessionnel (<b>PPCP</b>) mis en place au lycée professionnel à la rentrée 2000 : "[...] Dans différents passages du texte, <b>l’aspect existentiel</b> [d'un projet] poursuit trois objectifs : <b>la  'motivation'</b>, <b>'l’implication  de l’élève'</b> ou <b>son 'projet professionnel'</b>. <b>L’aspect méthodologique</b> s’intéresse à <b>la “démarche”</b> et <b>au “travail d’équipe”</b> et <b>l’aspect opératoire</b> se limite à <b>la 'réalisation'</b> et <b>au 'résultat concret'</b>
+++

#### Le projet en tant qu'approche pédagogique

##### Une méthode active

+++citation
Cette philosophie de l’éducation [la méthode active] [...] a fait passer la vision de l’école d’une approche centrée sur l’enseignant à une approche centrée sur l’élève et proche de leur vie quotidienne
+++

+++citation
L’approche par projet s’inscrit dans cette démarche puisqu’elle "favoris[e] une approche interdisciplinaire centrée sur   l’intérêt des apprenants et parce qu’elle privilégi[e] aussi, comme contexte d’apprentissage, des situations concrètes de la vie courante." (Proulx, 2004)
+++

+++citation
"[L’approche par projet] prend parti pour l’enseignement de l’apprentissage dans l’action, pour l’apprenant comme chef de file de sa formation et pour l’enseignant comme sa vigie. [...] l’approche par projet est idéologique avant d’être une mode ou une formule pédagogique à proprement parler." (Proulx, 2004)
+++

#####  Définition de l'apprentissage par projet

+++citation
Deux points leur [Blumenfeld et al. <a href="http://wikindx.ens-lyon.fr/biblio_vst/index.php?action=resource_RESOURCEVIEW_CORE&id=8499" target="_blank">1991</a>] paraissent essentiels dans cet apprentissage [par projet] : un problème ou une question doit servir de fil directeur aux activités réalisées dans le projet, et ces activités doivent aboutir à un produit final qui apporte la solution au problème.
+++

+++citation
Marx et al. (<a href="http://wikindx.ens-lyon.fr/biblio_vst/index.php?action=resource_RESOURCEVIEW_CORE&id=8612" target="_blank">1997</a>) définissent cinq caractéristiques de l’enseignement des sciences par le projet (project-based science) [...] : "a driving question, investigations and artifacts, collaboration, and technological tools", apportant en plus la collaboration et les TIC [...] pas forcément soulignées dans les autres définitions
+++

+++citation
Pour Perrenoud (<a href="http://wikindx.ens-lyon.fr/biblio_vst/index.php?action=resource_RESOURCEVIEW_CORE&id=8542" target="_blank">2002</a>), une <br>
" <br>
démarche de projet :
<ul>
<li> est une entreprise collective gérée par le groupe-classe [...] ; </li>
<li> s’oriente vers une production concrète [au sens large] ; </li>
<li> induit un ensemble de tâches dans lesquelles tous les élèves peuvent s’impliquer et jouer un rôle actif, qui peut varier en fonction de leurs moyens et intérêts ; </li>
<li> suscite l’apprentissage de savoirs et de savoir-faire de gestion de projet (décider, planifier, coordonner, etc.) ; </li>
<li> favorise en même temps des apprentissages identifiables (au moins après-coup) figurant au programme d’une ou plu-sieurs disciplines . </li>
</ul>
"
+++

##### Ce que l'apprentissage par projet n'est pas

+++citation
L’apprentissage par projet ne doit pas être confondu avec les autres types d’apprentissage fondés sur l’investigation, appartenant à la famille de l’inquiry-based learning,  dont  la  démarche  d’investigation mise en place [...] en France depuis quelques années [...] et qui a pour ambition de placer les élèves en position de chercheur. C’est une méthode développée surtout en sciences, puisqu’elle s’inspire de la démarche scientifique, déjà utilisée lors des séances de travaux pratiques
+++

+++citation
Le plus célèbre type d’apprentissage par investigation, très en vogue  actuellement à tous les niveaux du système scolaire, est l’apprentissage par la résolution de problèmes [...] [qui] est  certainement, par rapport à  l’apprentissage par projet, plus facile à mettre en place (puisque moins coûteux en temps), souvent plus centré sur la discipline étudiée et s’intéressant davantage à l’acquisition qu’à l’application des connaissances.
+++

+++citation
Contrairement à l’apprentissage par projet, qui ne permet pas aux élèves de définir et d’envisager toutes les solutions des problèmes qui se présentent au fur et à mesure de l’avancée du projet, l’apprentissage par la résolution de problèmes développe ces compétences utiles dans le monde professionnel (Savery, <a href="http://wikindx.ens-lyon.fr/biblio_vst/index.php?action=resource_RESOURCEVIEW_CORE&id=8538" target="_blank">2006</a>)
+++

+++citation
L’apprentissage par étude de cas (ou case-based learning, considéré comme une forme particulière d’apprentissage par la résolution de problèmes pour certains auteurs) consiste à étudier différents exemples de problèmes complexes réels (construits ou reconstruits par l’enseignant) pour développer des compétences comme l’analyse critique, faire le lien entre théorie et pratique, ou encore acquérir des connaissances et des compétences en contexte pour les appliquer à une autre situation d’investigation (Savery, <a href="http://wikindx.ens-lyon.fr/biblio_vst/index.php?action=resource_RESOURCEVIEW_CORE&id=8538" target="_blank">2006</a> ; Mayer & Alexander, <a href="http://wikindx.ens-lyon.fr/biblio_vst/index.php?action=resource_RESOURCEVIEW_CORE&id=8536" target="_blank">2011</a>).
+++

+++citation
Un dernier type d’apprentissage voisin est l’apprentissage par la conception (design-based learning), qui consiste à développer l’apprentissage en concevant et réalisant un produit final (un robot en cours d’électronique, un bâtiment en cours d’architecture, un site Internet lors d’un concours, etc.) L’apprentissage se fait par essais et erreurs, suite à l’évaluation de l’objet en construction à plusieurs moments de sa réalisation. Les compétences développées sont de nature technique et de l’ordre de la coopération, du partage d’expertises, de la planification et de la gestion des contraintes. [...] Contrairement à l’apprentissage par projet, les objectifs d’apprentissage sont ici centrés sur la fabrication d’un objet et non sur la résolution d’une problématique en vue de la réalisation d’un produit final.
+++

#### Apprendre et coopérer à travers les projets

+++citation
Dans l’apprentissage par projet, les élèves et les étudiants apprennent en étant actifs et en gardant un lien avec le monde réel, ce qui leur permet "de nourrir la communication, la coopération, la créativité et la réflexion en profondeur. L’attention aux processus d’apprentissage, et pas seulement au contenu, est bénéfique" (Barron & <a href="https://en.wikipedia.org/wiki/Linda_Darling-Hammond" target="_blank">Darling-Hammond</a>, <a href="http://wikindx.ens-lyon.fr/biblio_vst/index.php?action=resource_RESOURCEVIEW_CORE&id=8613" target="_blank">2010</a>).
+++

##### Des projets pour construire ses apprentissages

 