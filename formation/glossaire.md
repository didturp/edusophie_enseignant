# glossaire

+++ sommaire

glossaire ici : [lien](https://cpa.enset-media.ac.ma/lexique_termes_pedagogiques.htm)

amenagement classe : [lien](https://profpower.lelivrescolaire.fr/repenser-lamenagement-de-la-classe/)

## capacité
- ensemble d'aptitude que l'individu met en oeuvre dans différences situations 
- une capacité n'est ni observable ni évaluable, il s'agit d'un axe de formation selon lequel l’élève doit progresser
- exemple; communiquer, s'imformer, analyser, gérer, traiter, décider, préparer, mettre en oeuvre, maintenir en état, diagnostiquer, ...
## compétence
- ensemble de savoirs et de savoir-faire organisés en vue d'accomplir de façon adaptée une activité
- un savoir est évaluable à travers 
## evaluation
### diagnostique ou pré-évaluation
- moyen d'identitification des acquis et d'analyse des besoins
- support d'aide à la construction des stratégies pédagogiques
### formative
- mise en oeuvre en situation au cours d'activités aux objectifs identifiés et en appui sur des critères de reussite appropriés
- permet d'obtenir du feedback afin d'identifier les lacunes de l'enseignement. Ce feedback permet ainsi de savoir sur quoi se focaliser pour continuer la formation
### sommative
- en fin de processus de formation et en cohérence avec l'évaluation formative 
- permet la mesure des acquis
### certificative
### récapitulatif

![tableau evaluation](http://www4.ac-nancy-metz.fr/svt/evaluation/divers/docs/177_1.jpg)
[source](https://www4.ac-nancy-metz.fr/svt/evaluation/divers/index.php?idp=177)

## savoir associé / connaissance
- connaissances à mobiliser pour mettre en oeuvre une compétence
- ex : pour "analyser un dessin technique" (compétence), il est nécessaire de connaître les "conventions européennes de projection" (savoir)
## savoir-être
- comportement, attitude attendu pour la réalisation de l'activie
## savoir-faire
- ensemble de gestes et de méthodes appliqués à des situations, des activités précises
- habilité manifestée dans une situation précise et faisant appel à une activité physique. C'est un ensemble de gests et de méthodes les mieux adaptés à la tâche proposée
- ex : réaliser un assemblage par soudage, éditer un courrier, ...