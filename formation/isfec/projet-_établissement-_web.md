# Projet NSI : "Évolution du statut de la Femme"

+++sommaire 2

## Introduction

Le projet NSI se décompose en 3 phases principales : 

- [**la phase 1**](#phase1) durant laquelle les élèves de NSI doivent développer un formulaire Web de sondage accessible sur ordinateur et sur téléphone portable. Ce formulaire sera utilisé par les élèves de ST2S 
- [**la phase 2**](#phase2) durant laquelle les élèves de NSI doivent partager et aider les élèves en ST2S à l'extraction des données collectés par le formulaire développe lors de la phase 1
- [**la phase 3**](#phase3) durant laquelle les élèves de NSI doivent deployer un CMS (plateforme Web de gestion de contenu) qui permettra aux élèves hors NSI d'ajouter des articles dedans

Des séances le mercredi après midi nommée "mercredi tous unis" sont prévues tout au long de l'année pour permettre la rencontre entre les élèves de differentes disciplines.

## Capacités et compétences du programme officiel couvertes 

Ce projet s'inscrit dans la séquence "**Bases de données**" qui est un thème important abordé en spécialité NSI en terminale. C'est une excellente occasion pour mettre en pratique ce thème difficile. Le projet décrit ici est donc adapté pour des élèves de terminale NSI. 

### Capacités pré-requises de première NSI 

+++lien "https://cache.media.eduscol.education.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf" "lien vers le BO"

<div class="col-10 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr>
        <th colspan="2" class="text-center">Thème : 'Interactions entre l’homme et la machine sur le Web' </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <ul>
            <li>Analyser et modifier les méthodes exécutées lors d’un clic sur un bouton d’une page Web</li>
            <li>Distinguer ce qui est exécuté sur le client ou sur le serveur et dans quel ordre</li>
            <li>Distinguer ce qui est mémorisé dans le client et retransmis au serveur</li>
            <li>Reconnaître quand et pourquoi la transmission est chiffrée</li>
            <li>Analyser le fonctionnement d’un formulaire simple</li>
            <li>Distinguer les transmissions de paramètres par les requêtes POST ou GET</li>
          </ul>
        </td>
      </tr>
    </tbody>
  </table>
  
  <table class="table table-sm mt-2">
    <thead>
      <tr>
        <th colspan="2" class="text-center">Thème : 'Langages et programmation' </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <ul>
            <li>Mettre en évidence un corpus de constructions élémentaires</li>
            <li>Prototyper une fonction</li>
          </ul>
        </td>
      </tr>
    </tbody>
  </table>
</div>

### Capacités de terminale NSI 

+++lien "https://cache.media.eduscol.education.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf" "lien vers le BO"

<div class="col-10 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr>
        <th colspan="2" class="text-center">Thème : 'Bases de données' </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <ul>
            <li>Identifier les concepts définissant le modèle relationnel</li>
            <li>Savoir distinguer la structure d’une base de données de son contenu</li>
            <li>Repérer des anomalies dans le schéma d’une base de données</li>
            <li>Identifier les services rendus par un système de gestion de bases de données relationnelles</li>
            <li>Construire des requêtes d’interrogation à l’aide des clauses du langage SQL</li>
            <li>Construire des requêtes d’insertion et de mise à jour</li>
          </ul>
        </td>
      </tr>
    </tbody>
  </table>
</div>
  
### Compétences de terminale NSI

+++lien "https://cache.media.eduscol.education.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf" "lien vers le BO"

<div class="col-10 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr>
        <th colspan="2" class="text-center">Compétences de discipline</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <ul>
            <li>Modéliser un problème en termes de flux et de traitement d’informations</li>
            <li>Décomposer un problème en sous-problèmes, reconnaître des situations déjà analysées et réutiliser des solutions</li>
            <li>Mobiliser les concepts et les technologies utiles pour assurer les fonctions d’acquisition, de mémorisation, de traitement et de diffusion des informations</li>
            <li>Développer des capacités d’abstraction et de généralisation</li>
          </ul>
        </td>
      </tr>
    </tbody>
  </table>

  <table class="table table-sm mt-2">
    <thead>
      <tr>
        <th colspan="2" class="text-center">Compétences transversales</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <ul>
            <li>Faire preuve d’autonomie, d’initiative et de créativité </li>
            <li>Présenter un problème ou sa solution, développer une argumentation dans le cadre d’un débat</li>
            <li>Coopérer au sein d’une équipe dans le cadre d’un projet</li>
            <li>Rechercher de l’information, partager des ressources</li>
            <li>Faire un usage responsable et critique de l’informatique</li>
          </ul>
        </td>
      </tr>
    </tbody>
  </table>
</div>

## Planning général

Le projet évolue à travers deux types de séances : 
- les séances <b class="s sNSI">NSI</b> qui durent chacune 2 heures. En terminale, les élèves qui ont choisi l'option ont 6 heures de NSI par semaine ( = 3 séances )
- les séances <b class="s sI">inter</b>disciplinaires où enseignants et élèves de differentes disciplines passent du temps ensemble le mercredi

<div class="col-12 mx-auto">
<table class="table">
  <thead>
  <tr>
      <td scope="col" class="align-middle text-center"><b>séance</b></td>
      <th class="w-100 align-middle text-center" scope="col">contenu</th>
  </tr>
  </thead>
  <tbody>
    <tr><td class="modEven" colspan="2"><div class="float-start"><b>[ Phase 1 ] Développement d'un formulaire Web pour un sondage</b></div><div class="d-flex justify-content-end">
      <div><a href="#phase1"><i class="fas fa-arrow-circle-down text-blue"></i> détails</a></div>
    </div></td></tr>
    <tr><td class="sNSI text-center align-middle text-nowrap">NSI<br>#1<br>#2</td>
      <td>Composition d'équipes de 3 à 4 personnes
      <ul>
        <li>Création d'un formulaire Web simple</li>
        <li>Création d'une base de données simple</li>
      </ul>Choix collégial avec les élèves NSI du meilleur projet à utiliser pour la suite</td>
    </tr>
    <tr><td class="sI align-middle text-nowrap">inter #1</td>
      <td><b>Séance commune avec les élèves en ST2S</b><ul>
        <li>Démonstration de la première version du formulaire</li>
        <li>Recueil des éléments que doit contenir le formulaire de sondage avec les élèves ST2S</li>
      </ul></td>
    </tr>
    <tr><td class="sNSI text-center align-middle text-nowrap">NSI<br>#3<br>#4<br>#5</td>
      <td class="align-middle">
      <ul>
        <li>Ajout de champs dans le formulaire</li>
        <li>Modification de la base de données, création d'exemples de requêtes d'extraction de données</li>
        <li>Adaptation du formulaire pour un affichage mobile ou desktop, ajout de décoration/animations diverses</li>
      </ul></td>
    </tr>
    <tr><td class="sI align-middle text-nowrap">inter #2</td>
      <td><b>Séance commune avec les élèves en ST2S</b><ul>
        <li>Démonstration des versions finales des projets, choix collégial du projet retenu</li>
      </ul></td>
    </tr>
    <tr><td class="modOdd" colspan="2"><div class="float-start"><b>[ Phase 2 ] Échanges et analyses des données collectées par le formulaire</b></div><div class="d-flex justify-content-end">
      <div><a href="#phase2"><i class="fas fa-arrow-circle-down text-blue"></i> détails</a></div>
    </div></td></tr>
    <tr><td class="sI align-middle text-nowrap">inter #3</td>
      <td><b>Séance commune avec les élèves en ST2S</b><ul>
        <li>Partage de la totalité des données collectées sous un format .csv (fichier tableaur)</li>
        <li>Échanges sur des extraction plus fines de données</li>
      </ul></td>
    </tr>
    <tr><td class="sNSI align-middle text-nowrap">NSI #6</td>
      <td>Composition de binômes
      <ul>
        <li>Création des requêtes SQL permettant d'extraire finement certaines parties des données</li>
        <li>Réunion des résultats et envoie de ces derniers aux élèves ST2S</li>
      </ul></td>
    </tr>
    <tr><td class="modEven" colspan="2"><div class="float-start"><b>[ Phase 3 ] Développement d'une plateforme de gestion de contenu (CMS)</b></div><div class="d-flex justify-content-end">
      <div><a href="#phase3"><i class="fas fa-arrow-circle-down text-blue"></i> détail</a></div>
    </div></td></tr>
    <tr><td class="sI align-middle text-nowrap">inter #4</td>
      <td><ul>
        <li>Échanges sur les données du sondage recueillies et analysées</li>
        <li>Échanges sur les contenus des pages Web qui devront être disponibles pour l'ensemble du lycée</li>
      </ul></td>
    </tr>
      <tr><td class="sNSI text-center align-middle text-nowrap">NSI<br>#7<br>#8<br>#9</td>
      <td class="align-middle">Composition d'équipes de 3 à 4 personnes 
      <ul>
        <li>Formation sur un CMS choisi avec la classe</li>
        <li>Déploiement du CMS en local, configurations diverses et ajout de modules en fonctions des besoins exprimés par les élèves hors NSI</li>
        <li>Création de rôles : administrateur CMS, rédacteur</li>
        <li>Création d'exemples d'articles</li>
        <li>Déploiement du CMS en ligne</li>
      </ul></td>
    </tr>
  </tbody>
</table>
</div>

## Les phases en détail


### Phase 1 

Les élèves de NSI vont devoir développer une page Web dans laquelle n'importe quel élève du lycée pourra remplir un sondage en rapport avec l'évolution des droits des femmes. 

<u class="h5">Séances <span class="s sNSI">NSI #1</span> et <span class="s sNSI">NSI #2</span></u>

Deux séances de 2 heures sont nécessaires, un bref rappel de certaines notions importantes du Web vues en première NSI sera fait.

À l'issue de ces séances, un formulaire Web factice sera développé. Ce formulaire doit contenir des exemples de champs usuels, voici un exemple : 

<div class="border border-dark p-3 col-6 mx-auto rounded my-3 d-print-avoid-break">
  
  <p class="h5 text-center mb-3">Sondage 'Évolution du statut de la Femme'</p>
  
  <input class="w-100 mb-2" type="date">
  
  <input class="w-100 mb-2" type="text" placeholder="saisir une phrase ici">
  
  <hr>
  
  <div>
    <label for="volume" class="text-center d-block">Exemple de jauge</label><br>
    <input class="w-100" type="range" id="volume" name="volume" min="0" max="11">
  </div>
  
  <hr>
  
  <div>
    <input type="checkbox" name="checkbox1" id="checkbox1" checked="">
    <label for="checkbox1">exemple de checkbox 1</label>
  </div>

  <div>
    <input type="checkbox" name="checkbox2">
    <label for="checkbox2">exemple de checkbox 2</label>
  </div>
  
  <hr>
  
  <div>
    <input type="radio" id="huey" name="drone" value="huey" checked="">
    <label for="huey">exemple de radio button 1</label>
  </div>

  <div>
    <input type="radio" id="dewey" name="drone" value="dewey">
    <label for="dewey">exemple de radio button 2</label>
  </div>
  
  <hr>
  
  <label for="story">Exemple de champs texte multiligne :</label>

  <textarea class="w-100" placeholder="saisir un texte ici"></textarea>

  <hr>
  
  <input type="submit" name="" id="" value="valider">
  
</div>

Une base de donnée, simpliste, sera aussi créée afin de pouvoir stocker les données du formulaire factice. **Une grille d´évaluation** est fournise aux élèves et sera complétée lors de leur présentation en groupe. Pour terminer, nous garderons pour la suite le projet le code qui a reçu la meilleure note.

<u class="h5">Séance <span class="s sI">inter #1</span></u>

Lors de cette séance, l'équipe qui a développé le meilleur formulaire le présentera devant les autres élèves ST2S. Un extrait de toutes les données sera également montré sur un logiciel type tableau (exemple libreOffice Calc) 

Tous les élèves échangerons alors sur le contenu attendu de la version finale du formulaire pour le sondage. Une maquette sera dessinée, les élèves de NSI auront les informations en main pour pouvoir poursuivre le travail et élaborer la version finale du formulaire attendue.

<u class="h5">Séances <span class="s sNSI">NSI #3</span>, <span class="s sNSI">NSI #4</span> et <span class="s sNSI">NSI #5</span></u>

Trois séances sont prévues pour développer le formulaire demandé. Les élèves travailleront à partir du code choisi à l'issue de la séance <span class="s sNSI">NSI #2</span>.

Le travail sera présenté et évaluer lors de la séance suivante avec les élèves ST2S. 

<u class="h5">Séances <span class="s sI">inter #2</span></u>

Dans la dernière scéance de la phase les formulaires des différentes équipes d'élèves de NSI seront présentés ainsi qu'un exemple d'extraction de toutes les données collectées par le formulaire sous format .csv . En amont, l'enseignant en NSI aura rendu la page Web accessible au réseau utilisé dans le lycée.

L'ensemble des élèves voteront pour le meilleur travail qui contiendra alors le formulaire qui sera diffusés aux autres élèves du lycée. **Une grille d'évaluation** sera remplie par l'enseignant et permettra de noter chaque groupe. Le vote des participants aura une influence (mineure) sur la note obtenue.

### Phase 2

Le formulaire a été diffusé dans le lycée et a été remplis par un certain nombre d'élèves.

<u class="h5">Séance <span class="s sI">inter #3</span></u>

Le but de cette séance est de présenter l'ensemble des données qui ont été collectées aux élèves ST2S+ et de définir ensemble des requêtes d'extraction de données plus fines, comme par exemple : 

- quelles sont les différences entre les formulaires remplis par les élèves de seconde et les formulaires remplis par des élèves de terminale ?
- quelles sont les différences des tranches de salaires entre Homme et Femme en pourcentage

<u class="h5">Séance <span class="s sNSI">NSI #6</span></u>

Lors de cette séance, les élèves mis en binômes, élaborent des requêtes personnalisées d'extraction de données afin de répondre aux questions soulevées pendant la séance <span class="s sI">inter #3</span>

Les résultats des requêtes sont envoyées par mail aux élèves ST2S.

### Phase 3

Les données maintenant recuillies et analysées, cette phase consiste à la création et mise à disposition d'une plateforme Web de gestion de contenu. Elle servira aux élèves hors NSI pour créer des articles en rapport avec leur discipline et données auparavant collectées par le formulaire.

<u class="h5">Séance <span class="s sI">inter #4</span></u>

Lors de cette séance commune, les élèves échangeront sur les données auparavant analysées. Les élèves conviendront ensemble de la forme et du contenu des articles Web synthétisant les travaux précédents. Les élèves de NSI noteront les différentes demandes et l'enseignant NSI s'assurera de la faisabilité des demandes durant les trois prochaines séances. 

<u class="h5">Séances <span class="s sNSI">NSI #7</span>, <span class="s sNSI">NSI #8</span> et <span class="s sNSI">NSI #9</span></u>

Dans ces trois dernières séances, les élèves NSI seront formés pour configurer un site de gestion de contenu (type Wordpress, Drupal, Django, ...). Les élèves apprendront comment créér des rôles (rôle adrministrateur, rôle chef rédacteur, rôle rédacteur ...), comment télécharger et installer des modules pour personnaliser le site et comment ajouter des articles.

Les outils de création de contenu sont des outils simple d'utilisation mais complexes à comprendre. Les trois séances sont l'occasion de plonger dans les entrailles d'un outils de gestion de contenu et de prendre le temps d'en comprendre la logique.

Les élèves seront rassemblés en équipe de 3 à 4 personnes et **les projets seront évalués** à l'issue des 3 séances à travers un oral de présentation .

Avec l'assistance de l'enseignant NSI, le meilleur projet issue de ses trois séances sera déployé en ligne, et donc, sera accessibles aux élèves hors NSI pour pouvoir y rajouter des articles. 

Une fois le site déployé, l'information sera communiquée à l'ensemble des élèves hors NSI qui pourrons ajouter des articles sur la plateforme en ligne.

<style>
  .s{
    padding:2px 4px;
  }
  .sNSI{
    background-color:orange !important;
    font-weight: bold;
  }
  .sI{
    background-color:violet !important;
    font-weight: bold;
  }
  
  .t1{color:#67b021}
  .t2{color:#069668}
  .t3{color:#b25ea8}
  .t4{color:#081394}
  .t5{color:#932226}
  .t6{color:#444444}
  .t7{color:#e114dc}
  
  .modEven{background-color:#baffab !important;vertical-align: middle}
  .modOdd{background-color:#abfbff !important;vertical-align: middle}
  .vac{background-color:#ffabfb !important;vertical-align: middle}
  
  table td ul{
    margin-bottom:0px;
  }
  
  thead > tr {
    position: sticky;
    top: 0;
    background-color: #edecec;
    box-shadow: 0 4px 2px -1px rgba(0, 0, 0, 0.4);
  }
  table {
    border-collapse: separate; /* Don't collapse */
    border-spacing: 0;
}
</style>