# Tutorial thonny

+++sommaire

<br>
Url raccoucie de cette page : [shorturl.at/bwOPR](shorturl.at/bwOPR)

## Aperçu général

<br>
<img src="https://user-images.githubusercontent.com/1057839/104211453-61c0f400-5434-11eb-8f52-c61c616578da.png">

- version actuelle : 3.3.13 
- site officiel : [https://thonny.org/](https://thonny.org/)
- licence : [licence MIT](https://fr.wikipedia.org/wiki/Licence_MIT)

## Avantages

<br>
<table class="table table-sm">
  <tbody>
    <tr class="text-center">
      <th>Avantages pour les élèves</th>
    </tr><tr>
    </tr><tr>
      <td><ul>
        <li>Interface simple (pas de boutons superflu)</li>
        <li>Les contrastes sont très marqués, les mots clés ressortent bien</li>
        <li>Possibilité d'exécuter depuis une clé USB</li>
        <li>Gestionnaire de paquets python intégré</li>
      </ul></td>
    </tr>
    <tr class="text-center">
      <th>Avantages pour les profs</th>
    </tr><tr>
        </tr><tr>
      <td><ul>
        <li>Le poids (pour installer...)</li>
        <li>Peu gourmand en ressources (+1 pour les pc à configs légères)</li>
        <li>Crossplateforme (linux + mac + windows)</li>
        <li>Transportable (peut être installé sur clé USB)</li>
        <li>Opensource ( = outils soutenu par une communauté)</li>
        <li>Embarque python (v3.7)</li>
        <li>Plugins disponibles</li>
      </ul></td>
    </tr>
  </tbody><tbody>
</tbody></table>

## Guide d'utilisation
<br>

![](https://lh4.googleusercontent.com/SGZIjw5JbGwEpMvwl_QkSLRt20rvij3sty8sF5svwCTkv5TdZke7Dc1fG8d6gmDDL6p7mIyaO75Ac3U1zEJh=w1366-h667)

![](https://lh3.googleusercontent.com/Qnhbj5Dx5QxpBPz9nLXFGgDp1i9bGZIyry5xmKzStB9wBOVqdC4DtHx7lD_I_qJ7ykH4rGwahwvewv6PpAyf=w1366-h667)

![](https://lh4.googleusercontent.com/AxBJtKAUzPTw2b7zyEQyCHR488hRAF3fJAzZzh4wYH032w9efJf59PA8s9V19PXckF7TcI8p1m-OJK11e3Ep=w1366-h667)

![](https://lh5.googleusercontent.com/5q6s9qrzsgMxtRPwZtg3pierT0UDznWW_US3-xlW8_GDCeS5ltCAHBpLxEiSANY903Fa-n3p5SzRQoqM2YQA=w1366-h667)

![](https://lh6.googleusercontent.com/6HRDk6m7t4OjHIcL2i0yyQxJTIoFo67Ldjy2NbLSFH_gpuVBpy16zxiHidI-tWGjEffBi5Ok7F7_aA0EU59L=w1366-h667)

## Auteurs

Formation NSI 2021 : 

- Noémie
- Phillipe
- Alice
- Matthieu

