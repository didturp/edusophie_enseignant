# Types de données construites en Python

Python mets à disposition des types de variables representant des données dîtes construites ; les listes, les tuples, les dictionnaires et les ensembles. 

Nous présentons chaques types un à un avec leurs caractéristiques propres, leurs avantages, inconvénients et des cas de figures courant dans lesquels il est pratique de les utiliser. Ensuite, nous comparerons en détail les opérations disponibles avec ces types.

+++sommaire

## Dans les grandes lignes

Ces 4 types de structures peuvent contenir des éléments de toute sorte : des entiers, des chaines de caractères, des booléen, et également, des listes, des tuples, des dictionnaires, des ensembles, ...

### Les listes

#### Description

En python, une variable de type `list` contient une liste d'éléments ordonnés par position et altérables après initiatlisation. Le comportement et les opérations disponibles qu'offre ce type de structure sont similaires aux structures de tableaux (🇬🇧 : array) disponibles dans la plupart des langages de programmation.

#### Caractéristiques

- Stocke un nombre ordonné de données
- Peut être modifié après son initialisation
- Structure similaire aux tuples mais plus lourde, donc un temps d'exécution du code plus long pour les listes par rapport aux tuples
- Partage les mêmes opération que les chaines de caractères (à vérifier)

#### Cas d'utilisation conseillés/déconseillés

<table>
    <thead>
        <tr>
            <th colspan="2" class="text-center text-green">Quand est-ce qu'il vaut mieux les utiliser ?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><p>VS tuple</p></td>
            <td>si on souhaite pouvoir modifier les éléments au cours de l'exécution du programme</td>
        </tr>
        <tr>
            <td><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td>si on souhaite accéder aux éléments à travers leur position d'insertion</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <th colspan="2" class="text-center text-red">Quand est-ce qu'il ne vaut mieux <u>PAS</u> les utiliser ?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><p>VS tuple</p></td>
            <td>si on sait que les éléments initiaux n'auront pas besoin d'être modifiés pendant l'exécution du programme</td>
        </tr>
        <tr>
            <td><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td>si on souhaite accéder aux éléments à travers des clés définies et non à travers des positions</td>
        </tr>
        <tr>
            <td><p>VS ensemble</p></td>
            <td>si on souhaite éviter qu'un élément apparaisse plus d'une fois dans la structure</td>
        </tr>
    </tbody>
</table>

### Les tuples

#### Description

En python, la différence entre une variable de type `tuple` et une variable de type `type` est que les tuples ne sont plus modifiables après initialisation. 

#### Caractéristiques

- Stocke un nombre ordonné de données
- **Ne peut pas** être modifié après son initialisation contrairement aux listes
- Structure similaire aux listes mais plus légère, donc un temps d'exécution du code plus court pour les tuples par rapport aux listes

#### Cas d'utilisation conseillés/déconseillés

<table>
    <thead>
        <tr>
            <th colspan="2" class="text-center text-green">Quand est-ce qu'il vaut mieux les utiliser ?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><p>VS tuple</p><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td>si on sait que les éléments initiaux n'auront pas besoin d'être modifiés pendant l'exécution du programme</td>
        </tr>
        <tr>
            <td><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td>si on souhaite accéder aux éléments à travers leur position d'insertion</td>
        </tr>
        <tr>
            <td><p>VS ensemble</p></td>
            <td>si on souhaite qu'un élément puisse apparaitre plus d'une fois dans la structure</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <th colspan="2" class="text-center text-red">Quand est-ce qu'il ne vaut mieux <u>PAS</u> les utiliser ?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><p>VS tuple</p><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td>si on souhaite pouvoir modifier les éléments au cours de l'exécution du programme</td>
        </tr>
        <tr>
            <td><p>VS dictionnaire</p></td>
            <td>si on souhaite accéder aux éléments à travers des clés définies et non à travers des positions</td>
        </tr>
        <tr>
            <td><p>VS ensemble</p></td>
            <td>si la position des éléments n'importe pas et si on souhaite éviter qu'un élément apparaisse plus d'une fois dans la structure</td>
        </tr>
    </tbody>
</table>

### Les dictionnaires

#### Description

##### TODO : à compléter

#### Caractéristiques

##### TODO : à compléter

#### Cas d'utilisation conseillés/déconseillés

##### TODO : à compléter

<table>
    <thead>
        <tr>
            <th colspan="2" class="text-center text-green">Quand est-ce qu'il vaut mieux les utiliser ?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><p>VS tuple</p><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td></td>
        </tr>
        <tr>
            <td><p>VS tuple</p><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td></td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <th colspan="2" class="text-center text-red">Quand est-ce qu'il ne vaut mieux <u>PAS</u> les utiliser ?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><p>VS tuple</p><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td></td>
        </tr>
        <tr>
            <td><p>VS tuple</p><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td></td>
        </tr>
    </tbody>
</table>

### Les ensembles

#### Description

##### TODO : à compléter

#### Caractéristiques

##### TODO : à compléter

#### Cas d'utilisation conseillés/déconseillés

##### TODO : à compléter

<table>
    <thead>
        <tr>
            <th colspan="2" class="text-center text-green">Quand est-ce qu'il vaut mieux les utiliser ?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><p>VS tuple</p><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td></td>
        </tr>
        <tr>
            <td><p>VS tuple</p><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td></td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <th colspan="2" class="text-center text-red">Quand est-ce qu'il ne vaut mieux <u>PAS</u> les utiliser ?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><p>VS tuple</p><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td></td>
        </tr>
        <tr>
            <td><p>VS tuple</p><p>VS dictionnaire</p><p>VS ensemble</p></td>
            <td></td>
        </tr>
    </tbody>
</table>

## Opérations et comparatifs entres les différents types de structures

### Initiatlisation et/ou affection

widget python
# Liste, initiatlisation et affectation 
liste = ['b', 7, 2, {}, ("1", 2)]
print liste 

liste_dixElementVides = [None] * 10
liste_pairs = [n for n in range(10) if n % 2 == 0] * 10 # On stocke les nombres paires compris de 0 à 10
liste_matrice = [([0] * 15) for i in range (10)] # On stocke un tableau de deux dimensions (une matrice) de 15 colonnes et de 10 lignes

# Tuple, initiatlisation et affectation 

# Dictionnaire, initiatlisation et affectation 

# Ensemble, initiatlisation et affectation 
widget

### Ajout/suppression d'élément(s)

### Vérifier si un élément existe

### Parcourir les éléments dans une boucle

### Concatenation

### Opérations d'ensembles

#### Différence 

#### Intersection 

#### Ou exclusif

### Insertion d'un élément

### Opération spécifiques 

#### Liste

##### Insérer un élément

#### Tuple

#### Dictionnaire

##### Obtenir toutes les clés

#### Ensemble 