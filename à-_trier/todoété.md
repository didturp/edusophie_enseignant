# todoété

+++sommaire

## premier blabla avec les eleves

- faire completer une fiche aux eleves : questions diverses + eval diagnostique
    - nom / prenom / ville de résidence
    - avez vous un ordinateur personnel chez vous ? 
    - avez vous acces a internet chez vous ? 
    - exercice python niveau novice 
    - exercice python niveau avancé

- parler de pedagogie inversée 
- passer la premiere seance sur des exo python 

### Seconde

### Premiere

## matériel a acheter

- deux banettes étiquetées : "fiches help" / "help !"
- clé usb (20) pour systmee linux du 8go minimum

## Établir des règles avec élèves 

- Lorsqu'on aide une personne ou un groupe : 
    - ne pas donner la réponse, ne pas orienter directement vers la réponse
    - ne pas toucher au clavier
- Lors d'un TP ou projet : 
    - (je vous le rapellerai) systeme de support; remplir une feuille "help !" et la poser  
- travail maison : 
    - un exo non fait = un avertissement, 3 avertissement = colle

## MAJ du site

### fonctionnalités

- pouvoir donner des couleur a des themes 
- integration google form + flubaroo : pedagogie active

### ui/ux

- Améliorer rendu du sommaire
- Renommer dossiers : apprentisages => espace eleves , enseignement => espace enseignant
- Ameliorer le coté responsive

### contenu

- Créer les premières séances

## Question directeur/orga

### En attente de rep
- des eleves presentant des handicap particuliers ? 
- possible dembarquer un pc portable avant la rentrée : voir un peu l'env de travail

## Organisation generale années 

- classe inversée systématique pour toute nouvelle séquence
- projet :
    - dossier de suivis de projet systématique ? 
    
## Organisation séquence/séance/évals

<table class="table bg-white">
    <tbody><tr>
        <th>Jour de rentrée scolaire des élèves</th>
        <td><u>Jeudi 2 septembre</u></td>
    </tr>
    <tr>
        <th>Zone académique</th>
        <td><u>C</u> (académie de Montpellier)</td>
    </tr>
    <tr>
        <th>Nombre de semaines de cours plaines (hors rentrée + semaines avec jour(s) fériés)</th>
        <td><u>30 semaines</u></td>
    </tr>
    <tr>
        <th>Nombre de theme en Seconde (hors transverse)</th>
        <td><u>7 thèmes</u></td>
    </tr>
    <tr>
        <th>Nombre de theme en Premiere (hors transverse)</th>
        <td><u>7 thèmes</u></td>
    </tr>
    <tr>
        <th>Nombre d'heures par theme sur les 19 semaines pleines</th>
        <td>( 19 semaines * 6 heures ) / 5 thèmes = <u>22.8 heures par thème soit environ 11.4 séances de 2 heures par thème</u></td>
    </tr>
</tbody></table>

### Seconde


### Premiere