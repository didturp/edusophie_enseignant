# exemple de création de base de données SQL

Ce document décrit comment construire une base de données SQL simple à travers un sénario.

## Dans la peau d'un développeur web

Vous avez reçu ce matin dans votre startup '**Yes, we dev !**' un nouveau client, Thomas. 

Thomas est un client au profil assez classique; jovial tout en restant professionnel et pleins d'idées mais néophyte en informatique (d'où le besoin de faire appel à vos services).

Thomas souhaite créer un site web dans lequel il va pouvoir partager avec sa communauté ses réalisateurs, ses acteurs et ses films préférés. Le site, dans sa version finale, doit permettre à la communauté de Thomas de noter les films, faire des recherches, laisser des commentaires, etc etc...tout un programme !

Matthieu, la personne la plus expérimentée de votre équipe, se charge de la répartition des taches. 

Connaissant votre niveau actuel, ce dernier vous propose de profiter de ce projet pour monter en compétence dans la conception et le développement de bases de données. Pas d'inquiétudes, il vous aidera tout au long de ce projet. 

### version 0.1

Matthieu revient vers vous avec des maquêtes fournises par Thomas :

<div class="d-flex justify-content-center flex-wrap w-100">
    <!--
    <div class="col-12">
        <img src="https://i.ibb.co/X5qCcRw/Screenshot-from-2021-01-15-22-08-48.png">
        <p class="text-center">maquette n°1</p>
    </div>
    -->
    <div class="col-12 md-2 col-md-6 mt-md-0">
        <img src="https://i.ibb.co/XJyB16P/Screenshot-from-2021-01-15-22-08-22.png">
        <p class="text-center">maquette n°1</p>
    </div>
    <div class="col-12 col-md-6">
        <img src="https://i.ibb.co/tLXt1QW/Screenshot-from-2021-01-15-22-43-15.png)">
        <p class="text-center">maquette n°2</p>
    </div>
</div>

Matthieu : ""

## Allez plus loin...

Emma, de votre équipe, est malheureusement tombée malade. Matthieu étant pris sur un autre projet, vous allez devoir reprendre le travail d'Emma et le terminer. Emma se chargait d'implementer la page 'Quizz', une page demandée par le client Thomas où les utilisateurs 

## trash

 
Voici les données que nous souhaitons stoquer dans notre SGBD (**S**ystème de **G**estion de **B**ase de **D**onnées) ? 

Nous voulons conserver, et pouvoir extraire efficacement, des données relatives aux domaine du **cinéma**. Voici quelques règles : 
- Les **acteurs** sont définis par un <u>nom</u>, un <u>prénom</u> et une <u>nationalité</u>
- Les **réalisateurs** sont également définis par un <u>nom</u>, un <u>prénom</u> et une <u>nationalité</u>
- Les **films** sont eux définis par un <u>titre</u>, une <u>année</u>, un <u>pays</u>, le <u>nombre de téléspectateur</u> qui ont visualisé le film et par le <u>réalisateur</u> qui l'a réalisé
- 


## 

+++ diagramme
erDiagram
    
+++